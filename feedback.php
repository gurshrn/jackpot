<?php
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'connect.php';
include 'GoogleRecaptcha.php';
if (!isset($_SESSION['id'])) {
	$_SESSION['returnUrl'] = $_SERVER['REQUEST_URI'];
	header("Location: login.php");
	exit;
}
$message = '';
if (isset($_POST['addfeedback'])) {
	$captcha = $_POST['g-recaptcha-response'];
	if(!empty($captcha)) {
		$cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($captcha);
		if($verified) {
		if (strlen($feedback) <= 300) {
			$feedback = filter_var($_POST['feedback'], FILTER_SANITIZE_STRING);
			$rating = filter_var($_POST['rating'], FILTER_VALIDATE_INT);
			if (empty($feedback)) {
				$message = '<div class="error">Feedback cannot be empty!</div>';
			}
			else if ($rating === false) {
				$message = '<div class="error">Invalid Rating!</div>';
			}
			else {
				$usr = $_SESSION['usr'];
				$usrid = $_SESSION['id'];
				$no = 'no';
				//$query = $mysqli->query("INSERT INTO rsj_feedbacks (`username`, `userid`, `feedback`, `rating`, `show`) VALUES ('{$usr}', '{$usrid}', '{$feedback}', '{$rating}', 'no')");
				
				$stmt = $mysqli->prepare("INSERT INTO rsj_feedbacks (`username`, `userid`, `feedback`, `rating`, `show`) VALUES (?, ?, ?, ?, 'no')");
				$stmt->bind_param("ssss", $usr, $usrid, $feedback, $rating);
				if ($stmt->execute()) {
					$message = '<div class="message">Feedback submitted successfully!</div>';
				}
				else {
					$message = '<div class="error">Error submitting the feedback. Please try again later.</div>';
				}
			}
		}
		else {
			$message = '<div class="error">Feedback exceeds max limit of 300 characters.</div>';
		}
		}
		else {
			$message = '<div class="error">Incomplete reCAPTCHA.</div>';
		}
	}
	else {
		$message = '<div class="error">Please complete reCAPTCHA.</div>';
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Feedback</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.1" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>';
		if ((isset($_SESSION['vip'])) && ($_SESSION['vip'] != 0)) {
			echo '<div class="vip-label"></div>';
		}
		echo'
		<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		
		 echo '</ul> 

		</div>
		</div>'; ?>
<div class="content-wrap">
	<div class="page-title">Post a feedback</div>
    <div class="main-content">
    <div class="form-wrap">
    <? if (!empty($message)) {
		echo $message;
	}?>
    <form method="post" action="feedback.php">
    	<textarea name="feedback" rows="5" placeholder="Write your feedback here. Character limit: 300" maxlength="300" required></textarea><br>
        <div class="acidjs-rating-stars">
        <input type="radio" name="rating" id="group-1-0" value="5" checked required/><label for="group-1-0"></label><!--
        --><input type="radio" name="rating" id="group-1-1" value="4" /><label for="group-1-1"></label><!--
        --><input type="radio" name="rating" id="group-1-2" value="3" /><label for="group-1-2"></label><!--
        --><input type="radio" name="rating" id="group-1-3" value="2" /><label for="group-1-3"></label><!--
        --><input type="radio" name="rating" id="group-1-4"  value="1" /><label for="group-1-4"></label>
        </div><br>
<label>reCAPTCHA</label>
        <div class="g-recaptcha" data-sitekey="6LdvsxATAAAAAJIUyXMFjqbRJzjVnH5MSMeTm1u7"></div>
        <input type="submit" class="submit" name="addfeedback" value="Submit" />
        </form></div>
    </div>
</div>
                        <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
