<?php
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include_once("config.php");

/* =====================================
 *	 PayPal Express Checkout Call
 * =====================================
 */
require_once ("paypal-express-checkout-vip/paypalfunctions.php");


$PaymentOption = "PayPal";
if ( $PaymentOption == "PayPal" )
{
    /*
     '------------------------------------
     ' this  step is required to get parameters to make DoExpressCheckout API call,
     ' this step is required only if you are not storing the SetExpressCheckout API call's request values in you database.
     ' ------------------------------------
     */
    $res = GetExpressCheckoutDetails( $_SESSION['TOKEN'] );

    /*
     '------------------------------------
     ' The paymentAmount is the total value of
     ' the purchase.
     '------------------------------------
     */

    $finalPaymentAmount =  $res["PAYMENTREQUEST_0_AMT"];

    /*
     '------------------------------------
     ' Calls the DoExpressCheckoutPayment API call
     '
     ' The ConfirmPayment function is defined in the file PayPalFunctions.php,
     ' that is included at the top of this file.
     '-------------------------------------------------
     */

    //Format the  parameters that were stored or received from GetExperessCheckout call.
    //$token 				= $_REQUEST['token'];
	$token				= $_SESSION['TOKEN'];
    $payerID 			= $_REQUEST['PayerID'];
    $paymentType 		= 'Sale';
    $currencyCodeType 	= $res['CURRENCYCODE'];
    $items = array();
    $i = 0;
    // adding item details those set in setExpressCheckout
    while(isset($res["L_PAYMENTREQUEST_0_NAME$i"]))
    {
        $items[] = array('name' => $res["L_PAYMENTREQUEST_0_NAME$i"], 'amt' => $res["L_PAYMENTREQUEST_0_AMT$i"], 'qty' => $res["L_PAYMENTREQUEST_0_QTY$i"]);
        $i++;
    }

    $resArray = ConfirmPayment ( $token, $paymentType, $currencyCodeType, $payerID, $finalPaymentAmount, $items );
    $ack = strtoupper($resArray["ACK"]);
    if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" )
    {

        /*
         * TODO: Proceed with desired action after the payment
         * (ex: start download, start streaming, Add coins to the game.. etc)
         '********************************************************************************************************************
         '
         ' THE PARTNER SHOULD SAVE THE KEY TRANSACTION RELATED INFORMATION LIKE
         '                    transactionId & orderTime
         '  IN THEIR OWN  DATABASE
         ' AND THE REST OF THE INFORMATION CAN BE USED TO UNDERSTAND THE STATUS OF THE PAYMENT
         '
         '********************************************************************************************************************
         */

        $transactionId		= $resArray["PAYMENTINFO_0_TRANSACTIONID"]; // Unique transaction ID of the payment.
        $transactionType 	= $resArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; // The type of transaction Possible values: l  cart l  express-checkout
        $paymentType		= $resArray["PAYMENTINFO_0_PAYMENTTYPE"];  // Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
        $orderTime 			= $resArray["PAYMENTINFO_0_ORDERTIME"];  // Time/date stamp of payment
        $amt				= $resArray["PAYMENTINFO_0_AMT"];  // The final amount charged, including any  taxes from your Merchant Profile.
        $currencyCode		= $resArray["PAYMENTINFO_0_CURRENCYCODE"];  // A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
        $feeAmt				= $resArray["PAYMENTINFO_0_FEEAMT"];  // PayPal fee amount charged for the transaction
        //	$settleAmt			= $resArray["PAYMENTINFO_0_SETTLEAMT"];  // Amount deposited in your PayPal account after a currency conversion.
        $taxAmt				= $resArray["PAYMENTINFO_0_TAXAMT"];  // Tax charged on the transaction.
        //	$exchangeRate		= $resArray["PAYMENTINFO_0_EXCHANGERATE"];  // Exchange rate if a currency conversion occurred. Relevant only if your are billing in their non-primary currency. If the customer chooses to pay with a currency other than the non-primary currency, the conversion occurs in the customer's account.

        /*
         ' Status of the payment:
         'Completed: The payment has been completed, and the funds have been added successfully to your account balance.
         'Pending: The payment is pending. See the PendingReason element for more information.
         */

        $paymentStatus = $resArray["PAYMENTINFO_0_PAYMENTSTATUS"];

        /*
         'The reason the payment is pending:
         '  none: No pending reason
         '  address: The payment is pending because your customer did not include a confirmed shipping address and your Payment Receiving Preferences is set such that you want to manually accept or deny each of these payments. To change your preference, go to the Preferences section of your Profile.
         '  echeck: The payment is pending because it was made by an eCheck that has not yet cleared.
         '  intl: The payment is pending because you hold a non-U.S. account and do not have a withdrawal mechanism. You must manually accept or deny this payment from your Account Overview.
         '  multi-currency: You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
         '  verify: The payment is pending because you are not yet verified. You must verify your account before you can accept this payment.
         '  other: The payment is pending for a reason other than those listed above. For more information, contact PayPal customer service.
         */

        $pendingReason = $resArray["PAYMENTINFO_0_PENDINGREASON"];

        /*
         'The reason for a reversal if TransactionType is reversal:
         '  none: No reason code
         '  chargeback: A reversal has occurred on this transaction due to a chargeback by your customer.
         '  guarantee: A reversal has occurred on this transaction due to your customer triggering a money-back guarantee.
         '  buyer-complaint: A reversal has occurred on this transaction due to a complaint about the transaction from your customer.
         '  refund: A reversal has occurred on this transaction because you have given the customer a refund.
         '  other: A reversal has occurred on this transaction due to a reason not listed above.
         */

        $reasonCode	= $resArray["PAYMENTINFO_0_REASONCODE"];

        // Add javascript to close Digital Goods frame. You may want to add more javascript code to
        // display some info message indicating status of purchase in the parent window


	$quantity 	= 1;
	$ip = $_SERVER['REMOTE_ADDR'];
	$fct = $_SESSION['fct'];
             
                    /*$name 	= filter_var($cart_itm["name"], FILTER_SANITIZE_STRING);
                    $username 	= filter_var($cart_itm["accountName"], FILTER_SANITIZE_STRING);
                    $password 	= filter_var($cart_itm["accountPassword"], FILTER_SANITIZE_STRING);
                    $server 	= filter_var($cart_itm["server"], FILTER_SANITIZE_STRING);
                    $price = $cart_itm["price"]; //CHECK PRICES!!!
                    $price*=$quantity;
					$link 	= filter_var($cart_itm["link"], FILTER_SANITIZE_STRING);*/
             //$mysqli->query("INSERT INTO rsj_payments (txnid, payment_amount, payment_status, productid, product, productname, case_status, item, userid, createdtime, ip, delivery, coupon) VALUES ('".$transactionId."', '".$amt."', '".$paymentStatus."', '".$productid."', '".$p."', '".$productname."', 'closed', '".$item[$y]."', '".$_SESSION['id']."', '".gmdate("Y-m-d H:i:s")."', '".$ip."', 'tbd', '".$_SESSION["coupon"]."')");
			$dt = gmdate("Y-m-d H:i:s");
			$num = 30;
			$exp = gmdate("Y-m-d H:i:s",strtotime('+ '.$num.'days'));
			$stmt = $mysqli->prepare("INSERT INTO rsj_vip (txnid, payment_amount, payment_status, userid, username, createdtime, expiration, ip, freecasetype) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("ssssssss", $transactionId, $amt, $paymentStatus, $_SESSION['id'], $_SESSION['usr'], $dt, $exp, $ip, $fct);
			$stmt->execute();
			$stmt->close();
			
			/*$fctevent = "CREATE EVENT vipfc_:{$transactionId}
    		ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 11 DAY
    		DO
				BEGIN
        			INSERT INTO `rsj_payments`
      			END";*/
			//$stmt = $mysqli->prepare("");
			unset($_SESSION["TOKEN"]);
			unset($_SESSION["fct"]);
        ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Payment Successful</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
</style>
</head>
<body>
<h1>Payment Successful</h1>
Your payment was completed successfully.<br>
Transaction ID: <br>
<strong> <? echo $transactionId; ?> </strong><br><br>
You're now a VIP member! <strong>Please logout and login again to enjoy VIP features.</strong>
</body>
</html>
    
    <?php
    }
    else
    {
        //Display a user friendly Error on the page using any of the following error information returned by PayPal
        $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
        $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
        $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
        $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

        //echo "DoExpressCheckoutDetails API call failed. ";
       // echo "Detailed Error Message: " . $ErrorLongMsg;
        //echo "Short Error Message: " . $ErrorShortMsg;
        //echo "Error Code: " . $ErrorCode;
       // echo "Error Severity Code: " . $ErrorSeverityCode;
        ?>
        <!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Payment Unsuccessful</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
</style>
</head>
<body>
<h1>Payment Unsuccessful</h1>
Your payment couldn't be completed.<br><br>

<a class="button" onClick="window.close();">
Close
</a>
</body>
</html>
    <?php
    }
}




?>