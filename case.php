<?php
	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require 'config.php';
	

	if (!isset($_SESSION['id'])) 
	{
		header("Location: login.php");
		exit;
	}
	

	else if (isset($_POST['case_name'])) 
	{
		//print_r($_POST['case_name']);die;
		include_once('free-case-animation.php');
		
		if(isset($_POST['case_type']) && !empty($_POST['case_type']))
		{
			$items = $mysqli->query("SELECT * FROM rsj_order_detail WHERE case_name='{$_POST['case_name']}' AND order_id ='{$_POST['order_id']}'");
			$item = mysqli_fetch_all($items,MYSQLI_ASSOC);
			
			
			
		
			$itemCase = explode(",",$item[0]['case_items']);
			$itemColor = explode(",",$item[0]['item_color']);
			$itemImage = explode(",",$item[0]['item_image']);
			
			

			$countItemCase = count($itemCase)-1;
			$rands = rand( 0, $countItemCase);
			
			$itemtargetitem = $itemCase[$rands];
			$targetItemColor = $itemColor[$rands];
			$targetItemImage = $itemImage[$rands];
			
			$itemtargetitem = $itemCase[$rands];
			$itemtargetkey = array_search ($itemtargetitem, $itemCase);
			$itemitemscount = count($itemCase);

			$itemmarginpxprep = $itemtargetkey+ $itemitemscount*3;
			$marginpx = 175 - (110*$itemmarginpxprep)- ($itemtargetkey!=$itemitemscount-1?0:140);

			$i = 0;
			for($k=0;$k<4;$k++){
				foreach($itemCase as $key=>$val)
				{
					echo 	'<div class="product-item '.$itemColor[$key].'-c" id="selector-'.$i.'">
							<div class="img"><img src="admin/upload/'.$itemImage[$key].'" alt="'.$val.'"/></div>
							'.$val.'
							</div>';
							
					$i++;
				}
			}
			
			mysqli_query($mysqli,"INSERT INTO rsj_order_item (order_item, order_item_color,order_item_image, order_id) VALUES ('".$itemtargetitem."','".$targetItemColor."', '".$targetItemImage."','".$_POST['order_id']."')");
			
			$orderItems = $mysqli->query("SELECT * FROM rsj_order_item WHERE order_id ='{$_POST['order_id']}'");
			$totalOrderItems = mysqli_fetch_all($orderItems,MYSQLI_ASSOC);
			$orderTotalQty = $item[0]['case_qty'];
			$purchaseQty = count($totalOrderItems);
			$totalPurchaseQty = $orderTotalQty-$purchaseQty;
			
			
			mysqli_query($mysqli,"UPDATE rsj_order_detail SET purchase_qty='{$totalPurchaseQty}' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
			
			if(count($totalOrderItems) == $orderTotalQty)
			{
				mysqli_query($mysqli,"UPDATE rsj_order_detail SET status='opened',purchase_qty='0' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
			}
		}
		
		else
		{
			
			$fccheck = $mysqli->query( "SELECT content FROM rsj_editables WHERE name = 'freecases'" )->fetch_object()->content;
			if ( $fccheck == 'enabled' ) 
			{ 
				//PRE-CONDITION: Check if free cases are enabled
				
				$stmt = $mysqli->prepare("SELECT lastfreecase FROM rsj_members WHERE `id` =?");
				$stmt->bind_param("s", $_SESSION['id']);
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($lastfc);
				$stmt->fetch();
				$stmt->close();
				
				//CONDITION 1: Give case for the first time			
				if ( $lastfc == '0000-00-00 00:00:00' ) 
				{
					
					$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
					$stmt->bind_param( "s", $_SESSION[ 'id' ] );
					$stmt->execute();
					$stmt->store_result();
					$stmt->bind_result( $lastfc );
					$stmt->fetch();
					$stmt->close();
					
					
					if ( isset( $_POST[ 'case_name' ] ) ) 
					{
						$dt = gmdate("Y-m-d H:i:s");
						
						$items = $mysqli->query("SELECT * FROM rsj_order_detail WHERE case_name='{$_POST['case_name']}' AND order_id ='{$_POST['order_id']}'");
						$item = mysqli_fetch_all($items,MYSQLI_ASSOC);
						
					
						$itemCase = explode(",",$item[0]['case_items']);
						$itemColor = explode(",",$item[0]['item_color']);
						$itemImage = explode(",",$item[0]['item_image']);

						$countItemCase = count($itemCase)-1;
						$rands = rand( 0, $countItemCase);
						
						$itemtargetitem = $itemCase[$rands];
						$targetItemColor = $itemColor[$rands];
						$targetItemImage = $itemImage[$rands];
						
						$itemtargetitem = $itemCase[$rands];
						$itemtargetkey = array_search ($itemtargetitem, $itemCase);
						$itemitemscount = count($itemCase);

						$itemmarginpxprep = $itemtargetkey+ $itemitemscount*3;
						$marginpx = 175 - (110*$itemmarginpxprep)- ($itemtargetkey!=$itemitemscount-1?0:140);

						$i = 0;
						for($k=0;$k<4;$k++){
							foreach($itemCase as $key=>$val)
							{
								echo 	'<div class="product-item '.$itemColor[$key].'-c" id="selector-'.$i.'">
										<div class="img"><img src="admin/upload/'.$itemImage[$key].'" alt="'.$val.'"/></div>
										'.$val.'
										</div>';
										
								$i++;
							}
						}
						
						mysqli_query($mysqli,"INSERT INTO rsj_order_item (order_item, order_item_color,order_item_image, order_id) VALUES ('".$itemtargetitem."','".$targetItemColor."', '".$targetItemImage."','".$_POST['order_id']."')");
						
						$orderItems = $mysqli->query("SELECT * FROM rsj_order_item WHERE order_id ='{$_POST['order_id']}'");
						$totalOrderItems = mysqli_fetch_all($orderItems,MYSQLI_ASSOC);
						$orderTotalQty = $item[0]['case_qty'];
						$purchaseQty = count($totalOrderItems);
						$totalPurchaseQty = $orderTotalQty-$purchaseQty;
						
						
						mysqli_query($mysqli,"UPDATE rsj_order_detail SET purchase_qty='{$totalPurchaseQty}' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
						
						if(count($totalOrderItems) == $orderTotalQty)
						{
							mysqli_query($mysqli,"UPDATE rsj_order_detail SET status='opened',purchase_qty='0' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
						}
						
						$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
						$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
						$stmt->execute();
						$stmt->close();
					} 
				} 
				
				else 
				{
					
					//Outcome: We have purchase on record, lets see if it meets next condition

					//CONDITION 3: Check if the last purchase was within 1 month
					
					$lastpurchasedt = $mysqli->query( "SELECT createdtime FROM rsj_order_payment WHERE user_id = '{$_SESSION['id']}' AND payment_status='Completed' AND txn_id = 'Free Case' ORDER BY rsj_order_payment.id DESC LIMIT 1" )->fetch_object()->createdtime;

					$gmt = new DateTimeZone( 'GMT' );
					$currentdt = new DateTime( gmdate( "Y-m-d H:i:s" ), $gmt );

					$checkpurchasedt = new DateTime( $lastpurchasedt, $gmt );
					$checkpurchasedt->modify( '+30 day' );

					if ( $currentdt < $checkpurchasedt ) 
					{ //Outcome: It is within 1 month

						//CONDITION 4: Check if Daily Free Case was claimed already
						$dt = gmdate("Y-m-d H:i:s");
						$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
						$stmt->bind_param( "s", $_SESSION[ 'id' ] );
						$stmt->execute();
						$stmt->store_result();
						$stmt->bind_result( $lastfc );
						$stmt->fetch();
						$stmt->close();

						$gmt = new DateTimeZone( 'GMT' );
						$currentdt = new DateTime( gmdate( "Y-m-d H:i:s" ), $gmt );

						$lastfcdt = new DateTime( $lastfc, $gmt );

						$checkdt = new DateTime( $lastfc, $gmt );
						$checkdt->modify( '+1 day' );


						if ( $currentdt > $checkdt ) 
						{
							$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
							$stmt->bind_param( "s", $_SESSION[ 'id' ] );
							$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result( $lastfc );
							$stmt->fetch();
							$stmt->close();

							if ( isset( $_POST[ 'case_name' ] ) ) 
							{
								
							
								$items = $mysqli->query("SELECT * FROM rsj_order_detail WHERE case_name='{$_POST['case_name']}' AND order_id ='{$_POST['order_id']}'");
								$item = mysqli_fetch_all($items,MYSQLI_ASSOC);
								
							
								$itemCase = explode(",",$item[0]['case_items']);
								$itemColor = explode(",",$item[0]['item_color']);
								$itemImage = explode(",",$item[0]['item_image']);

								$countItemCase = count($itemCase)-1;
								$rands = rand( 0, $countItemCase);
								
								$itemtargetitem = $itemCase[$rands];
								$targetItemColor = $itemColor[$rands];
								$targetItemImage = $itemImage[$rands];
								
								$itemtargetitem = $itemCase[$rands];
								$itemtargetkey = array_search ($itemtargetitem, $itemCase);
								$itemitemscount = count($itemCase);

								$itemmarginpxprep = $itemtargetkey+ $itemitemscount*3;
								$marginpx = 175 - (110*$itemmarginpxprep)- ($itemtargetkey!=$itemitemscount-1?0:140);

								$i = 0;
								for($k=0;$k<4;$k++){
									foreach($itemCase as $key=>$val)
									{
										echo 	'<div class="product-item '.$itemColor[$key].'-c" id="selector-'.$i.'">
												<div class="img"><img src="admin/upload/'.$itemImage[$key].'" alt="'.$val.'"/></div>
												'.$val.'
												</div>';
												
										$i++;
									}
								}
								
								mysqli_query($mysqli,"INSERT INTO rsj_order_item (order_item, order_item_color,order_item_image, order_id) VALUES ('".$itemtargetitem."','".$targetItemColor."', '".$targetItemImage."','".$_POST['order_id']."')");
								
								$orderItems = $mysqli->query("SELECT * FROM rsj_order_item WHERE order_id ='{$_POST['order_id']}'");
								$totalOrderItems = mysqli_fetch_all($orderItems,MYSQLI_ASSOC);
								$orderTotalQty = $item[0]['case_qty'];
								$purchaseQty = count($totalOrderItems);
								$totalPurchaseQty = $orderTotalQty-$purchaseQty;
								
								
								mysqli_query($mysqli,"UPDATE rsj_order_detail SET purchase_qty='{$totalPurchaseQty}' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
								
								if(count($totalOrderItems) == $orderTotalQty)
								{
									mysqli_query($mysqli,"UPDATE rsj_order_detail SET status='opened',purchase_qty='0' WHERE case_name='{$_POST['case_name']}' AND order_id='{$_POST['order_id']}'");
								}
													

								$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
								$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
								$stmt->execute();
								$stmt->close();
							}
						} 
						else 
						{ //Outcome: There's still time left
							header( "Location: login.php" );
							exit;
						}
					} 
					else 
					{ //Outcome: Latest purchase not within 1 month
						header( "Location: login.php" );
						exit;
					}
				}
			}
		}
		?>

		

	
		<?php
			$items = $mysqli->query("SELECT * FROM rsj_order_detail WHERE case_name ='{$_POST['case_name']}' AND order_id ='{$_POST['order_id']}'");
			$item = mysqli_fetch_all($items,MYSQLI_ASSOC);
			
			
			
			foreach($item as $caseitem)
			{
				
				$itemCase = ucfirst($caseitem['case_items']);
			}
			
			
			$targetitem = $caseItems[$rand];
			$targetkey = array_search ($targetitem, $itemCase);
			$itemscount = count($itemCase);
			$marginpxprep = $targetkey + $itemscount*3;
			$marginpxs = 175 - (110*$marginpxprep)- ($targetkey!=$itemscount-1?0:140);


			

			$j = 0;
			for($k=0;$k<4;$k++){
				foreach($itemCase as $key=>$val1)
				{
					echo 	'<div class="product-item '.$val1['case_color'].'-c" id="selector-'.$i.'">
							<div class="img"><img src="admin/upload/'.$val1['item_image'].'" alt="'.$val1['case_items'].'"/></div>
							'.$val1['case_items'].'
							</div>';
							
					$j++;
				}
			}
			
		?>
		
		</div>
		<input type="hidden" value="<?php echo $itemmarginpxprep; ?>" class="itemmarginpxprep">
		<input type="hidden" value="<?php echo $marginpx; ?>" class="itemmarginpx">

		<input type="hidden" value="<?php echo $marginpxprep; ?>" class="marginpxprep"> 
		<input type="hidden" value="<?php echo $marginpxs; ?>" class="marginpxs"> 
		<div id="arrow"></div></div>
			<script type="text/javascript">
			$(function() {
				var marginx = $('.marginpxprep').val();
				var marginpx = $('.marginpxs').val();
				if($('.itemmarginpxprep').val() != '')
				{
					marginx = $('.itemmarginpxprep').val();
					marginpx = $('.itemmarginpx').val();

				}
				
				
				$("#arrow").animate({ 'borderRadius': '0', 'width': '2px' }, 1000, 'easeInOutExpo');
				  	
				setTimeout( function(){
						$(".open-case-wrap .product-item:first-child").animate({ "marginLeft":  marginpx + 'px'}, <?php echo 1000*$itemitemscount;?>, 'easeOutQuart',showSelectedItem); 
				}, 600 );
				
			function showSelectedItem(){
				setTimeout( function(){
					$("#selector-"+marginx).animate({ 'borderRadius': '0' }, 1000, 'easeOutElastic');
					 $("#selector-"+marginx).effect( "pulsate", {times:6},  1000 );
					setTimeout( function(){
					 $("#arrow").animate({ 'borderRadius': '130px', 'width': '450px' }, 1000, 'easeInOutExpo',pageRedirect);
					}, 2800 );
				}, 1000 );
				
			}
			function pageRedirect(){
				window.location = "inventory.php";
			}
			
			});
			</script>
		</body>
	</html>

	<?php } ?>
