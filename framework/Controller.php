<?php 
class Controller {
	
	private $database;
	
	function __construct() {
		include 'Database.php';
		include 'structures/Product.php';
		include 'structures/Pack.php';
		include 'structures/Coin.php';
		include 'structures/PriorityNode.php';
		include 'structures/PriorityItem.php';
		include 'structures/PriorityQueue.php';
		
		$this->database = new Database();
	}
	
	function getDatabase() {
		return $this->database;
	}
	
	function destroy() {
		$this->database->getMysqliConnection()->close();
	}
	
}
?>