<?php 
class Database {

	const SERVER_IP_ADDRESS = 'localhost';
	const USERNAME = 'customer_jackpot';
	const PASSWORD = 'im@rkinfotech';
	const NAME = 'customer_jackpot';
	
	private $mysqliConnection;
	
	function __construct() {
		$connect_to = self::SERVER_IP_ADDRESS;
		if ($connect_to == $_SERVER['SERVER_ADDR']) {
			$connect_to = 'localhost';
		}
		$this->mysqliConnection = new mysqli ( $connect_to, self::USERNAME, self::PASSWORD, self::NAME );
		if (mysqli_connect_error ()) {
			die ( 'There was a problem reaching the database' );
			exit();
		}
	}
	
	function getMysqliConnection() {
		return $this->mysqliConnection;
	}
	
	function getAnnouncement() {
		$result = $this->mysqliConnection->query("SELECT `content` from `rsj_editables` WHERE `name` = 'announcement'")->fetch_object()->content;
		$announcement = htmlspecialchars_decode(stripslashes($result));
		return $announcement;
	}
	
	function getFcase() {
		$fcaseid = $this->mysqliConnection->query("SELECT `content` from `rsj_editables` WHERE `name` = 'fcase'")->fetch_object()->content;
		$results = $this->mysqliConnection->query("SELECT * from `rsj_products` WHERE `id` = '{$fcaseid}'");
		$fcase = $results->fetch_assoc();
		return $fcase;
	}
	
	function getFpack() {
		$fpackid = $this->mysqliConnection->query("SELECT `content` from `rsj_editables` WHERE `name` = 'fpack'")->fetch_object()->content;
		$results = $this->mysqliConnection->query("SELECT * from `rsj_packs` WHERE `id` = '{$fpackid}'");
		$fpack = $results->fetch_assoc();
		return $fpack;
	}
	
	
	
	function getPacks() {
		$packs = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` from `rsj_packs`");
		$stmt->execute();
		$stmt->bind_result($id, $name, $desc, $items, $price, $type);
		while ($stmt->fetch()) {
			$packs[] = new Pack($id, $name, $desc, $items, $price, $type);
		}
		return $packs;
	}
	
	function getCoins() {
		$coins = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` from `rsj_coins`");
		$stmt->execute();
		$stmt->bind_result($id, $name, $desc, $items, $price, $type);
		while ($stmt->fetch()) {
			$coins[] = new Coin($id, $name, $desc, $items, $price, $type);
		}
		return $coins;
	}
	
	function getProductsByType($productType) {
		$products = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `id`,`case_name`,`case_price`,`case_type`,`case_color` FROM rsj_case WHERE `case_type`=?");
		$stmt->bind_param("s", $productType);
		$stmt->execute();
		$stmt->bind_result($id,$case_name,$case_price,$case_type,$case_color);
		while ($stmt->fetch()) {
			$products[] = new Product($id,$case_name,$case_price,$case_type,$case_color);
		}
		$products=$this->getCaseItems($products);
		return $products;
	}
	function getProducts($catId) {
		$products = array();
		$stmt = $this->mysqliConnection->prepare("SELECT rsj_case.id,rsj_case.case_name,rsj_case.case_price,rsj_sub_category.sub_category_name as case_type,rsj_case.case_color FROM rsj_case INNER JOIN rsj_sub_category ON rsj_case.case_type = rsj_sub_category.id WHERE rsj_case.cat_id=? AND rsj_case.case_price!= 0");
		$stmt->bind_param("s", $catId);
		$stmt->execute();
		$stmt->bind_result($id,$case_name,$case_price,$case_type,$case_color);
		while ($stmt->fetch()) {
			$products[] = new Product($id,$case_name,$case_price,$case_type,$case_color);
		}
		
		$products=$this->getCaseItems($products);
		return $products;
	}
	function getCaseItems($products)
	{
		foreach($products as $val)
		{
			
			$results = $this->mysqliConnection->query("SELECT * FROM rsj_case_items WHERE case_id='{$val->getId()}'");
			$fcase = $results->fetch_all(MYSQLI_ASSOC);
			$val->setCases($fcase);
		}
		return $products;
	}
	function oldgetProductsByType($productType) {
		$products = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `*` from `rsj_case_items` WHERE `type`=? AND `visibility` <> 'disabled'");
		$stmt->bind_param("s", $productType);
		$stmt->execute();
		$stmt->bind_result($id, $name, $desc, $items, $priority, $price, $type);
		while ($stmt->fetch()) {
			$products[] = new Product($id, $name, $desc, $items, $priority, $price, $type);
		}
		return $products;
	}
	
	function getPacksByType($packType) {
		$packs = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` from `rsj_packs` WHERE `type`=?");
		$stmt->bind_param("s", $packType);
		$stmt->execute();
		$stmt->bind_result($id, $name, $desc, $items, $price, $type);
		while ($stmt->fetch()) {
			$packs[] = new Pack($id, $name, $desc, $items, $price, $type);
		}
		return $packs;
	}
	
	function getCoinsByType($coinType) {
		$coins = array();
		$stmt = $this->mysqliConnection->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` from `rsj_coins` WHERE `type`=?");
		$stmt->bind_param("s", $coinType);
		$stmt->execute();
		$stmt->bind_result($id, $name, $desc, $items, $price, $type);
		while ($stmt->fetch()) {
			$coins[] = new Coin($id, $name, $desc, $items, $price, $type);
		}
		return $coins;
	}
	function getFeedbacks() {
		$result = $this->mysqliConnection->query("SELECT * from `rsj_feedbacks` WHERE `show` = 'yes' ORDER BY rsj_feedbacks.id DESC");
		return $result;
	}
	function getBigwins() {
		$result = $this->mysqliConnection->query("SELECT * from `rsj_bigwins` ORDER BY `rsj_bigwins`.`id` ASC");
		return $result;
	}
	function getFlashsale() {
		$results = $this->mysqliConnection->query("SELECT * from `rsj_flash`");
		$result = $results->fetch_assoc();
		return $result;
	}
	function getCategory()
	{
		$results = $this->mysqliConnection->query("SELECT * from `rsj_category`");
		$result = $results->fetch_all(MYSQLI_ASSOC);
		return $result;
	}
	public function getSubCategory($catId)
	{
		$results = $this->mysqliConnection->query("SELECT * from `rsj_sub_category` WHERE `cat_id` = '{$catId}'");
		$result = $results->fetch_all(MYSQLI_ASSOC);
		return $result;
	}
	function getFlashproduct() {
		$flashdata = $this->getFlashsale();
		$id = $flashdata['productid'];
		switch ($flashdata['p']) {
    		case "case":
        		$table = 'rsj_products';
        		break;
    		case "pack":
        		$table = 'rsj_packs';
        		break;
    		case "coin":
        		$table = 'rsj_coins';
        		break;
		}
		$results = $this->mysqliConnection->query("SELECT * from `{$table}` WHERE `id` = '{$id}'");
		$result = $results->fetch_assoc();
		return $result;
	}/*
	function getVIPexpiration($userid) {
		$result = $this->mysqliConnection->query("SELECT expiration from `rsj_vip` WHERE `userid` = '{$userid}'");
		$expiration = $result->fetch_object()->expiration;
		return $expiration;
	}*/
	
}
?>