<?php 
class PriorityQueue {
	
	private $queue;
	private $min;
	private $max;
	
	function __construct() {
		$this->queue = array();
		$this->min = 0;
		$this->max = 0;
	}
	
	function add($priorityNode) {
		if ($priorityNode->getPriority() < $this->min) {
			$this->min = $priorityNode->getPriority();
		}
		if ($this->max < $priorityNode->getPriority()) {
			$this->max = $priorityNode->getPriority();
		}
		$this->queue[] = $priorityNode;
	}
	
	function getMin() {
		return $this->min;
	}
	
	function getMax() {
		return $this->max;
	}
	
	function getQueueAtLevel($priority) {
		$subQueue = array();
		for ($index = count($this->queue) - 1; 0 <= $index; $index --) {
			$node = $this->queue[$index];
			if ($node->getPriority() == $priority) {
				$subQueue[] = $node;
			}
		}
		return $subQueue;
	}
	
}
?>