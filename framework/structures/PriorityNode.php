<?php 
class PriorityNode {
	
	private $priority;
	
	function __construct($priority) {
		$this->priority = $priority;
	}
	
	function getPriority() {
		return $this->priority;
	}
	
}
?>