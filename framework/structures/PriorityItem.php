<?php 
class PriorityItem extends PriorityNode {
	
	private $name;
	
	function __construct($priority, $name) {
		parent::__construct($priority);
		$this->name = $name;
	}
	
	function getName() {
		return $this->name;
	}
	
}
?>