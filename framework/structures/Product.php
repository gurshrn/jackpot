<?php 
class Product {
	
	private $id;
	private $name;
	private $desc;
	private $items;
	private $priority;
	private $price;
	private $type;
	private $cases;
	
	private $itemsQueue;
	
	function __construct($id, $name, $desc, $items, $priority, $price, $type,$cases=NULL) {
		$this->id = $id;
		$this->name = $name;
		$this->desc = $desc;
		$this->items = $items;
		$this->priority = $priority;
		$this->price = number_format($price-($price*$_SESSION['vip']), 2);
		$this->type = $type;
		$this->cases = $cases;
		
		$this->itemsQueue = new PriorityQueue();
		for ($i = 0; $i < count($this->getItemsArray()) && $i < count($this->getPrioritiesArray()); $i ++) {
			$itemName = $this->getItemsArray()[$i];
			$itemPriority = $this->getPrioritiesArray()[$i];
			$priorityItem = new PriorityItem($itemPriority, $itemName);
			
			$this->itemsQueue->add($priorityItem);
		}
	}
	
	function getId() {
		return $this->id;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getDesc() {
		return $this->desc;
	}
	function getItems() {
		return $this->items;
	}
	function getCases() {
		return $this->cases;
	}
	function setCases($cases) {
		$this->cases=$cases;
	}
	function getItemsArray() {
		return explode(",", $this->items);
	}
	
	function getPriority() {
		return $this->priority;
	}
	
	function getPrioritiesArray() {
		return explode(",", $this->priority);
	}
	
	/**
	 * @var PriorityQueue
	 */
	function getItemsQueue() {
		return $this->itemsQueue;
	}
	
	function getPrice() {
		return $this->price;
	}
	
	function getType() {
		return $this->type;
	}
	
}

?>