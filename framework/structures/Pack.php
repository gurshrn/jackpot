<?php 
class Pack {
	
	private $id;
	private $name;
	private $desc;
	private $items;
	private $price;
	private $type;
	
	function __construct($id, $name, $desc, $items, $price, $type) {
		$this->id = $id;
		$this->name = $name;
		$this->desc = $desc;
		$this->items = $items;
		$this->price = number_format($price-($price*$_SESSION['vip']), 2);
		$this->type = $type;
	}
	
	function getId() {
		return $this->id;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getDesc() {
		return $this->desc;
	}
	
	function getItems() {
		return $this->items;
	}
	
	function getItemsArray() {
		return explode(",", $this->items);
	}
	
	function getPrice() {
		return $this->price;
	}
	function getType() {
		return $this->type;
	}
	
}

?>