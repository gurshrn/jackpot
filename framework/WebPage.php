<?php 
include 'Controller.php';

abstract class WebPage {
	
	private $controller;
	
	function __construct() {
		$this->controller = new Controller();
	}
	
	/**
	 * @return boolean
	 */
	abstract function preProcessing();
	
	abstract function output();
	
	function execute() {
		include('session.php');
		
		secure_session_start();
		$timezone = $_SESSION['time'];
		if ($this->preProcessing()) {
			$this->output();
		}
	}
	
	function getController() {
		return $this->controller;
	}
	
}
?>