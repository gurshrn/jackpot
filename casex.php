<?php
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'connect.php';
if (!isset($_SESSION['id'])) {
	header("Location: login.php");
	exit;
}
else if (isset($_POST['caseid'])) {
	$caseid = $_POST['caseid'];
	//$results = $mysqli->query("SELECT * FROM rsj_payments WHERE userid ='{$_SESSION['id']}' && id = '{$caseid}'");
	
	$stmt = $mysqli->prepare("SELECT `case_status`, `productid`, `item` FROM rsj_payments WHERE userid=? AND id=?");
	$stmt->bind_param("ss", $_SESSION['id'], $caseid);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($casestatus, $caseproductid, $caseitem);
	$stmt->fetch();
	
	if ($stmt->num_rows === 0) {
		header("Location: inventory.php");
		exit;
	}
	else {
		if ($casestatus == 'opened') {
			header("Location: inventory.php");
			exit;
		}
		else {
			//$mysqli->query("UPDATE rsj_payments SET case_status = 'opened' WHERE id = '{$caseid}'");
			/*$stmtx = $mysqli->prepare("UPDATE rsj_payments SET case_status = 'opened' WHERE id =?");
			$stmtx->bind_param("s", $caseid);
			$stmtx->execute();
			$stmtx->close();*/
			?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Opening Case...</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
* {
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
#arrow {
	text-shadow: #000 0 1px 3px;
    font-size: 48px;
    display: inline-block;
    position: absolute;
    top: 0;
	right: 0;
	left: 0;
	margin: 0 auto;
    background: rgba(51, 103, 153, 0.4);
    width: 450px;
	border-radius: 130px;
    height: 203px;
}
	.super-wrap {
		width: 450px;
		margin: 0 auto;
		position:  relative;
	}
.open-case-wrap {
	overflow: hidden;
    display: inline-block;
    vertical-align: middle;
    background: #252525;
    width: 450px;
    border-radius: 130px;
    box-shadow: inset rgba(0, 0, 0, 0.3) 2px 2px 25px, #5d5d5d 0 0 3px;
    white-space: nowrap;
    padding: 50px 0;
}
.product-item {
	text-align: center;
    background: #222;
    color: #FFF;
    font-size: 12px;
    margin: 0 5px 0 5px;
    border-radius: 50px;
    overflow: hidden;
    box-shadow: rgba(0,0,0,0.5) 0 1px 5px;
    text-shadow: rgba(0,0,0,0.7) 0 1px 1px;
    display: inline-block;
    width: 100px;
    height: 100px;
    white-space: initial;
}
.open-case-wrap .product-item:first-child {
	margin-left: 450px;
}
.product-item .img {
	background: rgba(0,0,0,0.3);
    height: 48px;
    padding: 10px;
    margin-bottom: 15px;
    text-align: center;
}
.blue-c {
	border: solid 2px rgba(0, 76, 255, 0.5);
}
.purple-c {
	border: solid 2px rgba(105, 0, 255, 0.5);
}
.pink-c {
	border: solid 2px rgba(255, 0, 190, 0.5);
}
.red-c {
	border: solid 2px rgba(255, 0, 4, 0.5);
}
.green-c {
	border: solid 2px rgba(9, 231, 0, 0.5);
}
.gold-c {
	border: solid 2px rgba(255, 215, 0, 0.5);
}
.rotate {
  -webkit-transform: rotate(3240deg);  /* Chrome, Safari 3.1+ */
     -moz-transform: rotate(3240deg);  /* Firefox 3.5-15 */
      -ms-transform: rotate(3240deg);  /* IE 9 */
       -o-transform: rotate(3240deg);  /* Opera 10.50-12.00 */
          transform: rotate(3240deg);  /* Firefox 16+, IE 10+, Opera 12.50+ */
}
.rotate-transition {
  -webkit-transition: all 2s ease-out;  /* Chrome 1-25, Safari 3.2+ */
     -moz-transition: all 2s ease-out;  /* Firefox 4-15 */
       -o-transition: all 2s ease-out;  /* Opera 10.50–12.00 */
          transition: all 2s ease-out;  /* Chrome 26, Firefox 16+, IE 10+, Opera 12.50+ */
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
</head>

<body>
<h1>Opening Case...</h1>
<div class="super-wrap">
<div class="open-case-wrap">
<?
$products = $mysqli->query("SELECT * FROM rsj_products WHERE id ='{$caseproductid}'");
$product = $products->fetch_assoc();
$items = array_reverse(explode(',', $product['items']));
$priority = array_reverse(explode(',', $product['priority']));
$targetitem = $caseitem;
$targetkey = array_search ($targetitem, $items);
$itemscount = count($items);
//$marginpx = (($targetkey)*72)-72;
$marginpxprep = $targetkey + $itemscount*3;
$marginpx = 175 - (110*$marginpxprep);
for($i=0; $i<$itemscount; $i++) {
				if ($priority[$i] == '5') {
					$class = "blue-c";
				}
				if ($priority[$i] == '4') {
					$class = "purple-c";
				}
				if ($priority[$i] == '3') {
					$class = "pink-c";
				}
				if ($priority[$i] == '2') {
					$class = "red-c";
				}
				if ($priority[$i] == '1') {
					$class = "green-c";
				}
				if ($items[$i] == 'Godsword Shard 1/2/3') {
					$image = 'Godsword Shard';
				}
				else if ($product['id'] == '9') {
					if (strpos($items[$i],'Frost') !== false) {
						$image = 'Frost bones';
					}
					else if (strpos($items[$i],'Dagonnath') !== false) {
						$image = 'Dagonnath bones';
					}
					else if (strpos($items[$i],'Burnt') !== false) {
						$image = 'Burnt bones';
					}
					else if (strpos($items[$i],'Baby') !== false) {
						$image = 'Baby dragon bones';
					}
					else if (strpos($items[$i],'Monkey') !== false) {
						$image = 'Monkey bones';
					}
					else if (strpos($items[$i],'Big') !== false) {
						$image = 'Big bones';
					}
					else {
						$image = 'Bones';
					}
				}
				else {
					$image = $items[$i];
				}
				if ($product['type'] == '07') {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$i.'">
						<div class="img"><img src="assets/images/07/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
				else {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$i.'">
						<div class="img"><img src="assets/images/rs3/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
		}
			
			//repeat items
			
			for($i=0; $i<$itemscount; $i++) {
				$selector = $itemscount + $i;
				if ($priority[$i] == '5') {
					$class = "blue-c";
				}
				if ($priority[$i] == '4') {
					$class = "purple-c";
				}
				if ($priority[$i] == '3') {
					$class = "pink-c";
				}
				if ($priority[$i] == '2') {
					$class = "red-c";
				}
				if ($priority[$i] == '1') {
					$class = "green-c";
				}
				if ($items[$i] == 'Godsword Shard 1/2/3') {
					$image = 'Godsword Shard';
				}
				else if ($product['id'] == '9') {
					if (strpos($items[$i],'Frost') !== false) {
						$image = 'Frost bones';
					}
					else if (strpos($items[$i],'Dagonnath') !== false) {
						$image = 'Dagonnath bones';
					}
					else if (strpos($items[$i],'Burnt') !== false) {
						$image = 'Burnt bones';
					}
					else if (strpos($items[$i],'Baby') !== false) {
						$image = 'Baby dragon bones';
					}
					else if (strpos($items[$i],'Monkey') !== false) {
						$image = 'Monkey bones';
					}
					else if (strpos($items[$i],'Big') !== false) {
						$image = 'Big bones';
					}
					else {
						$image = 'Bones';
					}
				}
				else {
					$image = $items[$i];
				}
				if ($product['type'] == '07') {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/07/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
				else {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/rs3/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
		}
			
			//repeat items x 2
			
			for($i=0; $i<$itemscount; $i++) {
				$selector = (2*$itemscount) + $i;
				if ($priority[$i] == '5') {
					$class = "blue-c";
				}
				if ($priority[$i] == '4') {
					$class = "purple-c";
				}
				if ($priority[$i] == '3') {
					$class = "pink-c";
				}
				if ($priority[$i] == '2') {
					$class = "red-c";
				}
				if ($priority[$i] == '1') {
					$class = "green-c";
				}
				if ($items[$i] == 'Godsword Shard 1/2/3') {
					$image = 'Godsword Shard';
				}
				else if ($product['id'] == '9') {
					if (strpos($items[$i],'Frost') !== false) {
						$image = 'Frost bones';
					}
					else if (strpos($items[$i],'Dagonnath') !== false) {
						$image = 'Dagonnath bones';
					}
					else if (strpos($items[$i],'Burnt') !== false) {
						$image = 'Burnt bones';
					}
					else if (strpos($items[$i],'Baby') !== false) {
						$image = 'Baby dragon bones';
					}
					else if (strpos($items[$i],'Monkey') !== false) {
						$image = 'Monkey bones';
					}
					else if (strpos($items[$i],'Big') !== false) {
						$image = 'Big bones';
					}
					else {
						$image = 'Bones';
					}
				}
				else {
					$image = $items[$i];
				}
				if ($product['type'] == '07') {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/07/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
				else {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/rs3/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
		}
			
			//repeat items x 3
			
			for($i=0; $i<$itemscount; $i++) {
				$selector = (3*$itemscount) + $i;
				if ($priority[$i] == '5') {
					$class = "blue-c";
				}
				if ($priority[$i] == '4') {
					$class = "purple-c";
				}
				if ($priority[$i] == '3') {
					$class = "pink-c";
				}
				if ($priority[$i] == '2') {
					$class = "red-c";
				}
				if ($priority[$i] == '1') {
					$class = "green-c";
				}
				if ($items[$i] == 'Godsword Shard 1/2/3') {
					$image = 'Godsword Shard';
				}
				else if ($product['id'] == '9') {
					if (strpos($items[$i],'Frost') !== false) {
						$image = 'Frost bones';
					}
					else if (strpos($items[$i],'Dagonnath') !== false) {
						$image = 'Dagonnath bones';
					}
					else if (strpos($items[$i],'Burnt') !== false) {
						$image = 'Burnt bones';
					}
					else if (strpos($items[$i],'Baby') !== false) {
						$image = 'Baby dragon bones';
					}
					else if (strpos($items[$i],'Monkey') !== false) {
						$image = 'Monkey bones';
					}
					else if (strpos($items[$i],'Big') !== false) {
						$image = 'Big bones';
					}
					else {
						$image = 'Bones';
					}
				}
				else {
					$image = $items[$i];
				}
				if ($product['type'] == '07') {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/07/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
				else {
				echo 	'<div class="product-item '.$class.'" id="selector-'.$selector.'">
						<div class="img"><img src="assets/images/rs3/'.$image.'.png" alt="'.$items[$i].'"/></div>
            			'.$items[$i].'
            			</div>';
				}
		}
?>
	</div> 
<div id="arrow"></div></div>
<script type="text/javascript">
$(function() {
	$("#arrow").animate({ 'borderRadius': '0', 'width': '2px' }, 1000, 'easeInOutExpo');
	
	setTimeout( function(){
			if ($(".open-case-wrap .product-item:first-child").animate({ 'marginLeft': '<? echo $marginpx;?>px'}, 11000, 'easeOutQuart')) {
			setTimeout( function(){
				$("#selector-<? echo $marginpxprep ?> ").animate({ 'borderRadius': '0' }, 1800, 'easeOutElastic');
				 $("#selector-<? echo $marginpxprep ?> ").effect( "pulsate", {times:6}, 1600 );
				setTimeout( function(){
		 		 $("#arrow").animate({ 'borderRadius': '130px', 'width': '450px' }, 1000, 'easeInOutExpo');
				}, 2800 );
        	}, 11000 );
			setTimeout( function(){
               //window.location = "inventory.php";
        	}, 13000 );
		}	 
    }, 800 );
    
});
</script>
</body>
</html>
            <?
		}
	}
}
else {
	header("Location: inventory.php");
	exit;
}
?>
