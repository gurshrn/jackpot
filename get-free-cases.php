<?php 

	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require 'config.php';
	
	$catId = $_POST['catId'];
	$subCatId = $_POST['subCatId'];
	
	$username = $_SESSION['usr'];
	
	
	$usercheck = $mysqli->prepare("SELECT id, usr FROM rsj_members WHERE usr=?");
	$usercheck->bind_param('s', $username);
	$usercheck->execute();
	$usercheck->store_result();
	$usercheck->bind_result($userid, $usr);
	$usercheck->fetch();
	
	

	$casequery = $mysqli->query("SELECT * FROM rsj_case WHERE cat_id='{$catId}' AND `case_type` = '{$subCatId}' AND `case_price` = 0 ");
	$caseproduct = mysqli_fetch_all($casequery,MYSQLI_ASSOC);
	
	$data = '';
	if(!empty($caseproduct))
	{
		
		foreach($caseproduct as $val)
		{
			$case_id = $val['id'];
			
			$productquery = $mysqli->query("SELECT * FROM rsj_case_items WHERE case_id = '{$case_id}'");
			$product = mysqli_fetch_all($productquery,MYSQLI_ASSOC);
			
			$caseItem = [];
			$itemColor = [];
			$itemImage = [];
			foreach($product as $val1)
			{
				$caseItem[] = $val1['item_name'];
				$itemColor[] = $val1['color'];
				$itemImage[] = $val1['item_image'];
			}
			
			$caseItem1 = implode(",",$caseItem);
			$itemcolors = implode(",",$itemColor);
			$itemimage = implode(",",$itemImage);
			
			$caseName = $val['case_name'];
			$casePrice = $val['case_price'];
			$caseColor = $val['case_color'];
			$caseType = $val['sub_category_name'];
			$caseQty = 1;
			$orderDate = gmdate("Y-m-d");
			
			$dt = gmdate("Y-m-d H:i:s");
			$completed = 'Completed';
			$p = 'case';
			$closed = 'closed';
			$tbd = 'tbd';
			$notused = 'Not used';
			$na = 'NA';
			$processid = 'freecase';
			$proof = 'Free Case';
			$totalAmt = 0;
			
			$results = $mysqli->query("SELECT * FROM rsj_order_payment ORDER BY id DESC LIMIT 1");
			$order = mysqli_fetch_all($results,MYSQLI_ASSOC);
			if(!empty($order))
			{
				$i = $order[0]['id']+1;
			}
			else
			{
				$i = 1;
			}
			
			$orderId = 'Freecase_'.$i;
			
			mysqli_query($mysqli,"INSERT INTO rsj_order_detail (order_id, case_name,case_items, case_color, item_color, item_image, case_type, case_price, case_qty,purchase_qty,order_date,status) VALUES ('".$orderId."','".$val['case_name']."','".$caseItem1."','".$caseColor."','".$itemcolors."','".$itemimage."','".$caseType."','".$casePrice."','".$caseQty."','".$caseQty."','".$orderDate."','".$closed."')");
			$lastId = $mysqli->insert_id;
			
			mysqli_query($mysqli,"INSERT INTO rsj_order_payment (txn_id, order_id, order_total,paid_amount, payment_status, payment_method,user_id, createdtime, ip, delivery, coupen) VALUES ('".$proof."','".$orderId."','".$totalAmt."','".$totalAmt."','".$completed."','".$processid."','".$userid."','".$dt."','".$na."','".$tbd."','".$notused."')");
			
			
			
			
			$data .= '<div class="closed-case-wrap"><div class="closed-case group '.$val['case_color'].'">
			 
			<form action="case.php" method="post"><input type="hidden" name="case_name" value="'.$val[ 'case_name' ].'">
			<input type="hidden" name="free_case" value="freecase"><input type="hidden" name="case_id" value="'.$val['id'].'">
			 <input type="hidden" name="case_price" value="0">
			 <input type="hidden" name="order_id" value="Freecase_'.$lastId.'"><input type="submit" class="open-case-button" value="Open Case" />
			</form><div>' . ucfirst($val[ 'case_name' ]) .' '.'(Free Case)</div></div></div>';
		}
		echo $data;
	}
	
	
	
?>