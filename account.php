<?php
define('INCLUDE_CHECK',true);
require 'config.php';
include 'GoogleRecaptcha.php';

include_once('session.php');
secure_session_start();

if($_POST['submit']=='Register') {
	$captcha = $_POST['g-recaptcha-response'];
	if(!empty($captcha)) {
		$cap = new GoogleRecaptcha();
        $verified = $cap->VerifyCaptcha($captcha);
		if($verified) {
			if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email']) && (!preg_match("/[^A-Za-z0-9]/", $_POST['username'])))
 {
				
				$_POST['username'] == $mysqli->real_escape_string($_POST['username']);
				$_POST['password'] == $mysqli->real_escape_string($_POST['password']);
				$_POST['email'] == $mysqli->real_escape_string($_POST['email']);
				$username = $_POST['username'];
				$password = $_POST['password'];
				$email = $_POST['email'];
				
				$stmt = $mysqli->prepare("SELECT usr FROM rsj_members WHERE usr=?");
				$stmt->bind_param('s', $username);
				$stmt->execute();
				$stmt->store_result();
				
				$stmt2 = $mysqli->prepare("SELECT email FROM rsj_members WHERE email=?");
				$stmt2->bind_param('s', $email);
				$stmt2->execute();
				$stmt2->store_result();
				
				if ($stmt->num_rows == 1) {
					$_SESSION['error']='Username is already in use.';
					$stmt->close();
					header("Location: register.php");
					exit;
				}
				else if ($stmt2->num_rows == 1) {
					$_SESSION['error']='E-mail is already in use.';
					$stmt2->close();
					header("Location: register.php");
					exit;
				}
				else {
					if (isset($_POST['email-notif']) ) {
						$broadcast = 'yes';
					}
					else {
						$broadcast = 'no';
					}
					$stmt = $mysqli->prepare("INSERT INTO rsj_members(usr,pass,email,regIP,dt,broadcast) VALUES(?, ?, ?, ?, NOW(), ?)");
					$stmt->bind_param("sssss", $_POST['username'], $_POST['password'], $_POST['email'], $_SERVER['REMOTE_ADDR'], $broadcast);
					if($stmt->execute()) {
							$stmt = $mysqli->prepare("SELECT id,usr FROM rsj_members WHERE usr=? AND pass=?");
							$stmt->bind_param("ss", $_POST['username'], $_POST['password']);
							$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result($id, $usr);
							$stmt->fetch();
							$_SESSION['usr']= $usr;
							$_SESSION['id'] = $id;
							$stmt->free_result();
							$stmt->close();
							if (isset($_POST['returnUrl'])) {
								$returnUrl = $_POST['returnUrl'];
								header("Location: ".$returnUrl);
								exit;
							}
							else {
								header("Location: index.php");
								exit;
							}
						}
						else {
							$_SESSION['error']='Something went wrong with the database. Contact Admin.';
							header("Location: register.php");
							exit;
						}
				}
			}
			else {
				$_SESSION['error']='Something went wrong.';
				header("Location: register.php");
				exit;
			}
		}
		else {
			$_SESSION['error']='Invalid reCAPTCHA.';
			header("Location: register.php");
			exit;
		}
	}
	else {
		$_SESSION['error']='Invalid reCAPTCHA.';
		header("Location: register.php");
		exit;
	}
}
else if ($_POST['submit']=='Login') {
	$_POST['email'] == $mysqli->real_escape_string($_POST['email']);
	$_POST['password'] == $mysqli->real_escape_string($_POST['password']);
	
	$password = $_POST['password'];
	$email = $_POST['email'];
	
	$stmt = $mysqli->prepare("SELECT id,usr FROM rsj_members WHERE email=? AND pass=?");
	$stmt->bind_param('ss', $email, $password);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $usr);
	$stmt->fetch();
	
	//$results = $mysqli->query("SELECT id,usr FROM rsj_members WHERE email='{$_POST['email']}' AND pass='{$_POST['password']}'");
	//$row = $results->fetch_assoc();
	
	if($stmt->num_rows == 1) {
			// If everything is OK login
			$_SESSION['usr']=$usr;
			$_SESSION['id'] = $id;
			$_SESSION['vip'] = 0;
		
		
			$vipquery = $mysqli->query("SELECT `status` FROM rsj_vip WHERE `userid` ={$id}");
			if ($vipquery->num_rows != 0) {
				$status = $vipquery->fetch_object()->status;
				
				$vipdisc = $mysqli->query("SELECT `content` FROM rsj_editables WHERE `name` ='vipdiscount'")->fetch_object()->content;
				if ($status == 'active') {
					$_SESSION['vip'] = $vipdisc/100;
				}
				
				if(isset($_SESSION['guestSession']) && !empty($_SESSION['guestSession']))
				{
					$mysqli->query("Update rsj_cart SET `session` = '{$_SESSION['id']}', `type` = 'Logged' WHERE `session` = '{$_SESSION['guestSession']}'");
					unset($_SESSION['guestSession']);
				}
			}
		
			if (isset($_POST['returnUrl'])) {
				$returnUrl = $_POST['returnUrl'];
				header("Location: ".$returnUrl);
				exit;
			}
			else {
			header("Location: index.php");
			exit;
			}
	}
	else {
		$_SESSION['error']='Invalid E-mail or Password.';
		header("Location: login.php");
		exit;
	}
	
}
else if (isset($_GET['logout'])) {	
	$_SESSION = array();
 
				// get session parameters 
				$params = session_get_cookie_params();
 
				// Delete the actual cookie. 
				setcookie(session_name(),
        		'', time() - 42000, 
        		$params["path"], 
        		$params["domain"], 
        		$params["secure"], 
        		$params["httponly"]);
 
				// Destroy session 
				session_destroy();
			   	header("Location: index.php");
               	exit;
}
else {
	$_SESSION['error']='Form was not submitted using Register or Login page from this website.';
	header("Location: register.php");
	exit;
}
?>