// JavaScript Document by Fuser @ Fuser Designs
$(function() {
	$("#nav-icon").click(function() {
		$("#nav").slideToggle("slow");
	});
	$("#items-slider").owlCarousel({		
		itemsCustom : [
        [0, 2],
        [450, 3],
        [640, 4],
        [800, 3],
        [955, 3],
        [1200, 3],
        [1300, 4],
        [1600, 5]
      ]
	});
});

function validate() {
	"use strict";
	var username = document.forms['register']['username'].value;
	var password = document.forms['register']['password'].value;
	var confirmpassword = document.forms['register']['cnf-password'].value;
	if (username.length < 6) {
		$(".error").slideUp("fast");
		$("#error-code-1").slideDown("fast");
		return false;
	}
	if (password.length < 8) {
		$(".error").slideUp("fast");
		$("#error-code-2").slideDown("fast");
		return false;
	}
	if (password !== confirmpassword) {
		$(".error").slideUp("fast");
		$("#error-code-3").slideDown("fast");
		return false;
	}
}