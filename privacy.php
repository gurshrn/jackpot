<?php
include_once('session.php');
secure_session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Privacy policy</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.1" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>
		<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		
		 echo '</ul> 

		</div>
		</div>'; ?>
<div class="content-wrap">
  <div class="page-title">RSJACKPOT PRIVACY POLICY</div>
  <div class="main-content">
    <p>RSJackpot (<strong>"Company"</strong> or <strong>"We"</strong>) respects your privacy, and is committed to
        protecting it through our
        compliance with this policy. This policy describes the types of information We may collect from you or that you
        may provide when you visit the website rsjackpot.org (our <strong>"Website"</strong>) and our practices for
        collecting, using,
        maintaining, protecting and disclosing that information.</p>
    <ul>
        <li>
            <ul>
                This policy applies to information We collect:
                <li>On this Website.</li>
                <li>In e-mail, text and other electronic messages between you and this Website.</li>
                <li>Through mobile and desktop applications you download from this Website, which provide dedicated
                    non-browser-based interaction between you and this Website.
                </li>
                <li>When you interact with our advertising and applications on third-party websites and services, if
                    those applications or advertising include links to this policy.
                </li>
            </ul>
        </li>
        <li>
            <ul>
                It does not apply to information collected by:
                <li>Us offline or through any other means, including on any other website operated by the Company or any
                    third party (including our affiliates and subsidiaries); or
                </li>
                <li>Any third party (including our affiliates and subsidiaries), including through any application or
                    content (including advertising) that may link to or be accessible from or on the Website.
                </li>
            </ul>
        </li>
    </ul>
    <p>Please read this policy carefully to understand our policies and practices regarding your information and how we
        will treat it. If you do not agree with our policies and practices, your choice is not to use our Website. By
        accessing or using this Website, you agree to this Privacy Policy. This policy may change from time to time.
        Your continued use of this Website after we make changes is deemed to be acceptance of those changes, so please
        check the policy periodically for any updates. </p>
    <h3 class="font-color-blue">
        Children Under the Age of 13
    </h3>
    <p>Our Website is not intended for children under 13 years of age. No one under age 13 may provide any personal
        information to or on the Website. We do not knowingly collect personal information from children under 13. If
        you are under 13, do not use or provide any information on this Website or on or through any of its features,
        register on the Website, make any purchases through the Website, use any of the interactive or public comment
        features of this Website or provide any information about yourself to us, including your name, address,
        telephone number, e-mail address or any screen name or user name you may use. If we learn we have collected or
        received personal information from a child under 13 without verification of parental consent, we will delete
        that information. </p>
    <h3 class="font-color-blue">Information We Collect About You and How We Collect It</h3>
    <p>From time to time, We may collect several types of information from and about users of our Website, including
        information:</p>
    <ul>
        <li>By which you may be personally identified, such as name, screen name, postal address, e-mail address, and
            telephone number or ANY OTHER INFORMATION THE WEBSITE COLLECTS THAT IS DEFINED AS PERSONAL OR PERSONALLY
            IDENTIFIABLE INFORMATION UNDER AN APPLICABLE LAW ("Personal Information");
        </li>
        <li>That is about you individually, but does not identify you; and</li>
        <li>About your internet connection, the equipment you use to access our Website and usage details.</li>
    </ul>
    <p>We collect this information:</p>
    <ul>
        <li>Directly from you when you provide it to us;</li>
        <li>Automatically as you navigate through the site. Information collected automatically may include usage
            details, IP addresses and information collected through cookies, web beacons and other tracking
            technologies; and
        </li>
        <li>From third parties, for example, our business partners.</li>
    </ul>
    <h3 class="font-color-blue">Information You Provide to Us.</h3>
    <p>The information We collect on or through our Website may include:</p>
    <ul>
        <li>Information that you provide by filling in forms on our Website. This includes information provided at the
            time of registering to use our Website, subscribing to our service, posting material, or requesting further
            services. We may also ask you for information when you enter a contest or promotion sponsored by us, and
            when you report a problem with our Website.
        </li>
        <li>Records and copies of your correspondence (including e-mail addresses), if you contact us.</li>
        <li>Your responses to surveys that we might ask you to complete for research purposes.</li>
        <li>Details of transactions you carry out through our Website and of the fulfillment of your orders. You may be
            required to provide financial information before placing an order through our Website.
        </li>
        <li>Your search queries on the Website.</li>
    </ul>
    <p>You also may provide information to be published or displayed (hereinafter, <strong>"posted"</strong>) on public
        areas of the
        Website, or transmitted to other users of the Website or third parties (collectively, <strong>"User
            Contributions"</strong>).
        Your User Contributions are posted on and transmitted to others at your own risk. Additionally, we cannot
        control the actions of other users of the Website with whom you may choose to share your User Contributions.
        Therefore, we cannot and do not guarantee that your User Contributions will not be viewed by unauthorized
        persons.</p>
    <h3 class="font-color-blue">Information We Collect Through Automatic Data Collection Technologies.</h3>
    <p>As you navigate through and interact with our Website, We may use automatic data collection technologies to
        collect certain information about your equipment, browsing actions and patterns, including:</p>
    <ul>
        <li>Details of your visits to our Website, including traffic data, location data, logs and other communication
            data and the resources that you access and use on the Website.
        </li>
        <li>Information about your computer and internet connection, including your IP address, operating system and
            browser type.
        </li>
    </ul>
    <p>The information we collect automatically is statistical data and may include personal information, but we may
        maintain it or associate it with personal information we collect in other ways or receive from third parties. It
        helps us to improve our Website and to deliver a better and more personalized service, including by enabling us
        to:</p>
    <ul>
        <li>Estimate our audience size and usage patterns.</li>
        <li>Store information about your preferences, allowing us to customize our Website according to your individual
            interests.
        </li>
        <li>Speed up your searches.</li>
        <li>Recognize you when you return to our Website.</li>
    </ul>
    <p>The technologies we use for this automatic data collection may include:</p>
    <ul>
        <li><strong>Cookies (or browser cookies).</strong> A cookie is a small file placed on the hard drive of your
            computer. You may
            refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you
            select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your
            browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser
            to our Website.
        </li>
        <li><strong>Flash Cookies.</strong> Certain features of our Website may use local stored objects (or Flash
            cookies) to collect
            and store information about your preferences and navigation to, from and on our Website. Flash cookies are
            not managed by the same browser settings as are used for browser cookies.
        </li>
        <li><strong>Web Beacons.</strong> Pages of our the Website and our e-mails may contain small electronic files
            known as web
            beacons (also referred to as clear gifs. pixel tags and single-pixel gifs) that permit the Company, for
            example, to count users who have visited those pages or opened an e-mail and for other related website
            statistics (for example, recording the popularity of certain website content and verifying system and server
            integrity).
        </li>
    </ul>
    <p>We do not collect personal Information automatically, but we may tie this information to personal information
        about you that we collect from other sources or you provide to us.</p>
    <h3 class="font-color-blue">Third-party Use of Cookies and Other Tracking Technologies</h3>
    <p>Some content or applications, including advertisements, on the Website may be served by third-parties, including
        advertisers, ad networks and servers, content providers and application providers. These third parties may use
        cookies alone or in conjunction with web beacons or other tracking technologies to collect information about you
        when you use our Website. The information they collect may be associated with your personal information or they
        may collect information, including personal information, about your online activities over time and across
        different websites and other online services. They may use this information to provide you with interest-based
        (behavioral) advertising or other targeted content. </p>
    <p>We do not control these third parties tracking technologies or how they may be used. If you have any questions
        about an advertisement or other targeted content, you should contact the responsible provider directly. </p>
    <h3 class="font-color-blue">How We Use Your Information</h3>
    <ul>
        <li>We use information that we collect about you or that you provide to us, including any personal
            information:
        </li>
        <li>To present our Website and its contents to you.</li>
        <li>To provide you with information, products or services that you request from us.</li>
        <li>To fulfill any other purpose for which you provide it.</li>
        <li>To provide you with notices about your account.</li>
        <li>To carry out our obligations and enforce our rights arising from any contracts entered into between you and
            us, including for billing and collection.
        </li>
        <li>To notify you about changes to our Website or any products or services we offer or provide though it.</li>
        <li>To allow you to participate in interactive features on our Website.</li>
        <li>For any other purpose with your consent.</li>
    </ul>
    <p>We may also use your information to contact you about our own and third-parties goods and services that may be
        of interest to you. We may use the information we have collected from you to enable us to display advertisements
        to our advertisers target audiences. Even though we do not disclose your personal information for these
        purposes without your consent, if you click on or otherwise interact with an advertisement, the advertiser may
        assume that you meet its target criteria.</p>
    <h3 class="font-color-blue">
        Disclosure of Your Information
    </h3>
    <p>We may disclose aggregated information about our users, and information that does not identify any individual,
        without restriction. We may disclose personal information that we collect or you provide as described in this
        privacy policy:</p>
    <ul>
        <li>To our subsidiaries and affiliates.</li>
        <li>To contractors, service providers and other third parties we use to support our business and who are bound
            by contractual obligations to keep personal information confidential and use it only for the purposes for
            which we disclose it to them.
        </li>
        <li>To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization,
            dissolution or other sale or transfer of some or all of the Company’s assets, whether as a going concern or
            as part of bankruptcy, liquidation or similar proceeding, in which personal information held by the Company
            about our Website users is among the assets transferred.
        </li>
    </ul>
    <p>We may also disclose your personal information:</p>
    <ul>
        <li>To comply with any court order, law or legal process, including to respond to any government or regulatory
            request.
        </li>
        <li>To enforce or apply our Terms of Use and other agreements, including for billing and collection purposes.
        </li>
        <li>If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of the
            Company’s customers or others.
        </li>
    </ul>
    <h3 class="font-color-blue">Choices About How We Use and Disclose Your Information</h3>
    <p>We strive to provide you with choices regarding the personal information you provide to us. We have created
        mechanisms to provide you with the following control over your information: </p>
    <ul>
        <li><strong>Tracking Technologies and Advertising.</strong> You can set your browser to refuse all or some
            browser cookies, or to
            alert you when cookies are being sent. If you disable or refuse cookies, please note that some parts of this
            site may then be inaccessible or not function properly.
        </li>
        <li><strong>Disclosure of Your Information for Third-Party Advertising.</strong> If you do not want us to share
            your personal
            information with unaffiliated or non-agent third parties for promotional purposes, you can opt-out by
            sending an e-mail stating your request.
        </li>
        <li><strong>Promotional Offers from the Company.</strong> If you do not wish to have your contact information
            used by the Company
            to promote our own or third parties products or services, you can opt-out by by sending an e-mail stating
            your request.
        </li>
        <li><strong>Targeted Advertising.</strong> If you do not want us to use information that We collect or that you
            provide to us to
            deliver advertisements according to our advertisers target-audience preferences, you can opt-out by sending
            an e-mail stating your request.
        </li>
    </ul>
    <h3 class="font-color-blue">Accessing and Correcting Your Information</h3>
    <p>You may also send us an e-mail to request access to, correct or delete any personal information that you have
        provided to us. We cannot delete your personal information except by also deleting your user account. We may not
        accommodate a request to change information if We believe the change would violate any law or legal requirement
        or cause the information to be incorrect.</p>
    <p>If you delete your User Contributions from the Website, copies of your User Contributions may remain viewable in
        cached and archived pages, or might have been copied or stored by other Website users. Proper access and use of
        information provided on the Website, including User Contributions, is governed by our Terms of Use. </p>
    <h3 class="font-color-blue">Your California Privacy Rights</h3>
    <p>California Civil Code Section § 1798.83 permits users of our Website that are California residents to request
        certain information regarding our disclosure of personal information to third parties for their direct marketing
        purposes. To request your personal information, please send us an e-mail request. </p>
    <h3 class="font-color-blue">Data Security</h3>
    <p>We have implemented measures designed to secure your personal information from accidental loss and from
        unauthorized access, use, alteration and disclosure. All information you provide to us is stored on our secure
        servers behind firewalls. Any payment transactions will be encrypted using SSL technology.</p>
    <p>The safety and security of your information also depends on you. Where We have given you (or where you have
        chosen) a password for access to certain parts of our Website, you are responsible for keeping this password
        confidential. We ask you not to share your password with anyone. We urge you to be careful about giving out
        information in public areas of the Website like message boards. The information you share in public areas may be
        viewed by any user of the Website.</p>
    <p>Unfortunately, the transmission of information via the Internet is not completely secure. Although We do our best
        to protect your personal information, We cannot guarantee the security of your personal information transmitted
        to our Website. Any transmission of personal information is at your own risk. We are not responsible for
        circumvention of any privacy settings or security measures contained on the Website. </p>
    <h3 class="font-color-blue">Changes to Our Privacy Policy</h3>
    <p>It is our policy to post any changes we make to our Privacy Policy on this page. If we make material changes to
        how we treat our users personal information, we will notify you by e-mail to the primary e-mail address
        specified in your account. You are responsible for ensuring we have an up-to-date active and deliverable e-mail
        address for you, and for periodically visiting our Website and this Privacy Policy to check for any changes.</p>
    </div>
</div>
    
            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
