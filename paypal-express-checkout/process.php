<?php
include_once('../session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include_once("../config.php");
require_once ("paypalfunctions.php");
if(isset($_SESSION['id'])) {
	if (isset($_SESSION['coupon'])) {
			unset($_SESSION['coupon']);
	}
	$_SESSION['coupon'] = 'Not used';
	$discount = 0;
	if (isset($_POST['coupon']) && !empty($_POST['coupon'])) {
		
		$coupon = $mysqli->real_escape_string($_POST['coupon']);
		//$couponquery = $mysqli->query("SELECT * FROM `rsj_coupons` WHERE `coupon` = '{$coupon}'");
		//$couponresults = $couponquery->fetch_assoc();
		
		
		$stmt = $mysqli->prepare("SELECT `id`, `coupon`, `discount`, `status` FROM `rsj_coupons` WHERE `coupon` =?");
		$stmt->bind_param("s", $coupon);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($couponid, $couponcontent, $coupondiscount, $couponstatus);
		$stmt->fetch();		
		
		if (($stmt->num_rows == 0) or ($couponstatus == 'inactive')) {
			echo '<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Login Required</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
</style>
</head>
<body>
Invalid/Expired Coupon code.<br>
<br>
<a class="button" onClick="window.close();">
Close
</a><br>
</body>
</html>';
exit;
		}
		else {
			$discount = $coupondiscount;
			$_SESSION['coupon'] = $couponcontent;
		}
		
	$stmt->close();
	}
$PaymentOption = "PayPal";
if ( $PaymentOption == "PayPal")
{
    // ==================================
    // PayPal Express Checkout Module
    // ==================================



    //'------------------------------------
    //' The paymentAmount is the total value of
    //' the purchase.
    //'
    //' TODO: Enter the total Payment Amount within the quotes.
    //' example : $paymentAmount = "15.00";
    //'------------------------------------

    $paymentAmount = 0;
    /*foreach($_SESSION["products"] as $cart_itm){
        $paymentAmount += $cart_itm["price"];
    }*/



    //'------------------------------------
    //' The currencyCodeType
    //' is set to the selections made on the Integration Assistant
    //'------------------------------------
    $currencyCodeType = "USD";
    $paymentType = "Sale";

    //'------------------------------------
    //' The returnURL is the location where buyers return to when a
    //' payment has been succesfully authorized.
    //'
    //' This is set to the value entered on the Integration Assistant
    //'------------------------------------
    $returnURL = "http://rsjackpot.org/success.php";

    //'------------------------------------
    //' The cancelURL is the location buyers are sent to when they hit the
    //' cancel button during authorization of payment during the PayPal flow
    //'
    //' This is set to the value entered on the Integration Assistant
    //'------------------------------------
    $cancelURL = "http://rsjackpot.org/cancel.php";

    //'------------------------------------
    //' Calls the SetExpressCheckout API call
    //'
    //' The CallSetExpressCheckout function is defined in the file PayPalFunctions.php,
    //' it is included at the top of this file.
    //'-------------------------------------------------


    /*$items = array();


    foreach($_SESSION["products"] as $cart_itm){
        $items[] = array('name' => $cart_itm["name"], 'amt' => $cart_itm["price"], 'qty' => $cart_itm["qty"]);
        $paymentAmount+=$cart_itm["price"]*$cart_itm["qty"];
    }*/
	$p = $_POST['p'];
	$postproductid = $_POST['productid'];
	$quantity = $_POST['quantity'];
	$_SESSION['p'] = $p;
	$_SESSION['productid'] = $postproductid;
	$_SESSION['quantity'] = $quantity;
	if ($p == 'case') {
		//$result = $mysqli->query("SELECT * FROM rsj_products WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?");
	}
	else if ($p == 'pack') {
		//$result = $mysqli->query("SELECT * FROM rsj_packs WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_packs WHERE id=?");
	}
	else if ($p == 'coin') {
		//$result = $mysqli->query("SELECT * FROM rsj_coins WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_coins WHERE id=?");
	}
	
		$stmt->bind_param("s", $postproductid);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($productid, $productname, $productdesc, $productitems, $productprice, $producttype);
		$stmt->fetch();
		$stmt->close();
		
	//$product = $result->fetch_assoc();
	$absolutediscountx = 0;
	
	// check if product is in flash sale (makes any tampered coupon submitted useless)
	$flashquery = $mysqli->query("SELECT * FROM rsj_flash WHERE id='1'");
	$flashdata = $flashquery->fetch_assoc();
	
	$gmt = new DateTimeZone('GMT');
		
	$startdt = new DateTime($flashdata['startdt'], $gmt);
		
	$enddt = new DateTime($flashdata['enddt'], $gmt);
		
	$currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
	
	
	if (($flashdata['p'] == $p) && ($flashdata['productid'] == $productid) && ($flashdata['visibility'] == 'enable') && ($startdt < $currentdt) && ($currentdt < $enddt)) {
		$discount = $flashdata['discount'];
	}
	
	if ($discount !== 0) {
		$absolutediscount = $productprice*($discount/100);
		$absolutediscountx =  number_format($absolutediscount, 2, '.', '');
	}
	$finalprice = $productprice-$absolutediscountx;
	$finalpricewithvip = number_format($finalprice - $finalprice*$_SESSION['vip'], 2);
	
	$items = array();
	$items[] = array('name' => $productname, 'amt' => $finalpricewithvip, 'qty' => $quantity);
	
	
	$paymentAmount = $finalpricewithvip*$quantity;
	
	
    //::ITEMS::

    // to add anothe item, uncomment the lines below and comment the line above
    // $items[] = array('name' => 'Item Name1', 'amt' => $itemAmount1, 'qty' => 1);
    // $items[] = array('name' => 'Item Name2', 'amt' => $itemAmount2, 'qty' => 1);
    //$paymentAmount = $itemAmount1 + $itemAmount2;

    // assign corresponding item amounts to "$itemAmount1" and "$itemAmount2"
    // NOTE : sum of all the item amounts should be equal to payment  amount

    $resArray = SetExpressCheckoutDG( $paymentAmount, $currencyCodeType, $paymentType,
        $returnURL, $cancelURL, $items );

    $ack = strtoupper($resArray["ACK"]);
    if($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING")
    {
        $token = urldecode($resArray["TOKEN"]);
        RedirectToPayPalDG( $token );
    }
    else
    {
        //Display a user friendly Error on the page using any of the following error information returned by PayPal
        $ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
        $ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
        $ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
        $ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

        echo "SetExpressCheckout API call failed. ";
        echo "Detailed Error Message: " . $ErrorLongMsg;
        echo "Short Error Message: " . $ErrorShortMsg;
        echo "Error Code: " . $ErrorCode;
        echo "Error Severity Code: " . $ErrorSeverityCode;
    }
}



/*
/**
 * If the cart has items.. then it will find their existing order in the database (if there is one) and delete, replacing it with the current order
 * this is useful because if they go to checkout and decide they don't want something, or want something else - it won't be an issue.
 *
 * As there are no user accounts, orders are stored by session id, once we start implementing the IPN, I hope it is possible to take this further
 * and store their paypal email with their order too.

if(isset($_SESSION["products"])){
    $sessionid = session_id();
    $results = $mysqli->query("SELECT * FROM orders WHERE (`session_id` = '$sessionid') AND (`pay_status` = 0) ORDER BY id ASC");
    $order = false;
    $orderid;
    if($results){
        while($obj = $results->fetch_object()){
            if($order==false){
                $order = true;
                $orderid = $obj->id;
            } else {
                //deletes duplicate unpaid orders
                $mysqli->query("DELETE FROM orders WHERE (`id` = '$obj->id')");
                $mysqli->query("DELETE FROM order_products WHERE (`orderID` = '$obj->id')");
            }

        }
    }

    if($order==false){
    }

    if($order){
        $mysqli->query("DELETE FROM order_products WHERE (`orderID` = $orderid)");
        $mysqli->query("DELETE FROM order_products WHERE (`orderID` = '$orderid')");
        foreach($_SESSION["products"] as $cart_itm){
            $name 	= filter_var($cart_itm["name"], FILTER_SANITIZE_STRING);
            $quantity 	= filter_var($cart_itm["qty"], FILTER_SANITIZE_NUMBER_INT);
             $price = $cart_itm["price"]; //CHECK PRICES!!!
            $price*=$quantity;

            $mysqli->query("INSERT INTO order_products VALUES (".$orderid.", '".$name."', ".$price.", ".$quantity.")");
        }
    } else {
        $mysqli->query("INSERT INTO orders (session_id) VALUES ('".session_id()."')");
        $orderid = $mysqli->insert_id;
        $mysqli->query("DELETE FROM order_products WHERE (`orderID` = $orderid)");
        foreach($_SESSION["products"] as $cart_itm){
            $name 	= filter_var($cart_itm["name"], FILTER_SANITIZE_STRING);
            $quantity 	= filter_var($cart_itm["qty"], FILTER_SANITIZE_NUMBER_INT);
            $price = $cart_itm["price"]; //CHECK PRICES!!!
            $price*=$quantity;

            $mysqli->query("INSERT INTO order_products VALUES (".$orderid.", '".$name."', ".$price.", ".$quantity.")");
        }
    }

}

function validatePrice($item){

}

*/
}
else {
	echo '<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Login Required</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
</style>
</head>
<body>
You must be Logged in to continue.<br>
<br>

<a class="button" onClick="window.close(); opener.location.href = \'http://rsjackpot.org/login.php\';">
Login
</a><br>
<br>
OR<br>
<br>

<a class="button" onClick="window.close(); opener.location.href = \'http://rsjackpot.org/register.php\';">
Register
</a>
</body>
</html>
';
}
?>
