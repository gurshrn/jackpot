<?php 

	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require 'config.php';
	
	$catId = $_POST['catId'];
	
	$result = $mysqli->query("SELECT * from `rsj_sub_category` WHERE `cat_id` = '{$catId}'");
	$subCats = mysqli_fetch_all($result,MYSQLI_ASSOC);
	
	$results = $mysqli->query("SELECT rsj_case.id,rsj_case.case_name,rsj_case.case_price,rsj_sub_category.sub_category_name as case_type,rsj_case.case_color FROM rsj_case INNER JOIN rsj_sub_category ON rsj_case.case_type = rsj_sub_category.id WHERE rsj_case.cat_id='{$catId}' AND rsj_case.case_price != 0");
	$products = mysqli_fetch_all($results,MYSQLI_ASSOC);
	
	$data .= '<div class="type-nav">';
	
	if(isset($subCats) && !empty($subCats))
	{
		foreach($subCats as $vals)
		{
			$data .= '<a href="javascript:void(0)" class="subCategories" data-target="'.$vals['id'].'" data-attr="'.$vals['cat_id'].'">'.$vals['sub_category_name'].'</a>';
		}
		
		$data .= '</div><div class="allResult">';
	
	
		foreach($products as $val)
		{
			$results1 = $mysqli->query("SELECT * FROM rsj_case_items WHERE case_id='{$val['id']}'");
			$caseItems = $results1->fetch_all(MYSQLI_ASSOC);
			
			
				
			
			$data .='<div class="product"><div class="content group '.' '.$val['case_color'].'"><div class="title">' . ucfirst($val['case_name']) .' '.'('.$val['case_type'].')</div><a href="javascript:void(0)" data-attr="' . $val['case_name'] . '" data-qty="1" data-type="case" data-target="' . $val['id'] . '" class="addToCart">Add to cart</a><a href="product.php?p=case&id=' . $val['id'] . '" class="price"> $' . number_format($val['case_price'], 2) .'</a><div class="desc"> Case could contain:<div id="items-slider-' . $val['id'] . '" class="items-slider">';
			
			
			
			foreach ($caseItems as $item) 
			{
				$data .= '<div class="item ' . $item['color'] . '-c"><img height="30" width="40" src="admin/upload/' . $item['item_image'] . '" alt="' . $item['item_image'] . '"/><span>' . $item['item_unique_name'] . '</span></div>';
			}
			
			$data .='<script type="text/javascript">
						$("#items-slider-' . $val['id'] . '").owlCarousel({
							itemsCustom : [
							[0, 2],
								[450, 3],
							[640, 4],
							[800, 3],
							[955, 3],
							[1200, 3],
							[1300, 4],
							[1600, 5]
							  ]
						});
				</script></div><span class="stamp-' . $val['case_type'] . '">' . $val['case_type'] . '</span></div></div></div>';
			
		}
		$data .= '</div>';
	}
	else
	{
		$data .= 'No Product found...';
	}
	
	echo $data;