<?
include_once( 'session.php' );
secure_session_start();
define( 'INCLUDE_CHECK', true );
require 'config.php';
if ( !isset( $_SESSION[ 'id' ] ) ) {
	$_SESSION[ 'returnUrl' ] = $_SERVER[ 'REQUEST_URI' ];
	header( "Location: login.php" );
	exit;
	
}
//print_r($_POST);die;
if ( isset( $_POST) && !empty($_POST)) {
	
	print_r($_POST);die;

	$incomingurl = $_SERVER[ "HTTP_REFERER" ];
	$refData = parse_url( $incomingurl );
	if ( $refData[ 'host' ] == 'customer-devreview.com' ) {
		$fccheck = $mysqli->query( "SELECT content FROM rsj_editables WHERE name = 'freecases'" )->fetch_object()->content;
		if ( $fccheck == 'enabled' ) { //PRE-CONDITION: Check if free cases are enabled
			$stmt = $mysqli->prepare("SELECT lastfreecase FROM rsj_members WHERE `id` =?");
			$stmt->bind_param("s", $_SESSION['id']);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($lastfc);
			$stmt->fetch();
			$stmt->close();
			
			//CONDITION 1: Give case for the first time			
			if ( $lastfc == '0000-00-00 00:00:00' ) {

				$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
				$stmt->bind_param( "s", $_SESSION[ 'id' ] );
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result( $lastfc );
				$stmt->fetch();
				$stmt->close();

				if ( isset( $_POST[ 'claimfc07' ] ) ) {
					$productid = '35';

					$stmt = $mysqli->prepare( "SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?" );
					$stmt->bind_param( "s", $productid );
					$stmt->execute();
					$stmt->store_result();
					$stmt->bind_result( $productid, $productname, $productdesc, $productitems, $productprice, $producttype );
					$stmt->fetch();
					$stmt->close();

					$items = explode( ',', $productitems );

					$rand1 = rand( 1, 2001 );

					if ( ( 1 <= $rand1 ) && ( $rand1 <= 1000 ) ) {
						// Blue
						$i = rand( 0, 4 );
					} else if ( ( 1001 <= $rand1 ) && ( $rand1 <= 1840 ) ) {
						// Purple
						$i = rand( 5, 7 );
					} else if ( ( 1841 <= $rand1 ) && ( $rand1 <= 1980 ) ) {
						// Pink
						$i = rand( 8, 10 );
					} else if ( ( 1981 <= $rand1 ) && ( $rand1 <= 2000 ) ) {
						// Red
						$i = rand( 11, 12 );
					} else if ( $rand1 = 2001 ) {
						// Red
						$i = 13;
					}
					$item = $items[ $i ];

					$transactionId = 'Free Case';
					$amt = '0.00';
					$paymentStatus = 'Completed';
					$casestatus = 'closed';
					$dt = gmdate( "Y-m-d H:i:s" );
					$ip = $_SERVER[ 'REMOTE_ADDR' ];
					$delivery = 'tbd';
					$coupon = 'Not Applied';
					$p = 'case';
					
					

					$stmt = $mysqli->prepare( "INSERT INTO rsj_payments (user_id,txn_id, order_total,paid_amount, payment_status, createdtime, ip, coupen, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
					$stmt->bind_param( "ssssssssssssss", $_SESSION[ 'id' ],$transactionId, $amt,$amt, $paymentStatus, $dt, $ip, $coupon, $casestatus);
					$stmt->execute();
					$stmt->close();

					$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
					$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
					$stmt->execute();
					$stmt->close();
					header( "Location: inventory.php" );
					exit;
				} 
				else if ( isset( $_POST[ 'claimfcrs3' ] ) ) 
				{
					$productid = '36';

					$stmt = $mysqli->prepare( "SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?" );
					$stmt->bind_param( "s", $productid );
					$stmt->execute();
					$stmt->store_result();
					$stmt->bind_result( $productid, $productname, $productdesc, $productitems, $productprice, $producttype );
					$stmt->fetch();
					$stmt->close();

					$items = explode( ',', $productitems );

					$rand1 = rand( 1, 2001 );

					if ( ( 1 <= $rand1 ) && ( $rand1 <= 1000 ) ) {
						// Blue
						$i = rand( 0, 4 );
					} else if ( ( 1001 <= $rand1 ) && ( $rand1 <= 1840 ) ) {
						// Purple
						$i = rand( 5, 7 );
					} else if ( ( 1841 <= $rand1 ) && ( $rand1 <= 1980 ) ) {
						// Pink
						$i = rand( 8, 10 );
					} else if ( ( 1981 <= $rand1 ) && ( $rand1 <= 2000 ) ) {
						// Red
						$i = rand( 11, 12 );
					} else if ( $rand1 = 2001 ) {
						// Red
						$i = 13;
					}
					$item = $items[ $i ];

					$transactionId = 'Free Case';
					$amt = '0.00';
					$paymentStatus = 'Completed';
					$casestatus = 'closed';
					$dt = gmdate( "Y-m-d H:i:s" );
					$ip = $_SERVER[ 'REMOTE_ADDR' ];
					$delivery = 'tbd';
					$coupon = 'Not Applied';
					$p = 'case';

					$stmt = $mysqli->prepare( "INSERT INTO rsj_order_payment (txnid, payment_amount, payment_status, productid, product, color, productname, case_status, item, userid, createdtime, ip, delivery, coupon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
					$stmt->bind_param( "ssssssssssssss", $transactionId, $amt, $paymentStatus, $productid, $p, $productdesc, $productname, $casestatus, $item, $_SESSION[ 'id' ], $dt, $ip, $delivery, $coupon );
					$stmt->execute();
					$stmt->close();

					$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
					$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
					$stmt->execute();
					$stmt->close();
					header( "Location: inventory.php" );
					exit;
				}

			} 
			else 
			{ //CONDITION 2: If not the first time, proceed with last purchase check

				$purchasecheck = $mysqli->query( "SELECT * FROM rsj_order_payment WHERE user_id = '{$_SESSION['id']}' AND payment_status='Completed' AND txn_id = 'Free Case'" )->num_rows;

				if ( $purchasecheck == 0 ) 
				{ //Outcome: No purchase on record
					header( "Location: login.php" );
					exit;
				} 
				else 
				{ //Outcome: We have purchase on record, lets see if it meets next condition

					//CONDITION 3: Check if the last purchase was within 1 month
					$lastpurchasedt = $mysqli->query( "SELECT createdtime FROM rsj_order_payment WHERE userid = '{$_SESSION['id']}' AND payment_status='Completed' AND txn_id = 'Free Case' ORDER BY rsj_order_payment.id DESC LIMIT 1" )->fetch_object()->createdtime;

					$gmt = new DateTimeZone( 'GMT' );
					$currentdt = new DateTime( gmdate( "Y-m-d H:i:s" ), $gmt );

					$checkpurchasedt = new DateTime( $lastpurchasedt, $gmt );
					$checkpurchasedt->modify( '+30 day' );

					if ( $currentdt < $checkpurchasedt ) 
					{ //Outcome: It is within 1 month

						//CONDITION 4: Check if Daily Free Case was claimed already
						$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
						$stmt->bind_param( "s", $_SESSION[ 'id' ] );
						$stmt->execute();
						$stmt->store_result();
						$stmt->bind_result( $lastfc );
						$stmt->fetch();
						$stmt->close();

						$gmt = new DateTimeZone( 'GMT' );
						$currentdt = new DateTime( gmdate( "Y-m-d H:i:s" ), $gmt );

						$lastfcdt = new DateTime( $lastfc, $gmt );

						$checkdt = new DateTime( $lastfc, $gmt );
						$checkdt->modify( '+1 day' );


						if ( $currentdt > $checkdt ) 
						{
							$stmt = $mysqli->prepare( "SELECT lastfreecase FROM rsj_members WHERE `id` =?" );
							$stmt->bind_param( "s", $_SESSION[ 'id' ] );
							$stmt->execute();
							$stmt->store_result();
							$stmt->bind_result( $lastfc );
							$stmt->fetch();
							$stmt->close();

							if ( isset( $_POST[ 'claimfc07' ] ) ) {
								$productid = '35';

								$stmt = $mysqli->prepare( "SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?" );
								$stmt->bind_param( "s", $productid );
								$stmt->execute();
								$stmt->store_result();
								$stmt->bind_result( $productid, $productname, $productdesc, $productitems, $productprice, $producttype );
								$stmt->fetch();
								$stmt->close();

								$items = explode( ',', $productitems );

								$rand1 = rand( 1, 2001 );

								if ( ( 1 <= $rand1 ) && ( $rand1 <= 1000 ) ) {
									// Blue
									$i = rand( 0, 4 );
								} else if ( ( 1001 <= $rand1 ) && ( $rand1 <= 1840 ) ) {
									// Purple
									$i = rand( 5, 7 );
								} else if ( ( 1841 <= $rand1 ) && ( $rand1 <= 1980 ) ) {
									// Pink
									$i = rand( 8, 10 );
								} else if ( ( 1981 <= $rand1 ) && ( $rand1 <= 2000 ) ) {
									// Red
									$i = rand( 11, 12 );
								} else if ( $rand1 = 2001 ) {
									// Red
									$i = 13;
								}
								$item = $items[ $i ];

								$transactionId = 'Free Case';
								$amt = '0.00';
								$paymentStatus = 'Completed';
								$casestatus = 'closed';
								$dt = gmdate( "Y-m-d H:i:s" );
								$ip = $_SERVER[ 'REMOTE_ADDR' ];
								$delivery = 'tbd';
								$coupon = 'Not Applied';
								$p = 'case';

								$stmt = $mysqli->prepare( "INSERT INTO rsj_payments (txnid, payment_amount, payment_status, productid, product, color, productname, case_status, item, userid, createdtime, ip, delivery, coupon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
								$stmt->bind_param( "ssssssssssssss", $transactionId, $amt, $paymentStatus, $productid, $p, $productdesc, $productname, $casestatus, $item, $_SESSION[ 'id' ], $dt, $ip, $delivery, $coupon );
								$stmt->execute();
								$stmt->close();

								$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
								$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
								$stmt->execute();
								$stmt->close();
								header( "Location: inventory.php" );
								exit;
							} else if ( isset( $_POST[ 'claimfcrs3' ] ) ) {
								$productid = '36';

								$stmt = $mysqli->prepare( "SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?" );
								$stmt->bind_param( "s", $productid );
								$stmt->execute();
								$stmt->store_result();
								$stmt->bind_result( $productid, $productname, $productdesc, $productitems, $productprice, $producttype );
								$stmt->fetch();
								$stmt->close();

								$items = explode( ',', $productitems );

								$rand1 = rand( 1, 2001 );

								if ( ( 1 <= $rand1 ) && ( $rand1 <= 1000 ) ) {
									// Blue
									$i = rand( 0, 4 );
								} else if ( ( 1001 <= $rand1 ) && ( $rand1 <= 1840 ) ) {
									// Purple
									$i = rand( 5, 7 );
								} else if ( ( 1841 <= $rand1 ) && ( $rand1 <= 1980 ) ) {
									// Pink
									$i = rand( 8, 10 );
								} else if ( ( 1981 <= $rand1 ) && ( $rand1 <= 2000 ) ) {
									// Red
									$i = rand( 11, 12 );
								} else if ( $rand1 = 2001 ) {
									// Red
									$i = 13;
								}
								$item = $items[ $i ];

								$transactionId = 'Free Case';
								$amt = '0.00';
								$paymentStatus = 'Completed';
								$casestatus = 'closed';
								$dt = gmdate( "Y-m-d H:i:s" );
								$ip = $_SERVER[ 'REMOTE_ADDR' ];
								$delivery = 'tbd';
								$coupon = 'Not Applied';
								$p = 'case';

								$stmt = $mysqli->prepare( "INSERT INTO rsj_payments (txnid, payment_amount, payment_status, productid, product, color, productname, case_status, item, userid, createdtime, ip, delivery, coupon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" );
								$stmt->bind_param( "ssssssssssssss", $transactionId, $amt, $paymentStatus, $productid, $p, $productdesc, $productname, $casestatus, $item, $_SESSION[ 'id' ], $dt, $ip, $delivery, $coupon );
								$stmt->execute();
								$stmt->close();

								$stmt = $mysqli->prepare( "UPDATE rsj_members SET `lastfreecase` =? WHERE id=?" );
								$stmt->bind_param( "ss", $dt, $_SESSION[ 'id' ] );
								$stmt->execute();
								$stmt->close();
								header( "Location: inventory.php" );
								exit;
							}
						} 
						else 
						{ //Outcome: There's still time left
							header( "Location: login.php" );
							exit;
						}
					} 
					else 
					{ //Outcome: Latest purchase not within 1 month
						header( "Location: login.php" );
						exit;
					}
				}
			}




		} //PRECONDITION ENDS HERE
	} else {
		header( "Location: inventory.php" );
		exit;
	}


} else {
	header( "Location: inventory.php" );
	exit;
}

?>