<?php	
	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require 'config.php';
	
	if(isset($_SESSION['id']) && !empty($_SESSION['id']))
	{
		$carDetail = $mysqli->query("SELECT * FROM rsj_cart WHERE session = '{$_SESSION['id']}'");
		$cart = mysqli_fetch_all($carDetail,MYSQLI_ASSOC);
		if(empty($cart))
		{
			$array = 'Empty' ;
		}
		else
		{
		
			$results = $mysqli->query("SELECT * FROM rsj_order_payment ORDER BY id DESC LIMIT 1");
			$order = mysqli_fetch_all($results,MYSQLI_ASSOC);
			if(!empty($order))
			{
				$i = $order[0]['id']+1;
			}
			else
			{
				$i = 1;
			}
			
			$orderId = $i;
			
			$orderDate = date('Y-m-d');
			
			/*===Applied coupon===*/
			
			$coupon = $_POST['coupon'];
			
			$couponResult = $mysqli->query("SELECT * FROM rsj_coupons WHERE status = 'active' AND coupon = '{$coupon}'");
			$couponData = mysqli_fetch_all($couponResult,MYSQLI_ASSOC);
			
			$session = $_SESSION['id'];
			$createdtime = date('Y-m-d h:i:s');
			$ip = getenv('REMOTE_ADDR');
			
			$result = $mysqli->query("SELECT * FROM rsj_cart WHERE session='{$session}'");
			$cartItems = mysqli_fetch_all($result,MYSQLI_ASSOC);				
			$totalCasePrice = 0;
			
								
			foreach($cartItems as $val1)
			{
				
				$totalCasePrice += $val1['price'];
				$quantity += $val1['qty'];
			}
			if ($coupon !== 0) 
			{
				$absolutediscount = $totalCasePrice*($couponData[0]['discount']/100);
				$absolutediscountx =  number_format($absolutediscount, 2);
				$finalPrice = number_format($totalCasePrice-$absolutediscountx,2);
			}
			else
			{
				$finalPrice = $totalCasePrice;
			}
			mysqli_query($mysqli,"INSERT INTO rsj_order_payment (order_id, user_id,txn_id, order_total, paid_amount, payment_status, payment_method,createdtime,ip,delivery,proof,delivered_by,coupen,btc_id) VALUES ('".$orderId."','".$session."', '','".$finalPrice."', '".$finalPrice."', '','','".$createdtime."','".$ip."','','','','{$coupon}','')");
			
			foreach($cartItems as $val)
			{
				$caseName = $val['case_name'];
				$caseItems = $val['caseItems'];
				$case_type = $val['caseType'];
				$case_color = $val['caseColor'];
				$item_color = $val['itemColor'];
				$item_image = $val['itemImage'];
				$qty = $val['qty'];
				$price = $val['price'];
				$productType = $val['product_type'];
				mysqli_query($mysqli,"DELETE FROM rsj_cart WHERE session='{$_SESSION['id']}'");
				mysqli_query($mysqli,"INSERT INTO rsj_order_detail (order_id, case_type,case_color,item_color,item_image,case_name,case_items, case_price, case_qty, purchase_qty,product_type, order_date,status) VALUES ('".$orderId."','".$case_type."','".$case_color."','".$item_color."','".$item_image."','".$caseName."', '".$caseItems."','".$price."', '".$qty."','".$qty."', '".$productType."','".$orderDate."','closed')");
			}
			
			$array = array('success'=>true,'orderId'=>$orderId,'qty'=>$quantity,'coupen'=>$coupon,'price'=>$totalCasePrice);
			
		}
		echo json_encode($array);exit;
		
	}
	else
	{
		echo json_encode('plese login');exit;
	}
	