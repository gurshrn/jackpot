<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Maintenance</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
* {
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
.flip-wrap {
	text-align: center;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
</head>

<body>
<h1>We're changing a few things</h1>
<div class="flip-wrap">
	Check back after few hours.
</div>
</body>
</html>
