<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
$message = '';
if (isset($_POST['addworker']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['perm'])) {
	$wusername = $_POST['username'];
	$wpassword = $_POST['password'];
	$wperm = $_POST['perm'];
	if ($mysqli->query("INSERT INTO rsj_admin (username, pass, perm) VALUES ('".$wusername."', '".$wpassword."', '".$wperm."')")) {
		$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added worker', 'Workers', '".$wusername." ".$wpassword." ".$wperm."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
		$message = 'Added worker successfully. Please add worker\'s IP address through phpmyadmin.';
	}
	else {
		$message = 'Error adding worker into database.';
	}
	
}
else if (isset($_POST['removeworker']) && !empty($_POST['id'])) {
	$wid = $_POST['id'];
	$wname = $_POST['name'];
	if ($mysqli->query("DELETE FROM rsj_admin WHERE id = '{$wid}'")) {
		$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Removed worker', 'Workers', '".$wid." ".$wname."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
		$message = 'Removed worker successfully.';
	}
	else {
		$message = 'Error removing worker from database.';
	}

}

if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Add workers</div>
        <div class="content" style="text-align:center;">
        <form method="post" action="workers.php">
        	<input type="text" name="username" placeholder="Username" size="30" min="4" required/>
            <input type="text" name="password" placeholder="Password" size="30" min="4" required/>
            <select name="perm">
  				<option value="worker">Worker</option>
  				<option value="admin">Admin</option>
			</select>
            <input class="button green" type="submit" name="addworker" value="Add" />
        </form>
        </div>
        <? 
		if (!empty($message)) {
        echo '<div class="message">'.$message.'</div>';
		}
		?>
        <div class="sub-title">Current workers</div>
        <?
		echo '
        <table class="orders">
        <tr>
      		<th>ID</th>
     		<th>Username</th>
      		<th>Pass</th>
      		<th>Permissions</th>
            <th>Action</th>
      	</tr>';
		$query = $mysqli->query("SELECT * FROM rsj_admin ORDER BY rsj_admin.id DESC");
		while ($row = $query->fetch_assoc()) {
			$id = $row['id'];
			$username = $row['username'];
			$pass = $row['pass'];
			$perm = $row['perm'];
			
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$username.'</td>
			<td>'.$pass.'</td>
			<td>'.$perm.'</td>
			<td style="text-align:center;">
			<form action="workers.php" method="post"><input type="hidden" value="'.$id.'" name="id" /><input type="hidden" value="'.$username.'" name="name" />
			<input type="submit" class="button red" name="removeworker" value="Remove" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
		</div>
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>