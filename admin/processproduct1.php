<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
	if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
		$incomingurl = $_SERVER["HTTP_REFERER"];
		$refData = parse_url($incomingurl);
		
		if($refData['host'] == 'customer-devreview.com') 
		{
			if($_POST['old_item_image'] != '')
			{
				$item_image = $_POST['old_item_image'];
			}
			
			if(isset($_FILES['item_image']) && !empty($_FILES['item_image']))
			{
				
				foreach($_FILES['item_image']['size'] as $key=>$val)
				{
					if($val != '')
					{
						$imagename = $_FILES['item_image']['name'][$key];
						$date = time();
						$exps = explode('.', $imagename);
						
						$itemImageName = $exps[0] . '_' . $date . "." . $exps[1];

						$target_dir = "upload/";
						$target_file[] = $target_dir . $itemImageName;
						
						move_uploaded_file($_FILES["item_image"]["tmp_name"][$key], $target_file[$key]);

						$item_image[$key] = $itemImageName;
					}
				}
				
			}
			
			
			
			$goldprice = $_POST['gold_price'];
			$casename = $_POST['case_name'];
			$caseprice = $_POST['case_price'];
			$caseType = $_POST['case_type'];
			$caseColor = $_POST['case_color'];
			$catName = $_POST['category_name'];
			$itemname = $_POST['item_name'];
			$itemuniquename = $_POST['item_unique_name'];
			$odds = $_POST['odds'];
			$gpvalue = $_POST['gp_value'];
			$type = $_POST['type'];
			$color = $_POST['color'];
			if (isset($_POST['addproduct'])) 
			{ 
				
				$query = $mysqli->query("INSERT INTO rsj_case (`gold_price`,`case_name`, `case_price`,`case_type`,`case_color`,`cat_id`) VALUES ('".$goldprice."','".$casename."', '".$caseprice."', '".$caseType."','".$caseColor."','".$catName."')");
				if($query)
				{

   					$last_id = $mysqli->insert_id;
   					foreach($itemname as $key=>$val)
					{
						if($_POST['top_item'][$key] != '')
						{
							$top_item = 'yes';
						}
						else
						{
							$top_item = 'no';
						}
						$mysqli->query("INSERT INTO rsj_case_items (`case_id`, `item_name`, `item_unique_name`, `odds`, `gp_value`,`color`,`item_image`,`top_item`) VALUES ('".$last_id."', '".$val."', '".$itemuniquename[$key]."', '".$odds[$key]."', '".$gpvalue[$key]."', '".$color[$key]."', '".$item_image[$key]."','".$top_item."')");
					}
					// $mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
					
				 	header("Location: products1.php");
	     			exit;
				}
				else 
				{
					echo 'Error while inserting values in database.';
				}
			}
			
			else if (isset($_POST['editproduct'])) 
			{
				$id = $_POST['id'];
				if ($mysqli->query("UPDATE rsj_case SET `gold_price` = '{$goldprice}',`case_name` = '{$casename}', `case_price` = '{$caseprice}', `case_type` = '{$caseType}', `case_color` = '{$caseColor}', `cat_id` = '{$catName}' WHERE id={$id}")) 
				{
					$mysqli->query("DELETE FROM rsj_case_items WHERE case_id={$id}");
					foreach($itemname as $key=>$val)
					{
						if($_POST['top_item'][$key] != '')
						{
							$top_item = 'yes';
						}
						else
						{
							$top_item = 'no';
						}
						$mysqli->query("INSERT INTO rsj_case_items (`case_id`, `item_name`, `item_unique_name`, `odds`, `gp_value`,`color`,`item_image`,`top_item`) VALUES ('".$id."', '".$val."', '".$itemuniquename[$key]."', '".$odds[$key]."', '".$gpvalue[$key]."', '".$color[$key]."', '".$item_image[$key]."','".$top_item."')");
					}
					$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
					header("Location: products1.php");
					exit;
				}
				else 
				{
					echo 'Error while Updating values in database.';
				}
			}
			
			else if (isset($_POST['deleteproduct'])) 
			{ 
				$id = $_POST['id'];
				if ($mysqli->query("DELETE FROM rsj_case WHERE id={$id}")) {
					$mysqli->query("DELETE FROM rsj_case_items WHERE case_id={$id}");
					$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deleted product', 'Products', 'Name: ".$name."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
					header("Location: products1.php");
	    			exit;
				}
				else {
					echo 'Error while Updating values in database.';
				}
			}
		}
		else 
		{
			echo 'Run as fast as you can.';
			exit;
		}
	}
	else 
	{
		header("Location: index.php");
		exit;
	}
?>