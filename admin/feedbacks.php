<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	
	$message = '';
	if (isset($_POST['removefeedback'])) {
		$id = $_POST['id'];
		if ($mysqli->query("DELETE FROM rsj_feedbacks WHERE id={$id}")) {
			$message = 'Feedback removed successfully.';
		}
		else {
			$message = 'Error removing feedback from database.';
		}
	}
	else if (isset($_POST['showfeedback'])) {
		$id = $_POST['id'];
		if ($mysqli->query("UPDATE rsj_feedbacks SET `show` = 'yes' WHERE id={$id}")) {
			$message = 'Feedback made visible successfully.';
		}
		else {
			$message = 'Error editing feedback from database.';
		}
	}
	else if (isset($_POST['hidefeedback'])) {
		$id = $_POST['id'];
		if ($mysqli->query("UPDATE rsj_feedbacks SET `show` = 'no' WHERE id={$id}")) {
			$message = 'Feedback hidden successfully.';
		}
		else {
			$message = 'Error editing feedback from database.';
		}
	}
	$feedbacks = $mysqli->query("SELECT * FROM rsj_feedbacks");
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Feedbacks</div>
        <?
	  if (!empty($message)) {
      echo '<div class="message">'.$message.'</div>';
	  }
	  ?>
        <table class="orders">
        	<tr>
            <th>ID</th>
            <th>Username</th>
            <th>Comment</th>
            <th>Rating</th>
            <th>Visibility</th>
            <th>Action</th>
            </tr>
            <?
			while ($feedback = $feedbacks->fetch_assoc()) {
				echo '<tr>
				<td>'.$feedback['id'].'</td>
				<td>'.$feedback['username'].'</td>
				<td>'.$feedback['feedback'].'</td>
				<td>'.$feedback['rating'].'</td>';
				if ($feedback['show'] == 'yes') {
					echo '<td>
					<form method="post" action="feedbacks.php">
				<input type="hidden" name="id" value="'.$feedback['id'].'" />
				<input type="submit" name="hidefeedback" value="Hide" class="button red" />
				</form>
					</td>';
				}
				else {
					echo '<td>
					<form method="post" action="feedbacks.php">
				<input type="hidden" name="id" value="'.$feedback['id'].'" />
				<input type="submit" name="showfeedback" value="Show" class="button green" />
				</form>
					</td>';
				}
				echo '<td>
				<form method="post" action="feedbacks.php">
				<input type="hidden" name="id" value="'.$feedback['id'].'" />
				<input type="submit" name="removefeedback" value="Remove" class="button red" />
				</form>
				</td>';
				echo'
				</tr>';
			}
			?>
        </table>
		</div>
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>