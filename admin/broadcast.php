<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald' rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">E-mail Broadcasts</div>
        <?
	  if (isset($_SESSION['message'])) {
      	echo '<div class="message">'.$_SESSION['message'].'</div>';
		unset($_SESSION['message']);
	  }
	  ?>
        <div class="sub-title">Compose</div>
        <div class="content">
        <form method="post" action="process-broadcast.php">
        <input type="text" name="subject" placeholder="Subject" size="70" required /><br>
        	<textarea name="message" rows="15" style="width:100%;font-size:16px;font-weight:300;"><tr class="section">
  <td align="center">
    <h1>SHORT EMAIL TITLE!</h1>
  </td>
</tr>

<! -- EMAIL SECTION (Copy paste this for new section) --> 
<tr class="section">
  <td align="center">
     <p>
       This is a section. Any message goes here
     </p>
  </td>
</tr>
<! -- EMAIL SECTION --></textarea><br>
           <textarea name="textmessage" rows="5" style="width:100%;font-size:16px;font-weight:300;">Write a TEXT ONLY message here. This is incase when user's email client is unable to receive html messages.</textarea>
            <input type="submit" name="send" value="Broadcast E-mail" class="f-button green" style="margin:0 0 0 5px"/>
        </form>
        </div>
        
        
        <div class="sub-title">Previous Broadcasts</div>
        <div class="content"></div>
		</div>
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>