<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	
	if (isset($_POST['editproduct'])) {
		$p = $_POST['product'];
		$id = $_POST['id'];
		
		$query = $mysqli->query("SELECT * FROM rsj_case WHERE id = '{$id}'");
		$product = $query->fetch_assoc();
		
		$query1 = $mysqli->query("SELECT * FROM rsj_case_items WHERE case_id = '{$product['id']}'");
		$caseitems = $query1->fetch_all(MYSQLI_ASSOC);
		$appendedCaseItems = array_slice($caseitems,1);
		
        $productCat = $mysqli->query("SELECT * FROM rsj_category");
		$category = $productCat->fetch_all(MYSQLI_ASSOC);
		
		$productSubCat = $mysqli->query("SELECT * FROM rsj_sub_category WHERE cat_id = '{$product['cat_id']}'");
		$subCategory = $productSubCat->fetch_all(MYSQLI_ASSOC);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
	
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Edit Product: <? echo $product['name']; ?></div>
        <div class="content" style="text-align: center;">
        <form action="processproduct1.php" method="post" enctype="multipart/form-data">
		<input type="hidden" class="gold-price" name="gold_price" value="<?php echo $product['gold_price'];?>">
        <input type="hidden" name="product" value="<?php echo $p; ?>" />
        <input type="hidden" name="id" value="<?php echo $id; ?>" />
		
			<select name="category_name" class="catName" required>
				<option value="">Select Category...</option>
				<?php 
					if(isset($category) && !empty($category))
					{ 
						foreach($category as $val)
						{
							if($product['cat_id'] == $val['id'])
							{
								$sel = 'selected="selected"';
							}
							else
							{
								$sel = '';
							}
				?>
								<option <?php echo $sel;?> value="<?php echo $val['id']; ?>"><?php echo $val['category_name'];?></option>
							
					<?php } }  ?>
            </select>
			
        	<input type="text" size="20" name="case_name" placeholder="Case of name" value="<?php echo $product['case_name']; ?>" required/>
        	<input type="text" size="20" class="casePrice" name="case_price" placeholder="Case Price" value="<?php echo number_format($product['case_price'],2); ?>" required readonly/>
			
			
			
			<select name="case_type" class="subCategory" required>
				<option value="">Select Sub Category...</option>
            	<?php 
					if(isset($subCategory) && !empty($subCategory))
					{ 
						foreach($subCategory as $val1)
						{
							if($product['case_type'] == $val1['id'])
							{
								$sel = 'selected="selected"';
							}
							else
							{
								$sel = '';
							}							
				?>
							<option <?php echo $sel;?> value="<?php echo $val1['id']; ?>"><?php echo $val1['sub_category_name'];?></option>
							
				<?php } } ?>
            </select>
			
			 <select name="case_color" required>
				<option value="red" <?php if ($product['case_color'] == 'red') { echo 'selected';} ?>>Red</option>
            	<option value="green" <?php if ($product['case_color'] == 'green') { echo 'selected';} ?>>Green</option>
            	<option value="blue" <?php if ($product['case_color'] == 'blue') { echo 'selected';} ?>>Blue</option>
            	<option value="gold" <?php if ($product['case_color'] == 'gold') { echo 'selected';} ?>>Gold</option>
            	<option value="white" <?php if ($product['case_color'] == 'white') { echo 'selected';} ?>>White</option>
            	<option value="lightgray" <?php if ($product['case_color'] == 'light grey') { echo 'selected';} ?>>Light Gray</option>
            	<option value="lightblue" <?php if ($product['case_color'] == 'light blue') { echo 'selected';} ?>>Light Blue</option>
			</select>
			
			<br>
			<div class="form-hr clearfix gpValue">
				<input type="hidden" name="top_item[]" value="<?php if($caseitems[0]['top_item'] == 'yes'){echo 'yes';}?>">
				<input type="checkbox" class="top-item" <?php if($caseitems[0]['top_item'] == 'yes'){echo 'checked="checked"';}?>>Top Item
				<input type="text" size="20" name="item_name[]" placeholder="Item Name" value="<?php echo $caseitems[0]['item_name']; ?>" required/>
				<input type="text" size="20" name="item_unique_name[]" placeholder="Item unique name" value="<?php echo $caseitems[0]['item_unique_name']; ?>" required/>
				<input type="text" size="20" class="odds-value" name="odds[]" placeholder="Odds" value="<?php echo $caseitems[0]['odds']; ?>" required/>
				<input type="text" class="gp-value" size="20" name="gp_value[]" placeholder="GP value" value="<?php echo $caseitems[0]['gp_value']; ?>" required/>
				<select name="color[]" required>
					<option value="green" <?php if ($caseitems[0]['color'] == 'green') { echo 'selected';} ?>>Green</option>
					<option value="red" <?php if ($caseitems[0]['color'] == 'red') { echo 'selected';} ?>>Red</option>
					<option value="pink" <?php if ($caseitems[0]['color'] == 'pink') { echo 'selected';} ?>>Pink</option>
					<option value="purple" <?php if ($caseitems[0]['color'] == 'purple') { echo 'selected';} ?>>Purple</option>
					<option value="blue" <?php if ($caseitems[0]['color'] == 'blue') { echo 'selected';} ?>>Blue</option>
				
				</select>
				
				<input type="file" name="item_image[]"><input type="hidden" name="old_item_image[]" value="<?php echo $caseitems[0]['item_image'];?>"><span><?php echo $caseitems[0]['item_image'];?></span>
				
				<a href="javascript:void(0)" class="addItem">+</a><br>
			</div>
            
            <div class="input_fields_wrap">
                <?php 
				
                    foreach($appendedCaseItems as $val1){ 
					
					
                ?>
						
                        <div class="form-hr clearfix gpValue">
							<input type="hidden" name="top_item[]" value="<?php if($val1['top_item'] == 'yes'){echo 'yes';}?>">
							<input type="checkbox" class="top-item" <?php if($val1['top_item'] == 'yes'){echo 'checked="checked"';}?>>Top Item
                            <input type="text" size="20" name="item_name[]" placeholder="Item Name" value="<?php echo $val1['item_name']; ?>" required/>
                            <input type="text" size="20" name="item_unique_name[]" placeholder="Item unique name" value="<?php echo $val1['item_unique_name']; ?>" required/>
                            <input type="text" class="odds-value" size="20" name="odds[]" placeholder="Odds" value="<?php echo $val1['odds']; ?>" required/>
                            <input type="text"  size="20" class="gp-value" name="gp_value[]" placeholder="GP value" value="<?php echo $val1['gp_value']; ?>" required/>
                            <select name="color[]" required>
                                <option value="red" <?php if ($val1['color'] == 'red') { echo 'selected';} ?>>Red</option>
                                <option value="green" <?php if ($val1['color'] == 'green') { echo 'selected';} ?>>Green</option>
                                <option value="blue" <?php if ($val1['color'] == 'blue') { echo 'selected';} ?>>Blue</option>
                                <option value="gold" <?php if ($val1['color'] == 'gold') { echo 'selected';} ?>>Gold</option>
                                <option value="white" <?php if ($val1['color'] == 'white') { echo 'selected';} ?>>White</option>
                                <option value="lightgray" <?php if ($val1['color'] == 'lightgray') { echo 'selected';} ?>>Light Gray</option>
                                <option value="lightblue" <?php if ($val1['color'] == 'lightblue') { echo 'selected';} ?>>Light Blue</option>
                            </select>
							<input type="file" name="item_image[]"><input type="hidden" name="old_item_image[]" value="<?php echo $val1['item_image'];?>"><span><?php echo $val1['item_image'];?></span>
                            <a href="javascript:void(0)" class="remove_fields">-</a><br>
                        </div>
                    <?php } ?>


            </div>
            
            <input type="submit" class="button green" name="editproduct" value="Edit" />
            <a href="products1.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
            <input type="submit" class="button red" name="deleteproduct" value="Delete Product" style="margin-left:200px;" onclick="return confirm('Are you sure you want to continue?')" />
        </form>
        
        </div>
		</div>
    <?php
	}
	else {
		header("Location: products1.php");
		exit;
	}
	?>
</body>
</html>
<?php
}
else {
	header("Location: index.php");
	exit;
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    var max_fields      = 5;
    var wrapper         = $(".input_fields_wrap");
    $(document).off('click','.addItem').on('click','.addItem',function(e){
        e.preventDefault();
        var x = 1;

        if(x < max_fields){
            x++;
             $(wrapper).append('<div class="form-hr clearfix"><input type="hidden" name="top_item[]"><input type="checkbox" class="top-item">Top Item<input type="text" size="20" name="item_name[]" placeholder="Item Name" required/><input type="text" size="20" name="item_unique_name[]" placeholder="Item unique name" required/><input type="text" size="20" class="gpValue odds-value" name="odds[]" placeholder="Odds" required/><input type="text" size="20" name="gp_value[]" class="gpValue gp-value" placeholder="GP value" required/><select name="color[]" required><option value="green" selected>Green</option><option value="red">Red</option><option value="pink">Pink</option><option value="purple">Purple</option><option value="blue">Blue</option></select><input type="file" name="item_image[]" required><a href="javascript:void(0)" class="remove_fields">-</a><br></div>');
            
        }
    });
    $(wrapper).on("click",".remove_fields", function(e)
    {
        e.preventDefault();
        $(this).parent('div').remove();
    });
</script>
<script>
	$(document).on('change','.gpValue',function(){
		casePriceCal();
	});
	$(document).on('click','.remove_fields',function(){
		casePriceCal();
	});
	function casePriceCal()
	{
		
		var totalCal = 0;
		var goldprice = $('.gold-price').val();
		$('.gpValue').each(function(){
			gpValue =$(this).find('.odds-value').val();
			odds =$(this).find('.gp-value').val();
			
			if(gpValue != '' && odds != '')
			{
				calOddsPer = (parseFloat(gpValue)*parseFloat(odds))/100;
				totalCal += parseFloat(calOddsPer);
			}
			
			
		});
		var casePrice = parseFloat(totalCal)*parseFloat(goldprice);
		$('.casePrice').val(casePrice.toFixed(2));
	}
	$(document).on('change','.catName',function()
	{
		var catId = $(this).val();
		if(catId != '')
		{
			jQuery.ajax({
				type: "POST",
				url: 'get-cat-subcat.php',
				data:{catId:catId},
				dataType: "html",
				success: function (data)
				{
					$('.subCategory').html(data);
				}
			});
		}
		else
		{
			$('.subCategory').val('');
			$('.subCategory').html('<option>Select Sub Category...</option>');
		}
		
		
	});
	$(document).on('change','.subCategory',function()
	{
		var subCatId = $(this).val();
		if(subCatId != '')
		{
			
			jQuery.ajax({
				type: "POST",
				url: 'get-goldprice.php',
				data:{subCatId:subCatId},
				dataType: "html",
				success: function (data)
				{
					$('.gold-price').val(data);
					casePriceCal();
				}
			});
		}
		
		
	});
	$(document).on('change','.top-item',function(){
		if($(this).is(':checked'))
		{
			$(this).prev('input').val('yes');
		}
		else
		{
			$(this).prev('input').val('');
		}
		
	});
</script>