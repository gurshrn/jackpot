<?
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
if (!isset($_SESSION['username'])) {
	header("Location: index.php");
    exit;
}
else {
	$incomingurl = $_SERVER["HTTP_REFERER"];
	$themvp = $_SESSION['username'];
	if(!empty($_POST)) {
		
		if (isset($_POST['deliver'])) {
			unset($_POST['deliver']);
			if(isset($_POST['orderId']))
			{
				foreach ($_POST['orderId'] as $key => $val) 
				{
					
					//$mysqli->query("UPDATE rsj_payments SET delivery = 'delivered', deliveredby = '{$themvp}' WHERE id = '{$value}'");
					
					$stmt = $mysqli->prepare("UPDATE rsj_order_item SET delivery = 'delivered', delivered_by =? WHERE id =?");
					$stmt->bind_param("ss", $themvp, $val);
					$stmt->execute();
					$stmt->close();
					//$mysqli->query("UPDATE rsj_payments SET deliveredby = '{$themvp}' WHERE id = '{$value}' AND LENGTH(deliveredby)=0");
				}
			}
			else
			{
				foreach ($_POST as $key => $value) {
					
					//$mysqli->query("UPDATE rsj_payments SET delivery = 'delivered', deliveredby = '{$themvp}' WHERE id = '{$value}'");
					$stmt = $mysqli->prepare("UPDATE rsj_payments SET delivery = 'delivered', deliveredby =? WHERE id =?");
					$stmt->bind_param("ss", $themvp, $value);
					$stmt->execute();
					$stmt->close();
					$mysqli->query("UPDATE rsj_payments SET deliveredby = '{$themvp}' WHERE id = '{$value}' AND LENGTH(deliveredby)=0");
				}
			}
			header("Location: ".$incomingurl);
    		exit;
		}
		else if (isset($_POST['undeliver'])) {
			
			unset($_POST['undeliver']);
			if(isset($_POST['orderId']))
			{
				
				foreach ($_POST['orderId'] as $key => $value) {
					//$mysqli->query("UPDATE rsj_payments SET delivery = 'tbd', deliveredby = '' WHERE id = '{$value}'");
					$stmt = $mysqli->prepare("UPDATE rsj_order_item SET delivery = 'tbd', delivered_by = '' WHERE id =?");
					$stmt->bind_param("s", $value);
					$stmt->execute();
					$stmt->close();
				}
			}
			else
			{
				foreach ($_POST as $key => $value) {
					//$mysqli->query("UPDATE rsj_payments SET delivery = 'tbd', deliveredby = '' WHERE id = '{$value}'");
					$stmt = $mysqli->prepare("UPDATE rsj_payments SET delivery = 'tbd', deliveredby = '' WHERE id =?");
					$stmt->bind_param("s", $value);
					$stmt->execute();
					$stmt->close();
				}
			}
			
			header("Location: ".$incomingurl);
    		exit;
		}
		else {
			echo 'Error: Data tampered.';
		}
	}
	else {
			header("Location: orders.php");
    		exit;
	}
}
?>