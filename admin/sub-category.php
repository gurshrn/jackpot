<?php
	ob_start();
	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require('config.php');
	if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) 
	{
		
		$productCat = $mysqli->query("SELECT * FROM rsj_category");
		$category = $productCat->fetch_all(MYSQLI_ASSOC);
		
		$productSubCat = $mysqli->query("SELECT * FROM  rsj_sub_category WHERE id = '{$_POST['id']}'");
		$subCat = $productSubCat->fetch_all(MYSQLI_ASSOC);
		
		
		
		
		
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
		<div class="main">
			<div class="title"><?php if(isset($subCat) && !empty($subCat)){ echo 'Edit Sub Category';}else{ echo 'Add Sub Category';}?></div>
			<div class="content" style="text-align: center;">
				<form action="addSubCategory.php" method="post">
				<input type="hidden" name="id" value="<?php if(isset($subCat) && !empty($subCat)){ echo $subCat[0]['id'];}?>" />
				<select  name="catId" class="" required>
					<option value="">Select Category...</option>
					<?php 
						if(isset($category) && !empty($category))
						{ 
							foreach($category as $val1)
							{ 
								if(isset($subCat) && !empty($subCat)){
									if($subCat['cat_id'] = $val1['id'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
					?>
								<option <?php echo $sel;?> value="<?php echo $val1['id']; ?>"><?php echo $val1['category_name'];?></option>
								
					<?php } } ?>
				</select>

				<input type="text" size="25" name="sub_category_name" placeholder="Sub Category Name" value="<?php if(isset($subCat) && !empty($subCat)){ echo $subCat[0]['sub_category_name'];}?>" required/>
				
				<input type="text" size="25" name="gold_price" placeholder="Gold Price" value="<?php if(isset($subCat) && !empty($subCat)){ echo $subCat[0]['gold_price'];}?>"/>
				
				<input type="submit" class="button green" name="<?php if(isset($subCat) && !empty($subCat)){ echo 'editSubCategory';}else{ echo 'Edit';}?>" value="<?php if(isset($subCat) && !empty($subCat)){ echo 'editSubCategory';}else{ echo 'Add';}?>" />
				<a href="products1.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
			</div>
		</div>
<?php 	} ?>
		