<?php
	ob_start();
	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require('config.php');
	if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) 
	{
		if(isset($_POST['id']))
		{
			$productCat = $mysqli->query("SELECT * FROM rsj_category WHERE id = '{$_POST['id']}'");
			$category = $productCat->fetch_all(MYSQLI_ASSOC);
			
		}
		
		
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
		<div class="main">
			<div class="title"><?php if(isset($category) && !empty($category)){ echo 'Edit Category';}else{ echo 'Add Category';}?></div>
			<div class="content" style="text-align: center;">
				<form action="addCategory.php" method="post">
				<input type="hidden" name="id" value="<?php if(isset($category) && !empty($category)){ echo $category[0]['id'];}?>" />
				<input type="text" size="50" name="category_name" placeholder="Category Name" value="<?php if(isset($category) && !empty($category)){ echo $category[0]['category_name'];}?>" required/>
				<input type="submit" class="button green" name="<?php if(isset($category) && !empty($category)){ echo 'editCategory';}else{ echo 'addCategory';}?>" value="<?php if(isset($category) && !empty($category)){ echo 'Edit';}else{ echo 'Add';}?>" />
				<a href="products1.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
			</div>
		</div>
<?php 	} ?>
		