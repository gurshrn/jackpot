<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include('config.php');
include_once "pagination.php";
if (isset($_SESSION['username'])) {
	$casequery = $mysqli->query("SELECT * FROM rsj_case WHERE case_price != 0");
	if (isset($_POST['addcase'])) 
	{
		$username = $_POST['username'];
		$proof = $_POST['proof'];
		$caseid = $_POST['caseid'];
		$qty = $_POST['qty'];
		
		$usercheck = $mysqli->prepare("SELECT id, usr FROM rsj_members WHERE usr=?");
		$usercheck->bind_param('s', $username);
		$usercheck->execute();
		$usercheck->store_result();
		$usercheck->bind_result($userid, $usr);
		$usercheck->fetch();
		
		if ($usercheck->num_rows !== 0) {
			$usercheck->close();
			$casequery = $mysqli->query("SELECT case_price,case_name,case_color,sub_category_name FROM rsj_case INNER JOIN rsj_sub_category ON rsj_case.case_type = rsj_sub_category.id WHERE rsj_case.id='{$caseid}'");
			$caseproduct = mysqli_fetch_all($casequery,MYSQLI_ASSOC);
			
			$productquery = $mysqli->query("SELECT * FROM rsj_case_items WHERE case_id = '{$caseid}'");
			$product = mysqli_fetch_all($productquery,MYSQLI_ASSOC);
			
			foreach($product as $val)
			{
				$caseItem[] = $val['item_name'];
				$itemColor[] = $val['color'];
				$itemImage[] = $val['item_image'];
			}
			$caseItem1 = implode(",",$caseItem);
			$itemcolors = implode(",",$itemColor);
			$itemimage = implode(",",$itemImage);
			
			$caseName = $caseproduct[0]['case_name'];
			$casePrice = $caseproduct[0]['case_price'];
			$caseColor = $caseproduct[0]['case_color'];
			$caseType = $caseproduct[0]['sub_category_name'];
			$caseQty = $qty;
			$orderDate = gmdate("Y-m-d");
			
			
			
			$dt = gmdate("Y-m-d H:i:s");
			$completed = 'Completed';
			$p = 'case';
			$closed = 'closed';
			$tbd = 'tbd';
			$notused = 'Not used';
			$na = 'NA';
			$processid = $_POST['amount'];;
			$totalAmt = $casePrice*$caseQty;
			
			$results = $mysqli->query("SELECT * FROM rsj_order_payment ORDER BY id DESC LIMIT 1");
			$order = mysqli_fetch_all($results,MYSQLI_ASSOC);
			if(!empty($order))
			{
				$i = $order[0]['id']+1;
			}
			else
			{
				$i = 1;
			}
			
			$orderId = 'GP_'.$i;
			
			mysqli_query($mysqli,"INSERT INTO rsj_order_detail (order_id, case_name,case_items, case_color, item_color, item_image, case_type, case_price, case_qty,purchase_qty,order_date,status) VALUES ('".$orderId."','".$caseName."','".$caseItem1."','".$caseColor."','".$itemcolors."','".$itemimage."','".$caseType."','".$casePrice."','".$caseQty."','".$caseQty."','".$orderDate."','".$closed."')");
			
			mysqli_query($mysqli,"INSERT INTO rsj_order_payment (txn_id, order_id, order_total,paid_amount, payment_status, payment_method,user_id, createdtime, ip, delivery, coupen) VALUES ('".$proof."','".$orderId."','".$totalAmt."','".$totalAmt."','".$completed."','".$processid."','".$userid."','".$dt."','".$na."','".$tbd."','".$notused."')");
			
			header("Location: orders.php");
			exit;
		}
		else 
		{
			$usercheck->close();
			$message = 'Username invalid.';
		}
		
	}
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="superwrap">
  <div id="top-header-wrap">
    <div id="top-header"> <a href="dashboard.php" id="logo"></a>
      <div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
    </div>
  </div>
  <div class="main">
    <div class="title">Add Order</div>
    <?
	  if (!empty($message)) {
      echo '<div class="message">'.$message.'</div>';
	  }
	?>
      
    <div class="sub-title">Add a Product</div>
    <div class="content">
		<form action="add-order.php" method="post" autocomplete="off">
			<select name="caseid" required>
			<?
				echo '<option vlaue="">Select Product</option>';
				while ($case = $casequery->fetch_assoc()) 
				{
					echo '<option value="'.$case['id'].'">'.ucfirst($case['case_name']).'</option>';
				}
			?>
			</select>
		
			<input type="text" size="30" name="username" placeholder="Username" required/>
			<input type="text" size="10" name="amount" placeholder="Paid GP" required/>
			<input type="text" size="30" name="proof" placeholder="Proof URL with http" required/>
			<input type="number" size="10" name="qty" placeholder="Quantity" required/>
			<input type="submit" class="button green" name="addcase" value="Add" />
		</form>
    </div>
    
  </div>
</div>
</body>
</html>
<? } else {
header("Location: index.php");
exit;
}
?>