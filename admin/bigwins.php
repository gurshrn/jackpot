<?
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	if(!empty($_POST)) {
		if (isset($_POST['editbigwins'])) {
			$b = $_POST['one'].','.$_POST['two'].','.$_POST['three'].','.$_POST['four'].','.$_POST['five'];
			//$mysqli->query("UPDATE rsj_editables SET `content` = '{$b}' WHERE name='bigwins'");
			$stmt = $mysqli->prepare("UPDATE rsj_editables SET `content`=? WHERE name='bigwins'");
			$stmt->bind_param("s", $b);
			$stmt->execute();
			$stmt->close();
			
			
			$barray = explode(',', $b);
			foreach ($barray as $xkey => $value) {
				$key = $xkey + 1;
			if (empty($value)) {
				//$mysqli->query("UPDATE `rsj_bigwins` SET `paymentid` = ''  WHERE `id` ='{$key}'");
				$stmt = $mysqli->prepare("UPDATE `rsj_bigwins` SET `paymentid` = ''  WHERE `id` =?");
				$stmt->bind_param("s", $key);
				$stmt->execute();
				$stmt->close();
			}
			else {
				$paymentid = $value; 			//important
				//$results = $mysqli->query("SELECT productid, product, productname, item, userid FROM rsj_payments WHERE id='{$paymentid}'");
				$stmt = $mysqli->prepare("SELECT productid, product, productname, item, userid FROM rsj_payments WHERE id=?");
				$stmt->bind_param("s", $paymentid);
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($productid, $p, $productname, $item, $userid);
				$stmt->fetch();
				$stmt->close();
				
				$username = $mysqli->query("SELECT usr FROM rsj_members WHERE id='{$userid}'")->fetch_object()->usr;														//important
				if ($p == 'pack') {
					$productquery = $mysqli->query("SELECT `desc`, `type` FROM rsj_packs WHERE id='{$productid}'");
					$product = $productquery->fetch_assoc();
					$type = $product['type']; 			//important
					$desc = $product['desc'];			//important
				}
				else if ($p == 'coin') {
					$productquery = $mysqli->query("SELECT `desc`, `type` FROM rsj_coins WHERE id='{$productid}'");
					$product = $productquery->fetch_assoc();
					$type = $product['type']; 			//important
					$desc = $product['desc'];			//important
				}
				else { // for case
					$p = 'case';
					$productquery = $mysqli->query("SELECT `desc`, `type` FROM rsj_products WHERE id='{$productid}'");
					$product = $productquery->fetch_assoc();
					$type = $product['type']; 			//important
					$desc = $product['desc'];			//important
				}
				
				//$mysqli->query("UPDATE `rsj_bigwins` SET `paymentid` = '{$paymentid}', `item` = '{$item}', `product` = '{$productname}', `productid` = '{$productid}', `p` = '{$p}', `type` = '{$type}', `desc` = '{$desc}', `username` = '{$username}' WHERE `id` ='{$key}'");
				
				$stmt = $mysqli->prepare("UPDATE `rsj_bigwins` SET `paymentid` = '{$paymentid}', `item` = '{$item}', `product` = '{$productname}', `productid` = '{$productid}', `p` = '{$p}', `type` = '{$type}', `desc` = '{$desc}', `username` = '{$username}' WHERE `id` =?");
				$stmt->bind_param("s", $key);
				$stmt->execute();
				$stmt->close();
				
			}
			
		}
			header("Location: editables.php");
    		exit;
		}
		else {
			echo 'Error: Data tampered.';
		}
	}
	else {
			header("Location: editables.php");
    		exit;
	}
}
else {
	header("Location: index.php");
    exit;
}
?>