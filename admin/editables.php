<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	
	$message = '';
	if (isset($_POST['editannouncement'])) {
		$a = $_POST['announcement'];
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET `content` =? WHERE name='announcement'");
		$stmt->bind_param("s", $a);
		if ($stmt->execute()) {
			$message = 'Announcement updated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Announcement', 'Editables', '".$a."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Announcement', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $a);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error updating announcement on database.';
		}
		
	}
	else if (isset($_POST['editfcase'])) {
		$c = $_POST['fcase'];
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET `content` =? WHERE name='fcase'");
		$stmt->bind_param("s", $c);
		if ($stmt->execute()) {
			$message = 'Featured case updated successfully.';
			$stmt->close();
			
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Case', 'Editables', '".$c."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Case', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $c);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error updating featured case on database.';
		}
		
	}
	else if (isset($_POST['editfpack'])) {
		$p = $_POST['fpack'];
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET `content` ? WHERE name='fpack'");
		$stmt->bind_param("s", $p);
		if ($stmt->execute()) {
			$message = 'Featured pack updated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Pack', 'Editables', '".$p."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Pack', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $p);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error updating featured pack on database.';
		}
		
	}
	else if (isset($_POST['editGoldPrice'])) {
		$gPrice = $_POST['goldprice'];
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET `content` ? WHERE name='goldprice'");
		$stmt->bind_param("sss", $gPrice);
		if ($stmt->execute()) {
			$message = 'Gold price updated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Pack', 'Editables', '".$p."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Featured Pack', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $gPrice);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error updating gold price on database.';
		}
		
	}
	else if (isset($_POST['addcoupon'])) {
		$addcoupon = $_POST['coupon'];
		$adddiscount = $_POST['discount'];
		
		$stmt = $mysqli->prepare("INSERT INTO rsj_coupons (coupon, discount, status) VALUES (?, ?, 'active')");
		$stmt->bind_param("ss", $addcoupon, $adddiscount);
		if ($stmt->execute()) {
			$message = 'Coupon added successfully. Coupon is active to be used.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added Coupon', 'Editables', '".$addcoupon."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added Coupon', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $addcoupon);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error adding coupon in database.';
		}
		
	}
	else if (isset($_POST['deactivatecoupon'])) {
		$couponid = $_POST['couponid'];
		$couponname = $_POST['couponname'];
		
		$stmt = $mysqli->prepare("UPDATE rsj_coupons SET `status` = 'inactive' WHERE `id` =?");
		$stmt->bind_param("s", $couponid);
		
		if ($stmt->execute()) {
			$message = 'Coupon inactivated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deactivated Coupon', 'Editables', 'ID: ".$couponid." Coupon: ".$couponname."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$logdetails = 'ID: '.$couponid.' Coupon: '.$couponname;
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deactivated Coupon', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $logdetails);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error editing coupon from database.';
		}
		
	}
	else if (isset($_POST['activatecoupon'])) {
		$couponid = $_POST['couponid'];
		$couponname = $_POST['couponname'];
		$stmt = $mysqli->prepare("UPDATE rsj_coupons SET `status` = 'active' WHERE `id` =?");
		$stmt->bind_param("s", $couponid);
		
		if ($stmt->execute()) {
			$message = 'Coupon activated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Activated Coupon', 'Editables', 'ID: ".$couponid." Coupon: ".$couponname."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$logdetails = 'ID: '.$couponid.' Coupon: '.$couponname;
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Activated Coupon', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $logdetails);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error editing coupon from database.';
		}
		
	}
	else if (isset($_POST['removecoupon'])) {
		$couponid = $_POST['couponid'];
		$couponname = $_POST['couponname'];
		$stmt = $mysqli->prepare("DELETE FROM rsj_coupons WHERE `id` =?");
		$stmt->bind_param("s", $couponid);
		
		if ($stmt->execute()) {
			$message = 'Coupon removed successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Removed Coupon', 'Editables', 'ID: ".$couponid." Coupon: ".$couponname."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$logdetails = 'ID: '.$couponid.' Coupon: '.$couponname;
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Removed Coupon', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $logdetails);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error removing coupon from database.';
		}
		
	}
	else if (isset($_POST['flashsale'])) {
		$visibility = $_POST['visibility'];
		$p = $_POST['p'];
		$productid = $_POST['pid'];
		$discount = $_POST['discount'];
		$usertzinput = $_POST['usertz'];
		$startdt = $_POST['startdt'];
		$enddt = $_POST['enddt'];
		$gmt = new DateTimeZone('GMT');
		$usertz = new DateTimeZone($usertzinput);
		
		$startdate = new DateTime($startdt, $usertz);
		$startdate->setTimezone($gmt);
		$startdategmt = $startdate->format('Y-m-d H:i:s');
		
		$enddate = new DateTime($enddt, $usertz);
		$enddate->setTimezone($gmt);
		$enddategmt = $enddate->format('Y-m-d H:i:s');
		
		
		
		$stmt = $mysqli->prepare("UPDATE `rsj_flash` SET `p` =?, `productid` =?, `startdt` =?, `enddt` =?, `visibility` =?, `discount` =? WHERE `id` = '1'");
		$stmt->bind_param("ssssss", $p, $productid, $startdategmt, $enddategmt, $visibility, $discount);
		
		if ($stmt->execute()) {
			$message = 'Flash sale updated successfully.';
			$stmt->close();
			
			//$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Flash sale', 'Editables', 'Product: ".$p." ID: ".$productid." Start: ".$startdategmt." End: ".$enddategmt."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			
			$logdetails = 'Product: '.$p.' ID: '.$productid.' Start: '.$startdategmt.' End: '.$enddategmt;
			$stmt = $mysqli->prepare("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited Flash sale', 'Editables', ?, '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
			$stmt->bind_param("s", $logdetails);
			$stmt->execute();
			$stmt->close();
		}
		else {
			$message = 'Error updating Flash sale from database.';
		}
		
	}
	else if(isset($_POST['setvip'])) {
		$vipfree = implode(",",$_POST['freecasers3']);
		
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET content=? WHERE name='vip'");
		$stmt->bind_param("s", $_POST['vipprice']);
		$stmt->execute();
		$stmt->close();
		
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET content=? WHERE name='vipdiscount'");
		$stmt->bind_param("s", $_POST['vipdiscount']);
		$stmt->execute();
		$stmt->close();
		
		
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET content=? WHERE name='vipfree'");
		$stmt->bind_param("s", $vipfree);
		$stmt->execute();
		$stmt->close();
		$message = 'VIP settings updated successfully.';
	}
	else if (isset($_POST['updatefreecases'])) {
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET content=? WHERE name='freecases'");
		$stmt->bind_param("s", $_POST['freecases']);
		$stmt->execute();
		$stmt->close();
		$message = 'Free Cases settings updated successfully.';
	}
	
	else if (isset($_POST['updatetopitems'])) {
		$stmt = $mysqli->prepare("UPDATE rsj_editables SET content=? WHERE name='topitems'");
		$stmt->bind_param("s", $_POST['topitems']);
		$stmt->execute();
		$stmt->close();
		$message = 'Top items list updated successfully.';
	}
	
	$editables = $mysqli->query("SELECT * FROM rsj_editables");
	$data = array();
	while ($editable = $editables->fetch_assoc()) {
		$data[$editable['name']] = $editable['content'];
	}
	
	$cases = $mysqli->query("SELECT * FROM rsj_products");
	$packs = $mysqli->query("SELECT * FROM rsj_packs");
	$coins = $mysqli->query("SELECT * FROM rsj_coins");
	$coupons = $mysqli->query("SELECT * FROM rsj_coupons");
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../assets/js/jstz.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
        		if("<? echo $_SESSION['timezone'] ?>".length==0){
           			var tz = jstz.determine();
					var visitortimezone = tz.name();
            		$.ajax({
                		type: "GET",
                		url: "http://customer-devreview.com/jackpot/admin/timezone.php",
                		data: "time="+ visitortimezone,
                		success: function() {
                    		location.reload();
                		}
            		});
        		}
				
</script>
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/combodate.js"></script>

</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
      
        <div class="title">Editables</div>
      <?php
	  if (!empty($message)) {
      echo '<div class="message">'.$message.'</div>';
	  }
	  ?>
        <div class="sub-title">Announcement</div>
        <div class="content">
        <form method="post" action="editables.php">
        	<textarea name="announcement" rows="5" style="width:100%;margin:0;font-size:22px;font-weight:300;"><? echo $data['announcement'] ?></textarea>
            <input type="submit" class="f-button green" name="editannouncement" value="Update" />
        </form>
        </div>
        <div class="ninety">
        <div class="fifty">
        <div class="sub-title">Featured Case</div>
        <form method="post" action="editables.php">
        	<select name="fcase">
            <?
			while ($case = $cases->fetch_assoc()) {
            	echo '<option value="'.$case['id'].'"';if ($case['id'] == $data['fcase']) {echo 'selected';}
				echo'>'.$case['name'].'</option>';
			}
			?>
            </select>
            <input type="submit" class="button green" name="editfcase" value="Update" />
            
         </form></div><div class="fifty">
        <div class="sub-title">Featured Pack</div>
        <form method="post" action="editables.php">
        	<select name="fpack">
            <?
			while ($pack = $packs->fetch_assoc()) {
            	echo '<option value="'.$pack['id'].'"';if ($pack['id'] == $data['fpack']) {echo 'selected';}
				echo'>'.$pack['name'].'</option>';
			}
			?>
            </select>
            <input type="submit" class="button green" name="editfpack" value="Update" />
            
         </form></div>
        
        </div>
        
        <div class="sub-title" id="bigwins">Big wins</div>
        <div class="content">
        <form method="post" action="bigwins.php">
        	<?
            if (!empty($data['bigwins'])) {
				$winsarray = explode(',', $data['bigwins']);
			}
			?>
        	<input type="text" size="10" name="one" placeholder="Order ID #1" value="<? echo $winsarray[0] ?>" />
        	<input type="text" size="10" name="two" placeholder="Order ID #2" value="<? echo $winsarray[1] ?>" />
        	<input type="text" size="10" name="three" placeholder="Order ID #3" value="<? echo $winsarray[2] ?>" />
        	<input type="text" size="10" name="four" placeholder="Order ID #4" value="<? echo $winsarray[3] ?>" />
        	<input type="text" size="10" name="five" placeholder="Order ID #5" value="<? echo $winsarray[4] ?>" />
            <input type="submit" class="button green" name="editbigwins" value="Update" />
        </form>
        </div>
		
		<div class="sub-title" id="bigwins">RS Gold Price</div>
        <div class="content">
        <form method="post" action="editables.php">
        	<input type="text" size="10" name="goldprice" placeholder="Gold price" value="<? echo $data['goldprice'] ?>" />
        	<input type="submit" class="button green" name="editGoldPrice" value="Update" />
        </form>
        </div>
        
        <div class="sub-title">Coupons</div>
        <div class="content">
        <form method="post" action="editables.php">
        	<input type="text" size="15" name="coupon" placeholder="Coupon" value="" required/>
        	<input type="text" size="10" name="discount" placeholder="Discount %" value="" required/> 
            <input type="submit" class="button green" name="addcoupon" value="Add Coupon" style="margin:5px;" />
        </form>
        </div>
        <? 
		if ($coupons->num_rows !== 0) {
        echo '<table class="orders">';
        echo '<tr>
        	<th>ID</th>
            <th>Coupon</th>
            <th>Discount</th>
            <th>Status</th>
            <th>Action</th>
        </tr>';
		while ($coupon = $coupons->fetch_assoc()) {
			echo '<tr>';
			echo '<td>'.$coupon['id'].'</td>
			<td>'.$coupon['coupon'].'</td>
			<td>'.$coupon['discount'].'</td>';
			
			echo '<td><form method="post" action="editables.php"><input type="hidden" name="couponid" value="'.$coupon['id'].'" /><input type="hidden" name="couponname" value="'.$coupon['coupon'].'" />';
			if ($coupon['status'] == 'active') {
				echo '<input type="submit" name="deactivatecoupon" value="Deactivate" class="button red"/>';
			}
			else {
				echo '<input type="submit" name="activatecoupon" value="Activate" class="button green" />';
			}
			echo '</form></td>';
			
			echo '<td><form method="post" action="editables.php"><input type="hidden" name="couponid" value="'.$coupon['id'].'" /><input type="hidden" name="couponname" value="'.$coupon['coupon'].'" />';
			echo '<input type="submit" name="removecoupon" value="Remove" class="button red"/>';
			echo '</form></td>';
			
			echo '</tr>';
		}
        echo '</table>';
		}
		else {
			echo '<div class="message">No coupons currently in database.</div>';
		}
		
		
		// get flash sale values
		$flashquery = $mysqli->query("SELECT * FROM rsj_flash WHERE id = '1'");
	$flash = $flashquery->fetch_assoc();
	$gmt = new DateTimeZone('GMT');
	$usertz = new DateTimeZone($_SESSION['timezone']);
		
		$startdt = new DateTime($flash['startdt'], $gmt);
		$startdt->setTimezone($usertz);
		$startdate = $startdt->format('Y-m-d H:i:s');
		
		$enddt = new DateTime($flash['enddt'], $gmt);
		$enddt->setTimezone($usertz);
		$enddate = $enddt->format('Y-m-d H:i:s');
		?>
        <div class="ninety">
        	<div class="fifty"><div class="sub-title">Flash Sale</div>
        <form method="post" action="editables.php">
        <select name="visibility">
            	<option value="enable" <? if ($flash['visibility'] == 'enable') {echo 'selected';} ?>>Enable</option>
            	<option value="disable" <? if ($flash['visibility'] == 'disable') {echo 'selected';} ?>>Disable</option>
         </select>
        	<select name="p">
            	<option value="case" <? if ($flash['p'] == 'case') {echo 'selected';} ?>>Case</option>
            	<option value="pack" <? if ($flash['p'] == 'pack') {echo 'selected';} ?>>Pack</option>
            	<option value="coin" <? if ($flash['p'] == 'coin') {echo 'selected';} ?>>Coin flip</option>
            </select>
        	<input type="number" style="width:90px;" min="1" name="pid" placeholder="Product ID" value="<? echo $flash['productid'] ?>" required/>
        	<input type="text" size="10" name="discount" placeholder="Discount %" value="<? echo $flash['discount'] ?>" required/>
            <input type="hidden" name="usertz" id="usertz" value="" /><br><br>
Start Date-time (24 hour format)<br>
            <input type="text" id="startdatetime" data-format="YYYY-MM-DD HH:mm:ss" data-template="DD / MM / YYYY     HH : mm" name="startdt" value="<? echo $startdate; ?>"><br><br>
End Date-time<br>
			<input type="text" id="enddatetime" data-format="YYYY-MM-DD HH:mm:ss" data-template="DD / MM / YYYY     HH : mm" name="enddt" value="<? echo $enddate; ?>"><br>
            <input id="flashsalesubmit" type="submit" class="button green" name="flashsale" value="Update" style="margin:5px;" required/>
        </form>
        <script type="text/javascript">
		$(function() {
			$("#startdatetime, #enddatetime").combodate({
				minYear: 2017,
        		maxYear: 2018,
				smartDays: true,
				firstItem: 'none',
				minuteStep: 1
			});
			var tz = jstz.determine();
			$("#usertz").val(tz.name());
			
		});
		</script></div><div class="fifty"><div class="sub-title">VIP Settings</div>
        	<form method="post" action="editables.php">
        	<div class="fifty">
    <?
	$vipfree = $mysqli->query("SELECT `content` FROM rsj_editables WHERE `name` = 'vipfree'")->fetch_object()->content;
	$vipfreearray = explode(',',$vipfree);
	
	
	?>
        	
        		
        		VIP Price<br>
        		<input type="text" size="10" name="vipprice" value="<? echo $data['vip'] ?>" required/>
        	</div><div class="fifty">
        	Free Case<br>
			<?php 
				$casequery = $mysqli->query("SELECT * FROM rsj_case WHERE `case_price` = 0 ");
				$caseproduct = mysqli_fetch_all($casequery,MYSQLI_ASSOC);
				
			
			?>
        		<select name="freecasers3[]" id="multi-select" multiple="multiple" required>
        			<?
					if(!empty($caseproduct))
					{
						foreach($caseproduct as $val)
						{
							if (in_array($val['id'], $vipfreearray)) 
							{
								$sel = 'selected="selected"';
							}
							else
							{
								$sel= '';
							}
							echo '<option value="'.$val['id'].'"' .$sel.'>'.$val['case_name'].'</option>';
						}
					}
				?>
        		</select>
        		<br><br>
        		VIP Discount %<br>
        		<input type="text" size="10" name="vipdiscount" value="<? echo $data['vipdiscount'] ?>" required/>
        	</div>
        	<br>
<input type="submit" class="button green" name="setvip" value="Update" style="margin:5px;" />
        	</form></div>
        </div>
        
        <div class="ninety">
        	<div class="fifty">
        		<div class="sub-title">Free Case Giveaways</div>
        		<form method="post" action="editables.php">
        			<select name="freecases">
        				<option value="enabled" <? if ($data['freecases'] == 'enabled') {echo 'selected';} ?>>Enabled</option>
        				<option value="disabled" <? if ($data['freecases'] == 'disabled') {echo 'selected';} ?>>Disabled</option>
        			</select>
        			<input type="submit" name="updatefreecases" class="button green" value="Update" />
        		</form>
        	</div><div class="fifty">
        		<div class="sub-title">Top Items List</div>
        		<form method="post" action="editables.php">
        			<textarea style="width: 450px; height: 100px;" name="topitems" placeholder="Top Items seperated by commas (case sensitive)"><? echo $data['topitems']; ?></textarea><br>
        			<input type="submit" name="updatetopitems" class="button green" value="Update" style="margin: 5px;" />
        		</form>
        	</div>
        </div>
        
        <!---->
		</div>
	
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>