<?php
ob_start();
session_name('rsjAdmin');
session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	$incomingurl = $_SERVER["HTTP_REFERER"];
	if(isset($_POST['product'])) {
		if ($_POST['product'] == 'case') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$priority = '5,5,5,5,5,4,4,4,3,3,3,2,2,1';
			$price = $_POST['price'];
			$type = $_POST['type'];
			if ($mysqli->query("INSERT INTO rsj_products (`name`, `desc`, `items`, `priority`, `price`, `type`) VALUES ('".$name."', '".$desc."', '".$items."', '".$priority."', '".$price."', '".$type."')")) {
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error While inserting values in database.';
			}
		}
		else if ($_POST['product'] == 'pack') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$price = $_POST['price'];
			$type = $_POST['type'];
			if ($mysqli->query("INSERT INTO rsj_packs (`name`, `desc`, `items`, `price`, `type`) VALUES ('".$name."', '".$desc."', '".$items."', '1.99', '".$type."')")) {
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error While inserting values in database.';
			}
		
			
		}
		else if ($_POST['product'] == 'coin') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$price = $_POST['price'];
			$type = $_POST['type'];
			if ($mysqli->query("INSERT INTO rsj_coins (`name`, `desc`, `items`, `price`, `type`) VALUES ('".$name."', '".$desc."', '".$items."', '".$price."', '".$type."')")) {
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error While inserting values in database.';
			}
			
		}
		else {
			echo 'Error: Data tampered.';
		}
	}
	else {
			header("Location: products.php");
    		exit;
	}
}
else {
	header("Location: index.php");
    exit;
}
?>