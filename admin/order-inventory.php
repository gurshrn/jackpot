<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include('config.php');
include_once "pagination.php";
if (isset($_SESSION['username'])) {
	$baseurl =  "http://" . $_SERVER['SERVER_NAME'].'/jackpot/admin/';
	$orderId = $_GET['id'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="<?php echo $baseurl.'style.css'?>" rel="stylesheet" type="text/css">
</head>

<body>
<?php
		
		$inventory = $mysqli->query("SELECT * FROM rsj_order_item WHERE order_id = '".$orderId."' ORDER BY id DESC");
		
		$results = $mysqli->query("SELECT COUNT(*) as totalCount FROM rsj_order_item WHERE order_id = '".$orderId."'");
		$rec = $results->fetch_array();
    	$total = $rec['totalCount'];
?>
<div id="superwrap">
  <div id="top-header-wrap">
    <div id="top-header"> <a href="dashboard.php" id="logo"></a>
      <div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
    </div>
  </div>
  <div class="main">
    <div class="title">Order ID : <?php echo $orderId;?></div>
    <form action="delivery.php" method="post">
    	<div class="top-controls group">
        <?php echo 'Total Records: <strong>'.$total.'</strong>'; ?>
    	<input type="submit" name="deliver" value="Mark Delivered" class="button green" />
    	<input type="submit" name="undeliver" value="Mark Undelivered" class="button red" />
        
        <a href="javascript:window.location.href=window.location.href" class="button">Refresh</a>
        <a href="orders.php" class="button">+ Back</a>
        </div>
    <table class="orders">
		<tr>
			<th>Item</th>
			<th>Timestamp GMT</th>
			<th>Delivery</th>
			<th>Proof</th>
		</tr>
		<?php 
			if(!empty($inventory))
			{
				foreach($inventory as $val)
				{
					echo '<tr>';
					echo '<td>'.ucfirst($val['order_item']).'</td>';
					echo '<td>'.$val['date'].'</td>';
					if ($val['delivery'] == 'tbd') 
					{
						echo '<td class="not-delivered"><input type="checkbox" name="orderId[]" value="'.$val['id'].'" />';
					}
					else 
					{
						echo '<td class="delivered"><input type="checkbox" name="orderId[]" value="'.$val['id'].'" /><br>'.$val['delivered_by'];
					}
					if (empty($val['proof'])) 
					{
							echo '<td class="not-delivered" style="white-space:nowrap;">
							<a style="cursor:pointer;" onClick="proof'.$val['id'].'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$val['id'].'() {
								var screen = prompt("Enter direct link to screenshot", "");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$val['id'].'";
								}
								else {
									alert("Cannot be empty!");
								}
							}
							</script>
							</td>';
					}
					else 
					{
						echo '<td class="delivered" style="white-space:nowrap;">
							<a href="http://'.$val['proof'].'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							<a style="cursor:pointer;" onClick="proof'.$val['id'].'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$val['id'].'() {
								var screen = prompt("Enter direct link to screenshot", "'.$val['proof'].'");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$val['id'].'";
								}
							}
							</script>
							</td>';
					}
					echo '</tr>';
				}
			}
			else
			{
				echo '<tr><td colspan="4">No order inventory found....</td></tr>';
			}
			
	 
		?>
    </table></form>
    <!-- Page links goes here -->
	
<? 		 ?>
  </div>
</div>
</body>
</html>
<?php } else {
header("Location: index.php");
exit;
}
?>