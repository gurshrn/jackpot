<?
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
if (!isset($_SESSION['username'])) {
	header("Location: index.php");
    exit;
}
else {
	$incomingurl = $_SERVER["HTTP_REFERER"];
	if(!empty($_GET)) {
		if (isset($_GET['proof']) && isset($_GET['id']) && !empty($_GET['proof']) && !empty($_GET['id'])) {
			$proof = $_GET['proof'];
			if (!empty($proof)) {
				$pattern = '/(?:https:\/\/)?(?:[a-zA-Z0-9.-]+?\.(?:[a-zA-Z])|\d+\.\d+\.\d+\.\d+)/';
				if (preg_match($pattern, $proof)) {
					$id = $_GET['id'];
					$mysqli->query("UPDATE rsj_order_item SET proof = '{$proof}' WHERE id = '{$id}'");
					header("Location: ".$incomingurl);
    				exit;
				}
				else {
					echo 'Invalid URL';
				}
			}
			else {
				echo 'Link cannot be empty. <a href="orders.php">Go back</a>';
			}
			
		}
		else {
			echo 'Error: Data tampered.';
		}
	}
	else {
			header("Location: orders.php");
    		exit;
	}
}
?>