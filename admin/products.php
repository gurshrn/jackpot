<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
	$casesquery = $mysqli->query("SELECT * FROM rsj_products ORDER BY rsj_products.id");
	$packsquery = $mysqli->query("SELECT * FROM rsj_packs ORDER BY rsj_packs.id");
	$coinsquery = $mysqli->query("SELECT * FROM rsj_coins ORDER BY rsj_coins.id");
	?>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title"> Products </div>
        
        <div class="sub-title group">Cases
        <a href="addproduct1.php?product=case" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Case</a>
        </div>
        <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Name</th>
      		<th style="width:5%;">Price</th>
      		<th style="width:50%;">Items</th>
            <th style="width:5%;">Type</th>
            <th style="width:10%;">Action</th>
      	</tr>';
		while ($case = $casesquery->fetch_assoc()) {
			$id = $case['id'];
			$name = $case['name'];
			$price = $case['price'];
			$items = $case['items'];
			$type = $case['type'];
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$name.'</td>
			<td>'.$price.'</td>
			<td>'.$items.'</td>
			<td>'.$type.'</td>
			<td style="text-align:center;">
			<form action="editproduct.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="hidden" value="case" name="product" />
			<input type="submit" class="button green" name="editproduct" value="Edit" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
        <div class="sub-title group">Packs<a href="addproduct.php?product=pack" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Pack</a></div>
        
        <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Name</th>
      		<th style="width:5%;">Price</th>
      		<th style="width:50%;">Items</th>
            <th style="width:5%;">Type</th>
            <th style="width:10%;">Action</th>
      	</tr>';
		while ($pack = $packsquery->fetch_assoc()) {
			$id = $pack['id'];
			$name = $pack['name'];
			$price = $pack['price'];
			$items = $pack['items'];
			$type = $pack['type'];
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$name.'</td>
			<td>'.$price.'</td>
			<td>'.$items.'</td>
			<td>'.$type.'</td>
			<td style="text-align:center;">
			<form action="editproduct.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="hidden" value="pack" name="product" />
			<input type="submit" class="button green" name="editproduct" value="Edit" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
        <div class="sub-title group group">Coins<a href="addproduct.php?product=coin" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Coin</a></div>
        
        <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Name</th>
      		<th style="width:5%;">Price</th>
      		<th style="width:50%;">Items</th>
            <th style="width:5%;">Type</th>
            <th style="width:10%;">Action</th>
      	</tr>';
		while ($coin = $coinsquery->fetch_assoc()) {
			$id = $coin['id'];
			$name = $coin['name'];
			$price = $coin['price'];
			$items = $coin['items'];
			$type = $coin['type'];
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$name.'</td>
			<td>'.$price.'</td>
			<td>'.$items.'</td>
			<td>'.$type.'</td>
			<td style="text-align:center;">
			<form action="editproduct.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="hidden" value="coin" name="product" />
			<input type="submit" class="button green" name="editproduct" value="Edit" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
		</div>
    
</body>
</html><?php
}
else {
	header("Location: index.php");
	exit;
}
?>