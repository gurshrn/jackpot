<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	
	if (isset($_POST['editproduct'])) {
		$p = $_POST['product'];
		$id = $_POST['id'];
		if ($p == 'case') {
			$query = $mysqli->query("SELECT * FROM rsj_products WHERE id = '{$id}'");
			$product = $query->fetch_assoc();
			$placeholder = "Items (Seperated by comma) and Corresponding to Probability key (5,5,5,5,5,4,4,4,3,3,3,2,2,1). MAX : 14 or you'll break shit.";
		}
		else if ($p == 'coin') {
			$query = $mysqli->query("SELECT * FROM rsj_coins WHERE id = '{$id}'");
			$product = $query->fetch_assoc();
			$placeholder = "Items (Seperated by comma). MAX : 2 or you'll break shit. Item with 40% probability first.";
		}
		else if ($p == 'pack') {
			$query = $mysqli->query("SELECT * FROM rsj_packs WHERE id = '{$id}'");
			$product = $query->fetch_assoc();
			$placeholder = "Items (Seperated by comma). Put better items first.";
		}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Edit Product: <? echo $product['name']; ?></div>
        <div class="content" style="text-align: center;">
        <form action="processproduct.php" method="post">
        <input type="hidden" name="product" value="<? echo $p; ?>" />
        <input type="hidden" name="id" value="<? echo $id; ?>" />
        	<input type="text" size="30" name="name" placeholder="Name" value="<? echo $product['name']; ?>" required/>
        	<input type="text" size="30" name="price" placeholder="Price" value="<? echo $product['price']; ?>" required/>
            <select name="type" required>
            	<option value="rs3" <?php if ($product['type'] == 'rs3') { echo 'selected';} ?>>RS3</option>
            	<option value="07"	<?php if ($product['type'] == '07') { echo 'selected';} ?>>07</option>
            </select>
            <select name="desc" required>
            	<option value="red" <?php if ($product['desc'] == 'red') { echo 'selected';} ?>>Red</option>
            	<option value="green" <?php if ($product['desc'] == 'green') { echo 'selected';} ?>>Green</option>
            	<option value="blue" <?php if ($product['desc'] == 'blue') { echo 'selected';} ?>>Blue</option>
            	<option value="gold" <?php if ($product['desc'] == 'gold') { echo 'selected';} ?>>Gold</option>
            	<option value="white" <?php if ($product['desc'] == 'white') { echo 'selected';} ?>>White</option>
            	<option value="lightgray" <?php if ($product['desc'] == 'lightgray') { echo 'selected';} ?>>Light Gray</option>
            	<option value="lightblue" <?php if ($product['desc'] == 'lightblue') { echo 'selected';} ?>>Light Blue</option>
            </select><br>
            <textarea name="items" rows="5" placeholder="<? echo $placeholder; ?>" style="width:582px;" required><?php echo $product['items']; ?></textarea><br>
            <input type="submit" class="button green" name="editproduct" value="Edit" />
            <a href="products.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
            <input type="submit" class="button red" name="deleteproduct" value="Delete Product" style="margin-left:200px;" onclick="return confirm('Are you sure you want to continue?')" />
        </form>
        
        </div>
		</div>
    <?php
	}
	else {
		header("Location: products.php");
		exit;
	}
	?>
</body>
</html>
<?php
}
else {
	header("Location: index.php");
	exit;
}
?>