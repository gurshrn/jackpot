<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	
	$p = $_GET['product'];
	if ($p == 'case' or $p == 'coin' or $p == 'pack') {
		if ($p == 'case') {
			$placeholder = "Items (Seperated by comma) and Corresponding to Probability key (5,5,5,5,5,4,4,4,3,3,3,2,2,1). MAX : 14 or you'll break shit.";
		}
		else if ($p == 'coin') {
			$placeholder = "Items (Seperated by comma). MAX : 2 or you'll break shit. Item with 40% probability first.";
		}
		else if ($p == 'pack') {
			$placeholder = "Items (Seperated by comma). Put better items first.";
		}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Add Product: <? echo $p; ?></div>
        <div class="content" style="text-align: center;">
        <form action="processproduct.php" method="post">
        <input type="hidden" name="product" value="<? echo $p; ?>" />
        	<input type="text" size="30" name="name" placeholder="Name" required/>
        	<input type="text" size="30" name="price" placeholder="Price" required/>
            <select name="type" required>
            	<option value="rs3">RS3</option>
            	<option value="07">07</option>
            </select>
            <select name="desc" required>
            	<option value="red" selected>Red</option>
            	<option value="green">Green</option>
            	<option value="blue">Blue</option>
            	<option value="gold">Gold</option>
            	<option value="white">White</option>
            	<option value="lightgray">Light Gray</option>
            	<option value="lightblue">Light Blue</option>
            </select><br>
            <textarea name="items" rows="5" placeholder="<?php echo $placeholder; ?>" style="width:582px;" required></textarea><br>
            <?php
		if ($p == 'case') {
			echo '<input type="checkbox" name="viponly" value="yes" id="viponly" />&nbsp;<label for="viponly">VIP only</label>
           <br><br>';
		}
		?>
			
            <input type="submit" class="button green" name="addproduct" value="Add" />
            <a href="products.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
        </form>
        
        </div>
		</div>
</body>
</html>
<?php
	}
	else {
		header("Location: products.php");
		exit;
	}
}
else {
	header("Location: index.php");
	exit;
}
?>