<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include('config.php');
require_once 'ElasticEmailClient.php';
    
ElasticEmailClient\ApiClient::SetApiKey("73921124-9630-43eb-bdcb-de78281e9694");

if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) == $_SERVER['HTTP_HOST']) {

function SendEmail($recipients, $subject, $from, $fromName, $text, $html) {	
        $EEemail = new ElasticEmailClient\Email();
        
        try
        {
            $response = $EEemail->Send($subject, $from, $fromName, null, null, null, null, null, null, $recipients, array(), array(), array(), array(), array(), null, null, $html, $text);		
        }
        catch (Exception $e)
        {
            echo 'Something went wrong: ', $e->getMessage(), '\n';
            
            return;
        }		
        
        //echo 'MsgID to store locally: ', $response->messageid, '\n'; // Available only if sent to a single recipient
        $emailtxnid = $response->transactionid;
		if ($emailtxnid) {
			$_SESSION['message'] = 'Broadcast successful.';
			header("Location: broadcast.php");
			exit;
		}
    }
	
	
	$results = $mysqli->query("SELECT `email` FROM `rsj_members` WHERE `broadcast`='yes'");
	$emails = array();
	while ($emailassoc = $results->fetch_assoc()) {
    	$emails[] =  $emailassoc['email'];  
	}
	
	$prefix = $emaillist = '';
	foreach ($emails as $email)
	{
    $emaillist .= $prefix . '"' . $email . '"';
    $prefix = ', ';
	}
	
    $recipients = [ $emaillist ];
    $fromEmail = "no-reply@rsjackpot.org";
    $fromName = "RSJackpot";
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	$bodyHtml = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>RSJackPot - Updates</title>
<style type="text/css">
@media(max-width:480px){
  table[class=main_table],table[class=layout_table]{width:300px !important;}
  table[class=layout_table] tbody tr td.header_image img{width:271px !important;height:auto !important;}
}
	.main_table {
		background: #2d2d2d;
	}
	.header_image {
		text-align:  center;
		background: #336799;
		padding: 5px;
		color:  white;
		font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
		font-size: 11px;
	}
	p {
		font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
		color: white;
	}
	h1 {
		font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
		color: #CCC;
	}
	.section td {
		border-bottom:  1px solid #444;
	}
	a {
		color: #ccc !important;
		text-decoration:  none;
		border-bottom: 1px dotted #fff;
	}
</style>
</head>
<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr>
<td align="center" valign="top">
<!--  M A I N T A B L E  S T A R T  -->
   <table border="0" cellpadding="0" cellspacing="0" class="main_table" width="600">
    <tbody>
    <tr>
    <td>
      <!--  L A Y O U T _ T A B L E  S T A R T  -->
    <table border="0" cellpadding="0" cellspacing="0" class="layout_table" style="border-collapse:collapse;border: 3px solid #336799;" width="100%" >
      <tbody>
      <!--  H E A D E R R O W  S T A R T  -->
      <tr>
      <td align="left" class="header_image"><a href="https://rsjackpot.org/" style="border:none;"><img src="https://rsjackpot.org/assets/images/logo.png" width="235px" height="50px" style="border:0;display:inline-block;"></a></td>
      </tr>
      <!--  H E A D E R R O W  E N D  -->
      <tr><td style="font-size:13px;line-height:13px;margin:0;padding:0;">&nbsp;</td></tr>
      <!--  B O D Y R O W  S T A R T  -->
      <tr>
      <td align="center" valign="top">
        <table align="center" cellpadding="0" cellspacing="0" width="85%">
          <tbody>
          
          <!--  VARIABLE  -->
          '.$message.'
          <!--  VARIABLE  -->
          </tbody>
        </table>
      </td>
      </tr>
      <!--  B O D Y R O W  E N D  -->
      <tr><td style="font-size:13px;line-height:13px;margin:0;padding:0;">&nbsp;</td></tr>
      <!--  F O O T E R R O W  S T A R T  -->
      <tr>
      <td align="left" class="header_image">
      	<a href="https://v2.zopim.com/widget/livechat.html?api_calls=%5B%5D&hostname=rsjackpot.org&key=3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s&lang=en&mid=f2gDedxEJ5CK8Q">24/7 Live Chat</a> | <a href="http://rsjackpot.org/unsubscribe.php">Unsubscribe here</a>
      </td>
      </tr>
      <!--  F O O T E R R O W  E N D  -->
      </tbody>
    </table>
    <!--  L A Y O U T _ T A B L E  E N D  -->
    </td>
    </tr>
    </tbody>
</table>
<!--  M A I N _ T A B L E  E N D  -->
</td>
</tr>
</tbody>
</table>
</body>
</html>';
	$bodyText = $_POST['textmessage'];
    
    SendEmail($recipients, $subject, $fromEmail, $fromName, $bodyText, $bodyHtml);
	
}
else {
	header("Location: broadcast.php");
	exit;
}
}
else {
	header("Location: index.php");
	exit;
}
?>