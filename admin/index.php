<?php
ob_start();
include_once('session.php');
secure_session_start();
if (isset($_SESSION['username'])) {
	header("Location: dashboard.php");
    exit;
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="duramater">
  <div class="arachnoidmater">
    <div class="piamater">
      <div id="login-logo"></div>
      <form method="post" action="dashboard.php">
        <div class="login-wrap">
          <div class="form-unit" style="text-align:left;">Username:
            <input name="username" type="text" autofocus required="required" />
          </div>
          <div class="form-unit" style="text-align:right;">Password:
            <input name="password" type="password" required="required"/>
          </div>
          <div class="form-unit" style="margin-top: 30px;">
            <input type="submit" class="f-button" value="Login"/>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</body>
</html>