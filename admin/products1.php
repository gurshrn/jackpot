<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
	$casesquery = $mysqli->query("SELECT rsj_case.*,rsj_category.category_name,rsj_sub_category.sub_category_name as case_type FROM rsj_case INNER JOIN rsj_category ON rsj_case.cat_id = rsj_category.id INNER JOIN rsj_sub_category ON rsj_case.case_type = rsj_sub_category.id ORDER BY rsj_case.id DESC");
	$cases = $casesquery->fetch_all(MYSQLI_ASSOC);
	
	$query = $mysqli->query("SELECT * FROM rsj_category ORDER BY id DESC");
	$category = $query->fetch_all(MYSQLI_ASSOC);
	
	$query1 = $mysqli->query("SELECT rsj_sub_category.*,rsj_category.category_name FROM rsj_sub_category INNER JOIN rsj_category ON rsj_category.id = rsj_sub_category.cat_id ORDER BY rsj_sub_category.id DESC");
	$subCat = $query1->fetch_all(MYSQLI_ASSOC);
?>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title"> Products </div>
		
		<div class="sub-title group">Category
        <a href="category.php" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Category</a>
        </div>
		
		 <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Category</th>
      		<th style="width:10%;">Action</th>
      	</tr>';
		foreach($category as $val) {

			$id = $val['id'];
			$categoryName = ucfirst($val['category_name']);
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$categoryName.'</td>
			<td style="text-align:center;">
			<form action="category.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="hidden" value="case" name="product" />
			<input type="submit" class="button green" name="editproduct" value="Edit" />
			</form></td>
			</td>
			</tr>';
		}
		echo '</table>';
		?>
		
		<div class="sub-title group">Sub Category
        <a href="sub-category.php" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Sub Category</a>
        </div>
		
		 <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Category</th>
     		<th style="width:25%;">Sub Category</th>
      		<th style="width:10%;">Action</th>
      	</tr>';
		foreach($subCat as $val1) {

			$id = $val1['id'];
			$categoryName = ucfirst($val1['category_name']);
			$subCat = ucfirst($val1['sub_category_name']);
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$categoryName.'</td>
			<td>'.$subCat.'</td>
			<td style="text-align:center;">
			<form action="sub-category.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="submit" class="button green" name="editSubCategory" value="Edit" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
        
        <div class="sub-title group">Products
        <a href="addproduct1.php" class="f-button" style="vertical-align:text-bottom; float: right;">+ Add Product</a>
        </div>
        <?php
		echo '
        <table class="orders">
        <tr>
      		<th style="width:5%;">ID</th>
     		<th style="width:25%;">Name</th>
      		<th style="width:5%;">Price</th>
      		<th style="width:40%;">Items</th>
            <th style="width:10%;">Category Name</th>
            <th style="width:10%;">Type</th>
            <th style="width:10%;">Action</th>
      	</tr>';
		foreach($cases as $case) {

			$query = $mysqli->query("SELECT * FROM rsj_case_items WHERE case_id = '{$case['id']}'");
			$caseitems1 = $query->fetch_all(MYSQLI_ASSOC);

			$items = array();
			$itemType = array();
			foreach($caseitems1 as $val)
			{
				$items[] = ucfirst($val['item_name']);
			}
			
			$id = $case['id'];
			$name = ucfirst($case['case_name']);
			$price = $case['case_price'];
			$item = implode(",",$items);
			$type = $case['case_type'];
			echo '<tr>
			<td>'.$id.'</td>
			<td>'.$name.'</td>
			<td>'.number_format($price,2).'</td>
			<td>'.$item.'</td>
			<td>'.$case['category_name'].'</td>
			<td>'.$type.'</td>
			<td style="text-align:center;">
			<form action="editproduct.php" method="post">
			<input type="hidden" value="'.$id.'" name="id" />
			<input type="hidden" value="case" name="product" />
			<input type="submit" class="button green" name="editproduct" value="Edit" />
			</form></td>
			</tr>';
		}
		echo '</table>';
		?>
        
    
</body>
</html><?php
}
else {
	header("Location: index.php");
	exit;
}
?>