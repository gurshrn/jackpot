<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
if (isset($_SESSION['username'])) {
if (isset($_GET['id']) && !empty($_GET['id'])) {
	$id = $_GET['id'];
	$results = $mysqli->query("SELECT * FROM `rsj_members` WHERE `id` = {$id}");
	$user = $results->fetch_assoc();
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Viewing user : <? echo $user['usr']; ?></div>
        <div class="sub-title">Uploads</div>
        
        <? if (empty($user['uploads'])) {
			echo '<div class="message">No uploads.</div>';
		}
		else {
			$uploads = explode(',', $user['uploads']);
			foreach ($uploads as $upload) {
				echo '<div class="content">';
				echo '<a href="../acc_uploads/'.$upload.'" target="_blank" class="styled-link">'.$upload.'</a><br>';
				echo '</div>';
			}
		}
		?>
		</div>
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>