<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');

if (isset($_POST['username'])) {
	$username = $mysqli->real_escape_string($_POST['username']);
	$password = $mysqli->real_escape_string($_POST['password']);	
	
	//$remoteip = explode('.', $_SERVER['REMOTE_ADDR']);
	//$results = $mysqli->query("SELECT * FROM rsj_admin WHERE username='{$username}' AND pass='{$password}'");
	
	$stmt = $mysqli->prepare("SELECT username,pass,perm FROM rsj_admin WHERE username=? AND pass=?");
	$stmt->bind_param('ss', $username, $password);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($user, $pass, $perm);
	$stmt->fetch();
	
	
	
	
	//$row = $results->fetch_assoc();
	//$ip = explode ('.', $row['ip']);
	if ($stmt->num_rows == 1) {
		$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$user."', 'Logged in', 'Login', 'User and Pass submitted: ".$username." ".$password."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
		$_SESSION['username'] = $user;
		$_SESSION['perm'] = $perm;
		
		header("Location: dashboard.php");
		exit;
	}
	else {
		$_SESSION = array();
 
				// get session parameters 
				$params = session_get_cookie_params();
 
				// Delete the actual cookie. 
				setcookie(session_name(),
        		'', time() - 42000, 
        		$params["path"], 
        		$params["domain"], 
        		$params["secure"], 
        		$params["httponly"]);
 
				// Destroy session 
				session_destroy();
		echo '<!doctype html><html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body><div id="top-header-wrap">
    <div id="top-header"> <a href="/admin" id="logo"></a>
      <div class="clear"></div></div></div>
      <div class="main">
        <div class="title">Incorrect Login</div><br>
		<center><a href="index.php" class="f-button">LOG IN</a></center>
		</div></body></html>';
		exit;
     }
}

if (isset($_SESSION['username'])) {
	if (isset($_GET['logout'])) {			   
			   $_SESSION = array();
 
				// get session parameters 
				$params = session_get_cookie_params();
 
				// Delete the actual cookie. 
				setcookie(session_name(),
        		'', time() - 42000, 
        		$params["path"], 
        		$params["domain"], 
        		$params["secure"], 
        		$params["httponly"]);
 
				// Destroy session 
				session_destroy();
			   	header("Location: index.php");
               	exit;
    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="orders.php">Orders</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Welcome, <?php echo $_SESSION['username']; ?>!</div>
        <div class="touch-pad">
        	<div class="pad-wrap full">
        		<a href="orders.php" class="pad">
                	<img src="images/orders.png" alt="View Orders" /><br>
                    Orders
                </a>
        	</div><div class="pad-wrap full">
        		<a href="search.php" class="pad">
                	<img src="images/search.png" alt="Search" /><br>
                    Search
                </a>
        	</div><?php 
			if ($_SESSION['perm'] == 'admin') {
			echo '<div class="pad-wrap full">
        		<a href="products1.php" class="pad"><img src="images/product.png" alt="Product" />Products</a>
        	</div><div class="pad-wrap full">
        		<a href="editables.php" class="pad"><img src="images/editables.png" alt="Editables" />Editables</a>
        	</div><div class="pad-wrap two">
        		<a href="feedbacks.php" class="pad"><img src="images/feedback.png?v=2" alt="Feedbacks" />Feedbacks</a>
				<a href="vip.php" class="pad"><img src="images/vip.png" alt="VIP Payments" />VIP</a>
        	</div><div class="pad-wrap two">
        		<a href="workers.php" class="pad">
                	<img src="images/worker.png" alt="Add/Remove Workers" width="64px" height="64px" />
                    Workers
                </a>
				<a href="broadcast.php" class="pad">
                	<img src="images/broadcast.png" alt="Broadcast E-mail" />
                    Broadcasts
                </a>
        	</div>';
			}
            ?>
        </div>
		</div>
</body>
</html>
    <?php
}
else {
	header("Location: index.php");
	exit;
}
?>