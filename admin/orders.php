<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include('config.php');
include_once "pagination.php";
if (isset($_SESSION['username'])) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
			
	  $rec_limit = 50;
	  if(isset($_GET{'page'}) && $_GET{'page'} >= 1) {
            $page = $_GET{'page'} - 1;
            $offset = $rec_limit * $page ;
       }
	   else {
            $page = 0;
            $offset = 0;
       }
	if ($_SESSION['perm'] == 'admin') 
	{
		$query = $mysqli->query("SELECT * FROM rsj_payments ORDER BY rsj_payments.id DESC LIMIT $offset, $rec_limit"); 
		$order = $mysqli->query("SELECT * FROM rsj_order_payment ORDER BY id DESC LIMIT $offset, $rec_limit"); 
		$orderData = mysqli_fetch_all($order,MYSQLI_ASSOC);
		
		
	}
	else 
	{
		$query = $mysqli->query("SELECT * FROM rsj_payments WHERE payment_status = 'Completed' ORDER BY rsj_payments.id DESC LIMIT $offset, $rec_limit");
		$order = $mysqli->query("SELECT * FROM rsj_order_payment WHERE payment_status = 'Completed' ORDER BY id DESC LIMIT $offset, $rec_limit");
	}	
	
    	$results = $mysqli->query("SELECT COUNT(*) as totalCount FROM rsj_order_payment");
		$rec = $results->fetch_array();
    	$total = $rec['totalCount'];
		
		$results1 = $mysqli->query("SELECT COUNT(*) as totalCount FROM rsj_payments");
		$rec1 = $results1->fetch_array();
    	$total1 = $rec1['totalCount'];
		
		$totalCount = $total+$total1;
			?>
<div id="superwrap">
  <div id="top-header-wrap">
    <div id="top-header"> <a href="dashboard.php" id="logo"></a>
      <div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
    </div>
  </div>
  <div class="main">
    <div class="title">Orders</div>
    <form action="delivery.php" method="post">
    	<div class="top-controls group">
        <?php echo 'Total Records: <strong>'.$total.'</strong>'; ?>
    	
        
        <a href="search.php" class="button">Search</a>
        <a href="javascript:window.location.href=window.location.href" class="button">Refresh</a>
        <a href="add-order.php" class="button">+ Add</a>
        </div>
    <table class="orders">
      <tr>
        <th>ID</th>
        <th>Product</th>
        <th>Price</th>
        <th>Username</th>
        <th>IP</th>
        <th>Trans ID</th>
        <th>Order Status</th>
        <th>Paid Amount</th>
        <th>Coupon</th>
        <th>Item</th>
        <th>Total Case</th>
		<th>Opened Case</th>
        <th>Timestamp GMT</th>
      </tr>
      <?php 
			
			foreach($orderData as $orders)
			{
				
				
				$orderid = $orders['order_id'];
				$userId = $orders['user_id'];
				$paymentStatus = $orders['payment_status'];
				$txnId  = $orders['txn_id'];
				$datetime  = $orders['createdtime'];
				$paidAmount  = $orders['paid_amount'];
				$delivery  = $orders['delivery'];
				$ip  = $orders['ip'];
				$by = $orders['delivered_by'];
				$proof = $orders['proof'];
				$coupon = $orders['coupen'];
				$status = $orders['status'];
				 
				 $query2 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$userId}");
				
				 $user = $query2->fetch_assoc();
				 
				 
				
				$product = $mysqli->query("SELECT * FROM rsj_order_detail WHERE order_id='{$orderid}'");
				$productCase = $product->fetch_assoc();
				
				$orderItems = $mysqli->query("SELECT COUNT(*) as totalCount FROM rsj_order_item WHERE order_id = '".$orderid."'");
				$purchaseQty = $orderItems->fetch_array();
				$totalPurchaseQty = $purchaseQty['totalCount'];
				
				
				
				echo '<tr>
					<td><a href="order-inventory.php?id='.$orderid.'" class="get-order-inventory" data-target="'.$orderid.'">'.$orderid.'</td>
        			<td>'.ucfirst($productCase['case_name']).'</td>
        			<td>'.number_format($productCase['case_price'],2).'</td>
        			<td style="white-space:nowrap;"><a style="border-radius:3px 0 0 3px;" href="user.php?id='.$userId.'">'.$user["usr"].'</a><a href="search.php?username='.$user['usr'].'&searchuser=Search"  style="border-radius:0 3px 3px 0; margin-left:-1px;"><img src="images/find.png" style="width:12px; height:12px;" /></a></td>
        			<td>'.$ip.'</td>
					<td>'.$txnId.'</td>
					<td>'.$paymentStatus.'</td>
        			<td>'.number_format($paidAmount,2).'</td>
					<td>'.$coupon.'</td>
        			<td>'.$productCase['case_items'].'</td>
        			<td>'.$productCase['case_qty'].'</td>
        			<td>'.$totalPurchaseQty.'</td>
        			<td>'.$datetime.'</td>';
					
					
					
					echo '</tr>';
				
			}
			// while($row = $query->fetch_assoc()) {
				// $id = $row['id'];
				// $productid = $row['productid'];
				// $userid = $row['userid'];
				// $payment = $row['payment_status'];
				// $transid = $row['txnid'];
				// $time = $row['createdtime'];
				// $amount = $row['payment_amount'];
				// $item = $row['item'];
				// $casestatus = $row['case_status'];
				// $delivery = $row['delivery'];
				// $ip = $row['ip'];
				// $by = $row['deliveredby'];
				// $proof = $row['proof'];
				// $coupon = $row['coupon'];
				// $p = $row['product'];
				
				
			// $query2 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$userid}");
			// $user = $query2->fetch_assoc();
			
			// if ($p == 'pack') {
				// $query3 = $mysqli->query("SELECT * FROM `rsj_packs` WHERE id={$productid}");
			// }
			// else if ($p == 'coin') {
				// $query3 = $mysqli->query("SELECT * FROM `rsj_coins` WHERE id={$productid}");
			// }
			// else {
				// $query3 = $mysqli->query("SELECT * FROM `rsj_products` WHERE id={$productid}");
			// }
			// $product = $query3->fetch_assoc();
				// echo '<tr>
					// <td>'.$id.'</td>
        			// <td>'.$product["name"].'</td>
        			// <td>'.$product['price'].'</td>
        			// <td style="white-space:nowrap;"><a style="border-radius:3px 0 0 3px;" href="user.php?id='.$userid.'">'.$user["usr"].'</a><a href="search.php?username='.$user['usr'].'&searchuser=Search"  style="border-radius:0 3px 3px 0; margin-left:-1px;"><img src="images/find.png" style="width:12px; height:12px;" /></a></td>
        			// <td>'.$ip.'</td>';
					// if (filter_var($transid, FILTER_VALIDATE_URL)) {
        				// echo '<td><a href="'.$transid.'" target="_blank">Paid GP</a></td>';
					// }
					// else {
						// echo '<td>'.$transid.'</td>';
					// }
					
					// echo'
        			// <td>'.$payment.'</td>
        			// <td>'.$amount.'</td>
					// <td>'.$coupon.'</td>
        			// <td>'.$item.'</td>
        			// <td>'.$casestatus.'</td>
        			// <td>'.$time.'</td>';
					
					// if ($casestatus == 'closed') {
        				// echo '<td class="neutral"><input type="checkbox" name="id" disabled/>';
					// }
					// else {
						// if ($delivery == 'tbd') {
							// echo '<td class="not-delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" />';
						// }
						// else {
							// echo '<td class="delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" /><br>'.$by;
						// }
					// }
					// echo '</td>';
					// if (empty($proof)) {
							// echo '<td class="not-delivered" style="white-space:nowrap;">
							// <a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							// <script type="text/javascript">
							// function proof'.$id.'() {
								// var screen = prompt("Enter direct link to screenshot", "");
								// if (screen) {
								// var fscreen = screen.replace(/.*?:\/\//g, "")
									// document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								// }
								// else {
									// alert("Cannot be empty!");
								// }
							// }
							// </script>
							// </td>';
					// }
					// else {
						// echo '<td class="delivered" style="white-space:nowrap;">
							// <a href="http://'.$proof.'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							// <a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							// <script type="text/javascript">
							// function proof'.$id.'() {
								// var screen = prompt("Enter direct link to screenshot", "'.$proof.'");
								// if (screen) {
								// var fscreen = screen.replace(/.*?:\/\//g, "")
									// document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								// }
							// }
							// </script>
							// </td>';
					// }
					// echo '</tr>';
	  //}	  
	  ?>
    </table></form>
    <!-- Page links goes here -->
<? 		echo displayPaginationBelow($rec_limit,$page+1,$total); ?>
  </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
		// jQuery(document).on('click','.get-order-inventory',function(){
			// var orderId = $(this).attr('data-attr');
			// jQuery.ajax({
            // method:"POST",           
            // url: 'order-inventory.php',
            // data: {orderId:orderId},            
            // dataType: "html",           
            // success: function (data)            
            // {
               
            // }       
        // });
		// });
</script>
</body>
</html>
<?php } else {
header("Location: index.php");
exit;
}
?>

