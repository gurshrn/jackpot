<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include('config.php');
include_once "pagination.php";
if (isset($_SESSION['username'])) {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
</head>

<body>
<div id="superwrap">
  <div id="top-header-wrap">
    <div id="top-header"> <a href="dashboard.php" id="logo"></a>
      <div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
    </div>
  </div>
  <div class="main">
    <div class="title">Search</div>
    <div class="content">
    	<div class="fifty">
        <form method="get" action="search.php">
        	<input type="text" name="username" placeholder="Search by Username" size="40" min="4" required/>
            <input class="button green" type="submit" name="searchuser" value="Search" />
        </form>
        </div><div class="fifty" style="text-align:right;">
        <form method="get" action="search.php">
        	<input type="text" name="txnid" placeholder="Search by Transaction ID" size="40" required/>
            <input class="button green" type="submit" name="searchtxnid" value="Search" />
        </form>
        </div>
        </div>
     <?   
	if (isset($_GET['searchuser']) && !empty($_GET['username'])) {
		$key = $mysqli->real_escape_string($_GET['username']);
		$userids = $mysqli->query("SELECT id FROM rsj_members WHERE usr LIKE '%".$key."%'");
		if($userids->num_rows === 0) {
			echo '<div class="sub-title">
        			Your search returned no results.
        			</div>';
		}
		else {
			echo '<div class="sub-title">
        			Results
        			</div>';
			echo '<form action="delivery.php" method="post">
    	<div class="top-controls group" id="top-controls">
    	<input type="submit" name="deliver" value="Mark Delivered" class="button green" />
    	<input type="submit" name="undeliver" value="Mark Undelivered" class="button red" />
        </div>';
			echo '<table class="orders">
      				<tr>
      				<th>ID</th>
     				<th>Product</th>
      				<th>Price</th>
      				<th>Username</th>
      				<th>IP</th>
     				<th>Trans ID</th>
    				<th>Order Status</th>
        			<th>Paid Amount</th>
        			<th>Coupon</th>
        			<th>Item</th>
        			<th>Case Status</th>
        			<th>Timestamp GMT</th>
        			<th>Delivery</th>
        			<th>Proof</th>
      				</tr>';
					$records = 0;
		while ($userid = $userids->fetch_array()) {
			$key = $userid['id'];
			
			$query1 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$key}");
			$user = $query1->fetch_assoc();
			
			$query2 = $mysqli->query("SELECT * FROM rsj_payments WHERE userid={$key} ORDER BY rsj_payments.id DESC");
			
			$order = $mysqli->query("SELECT * FROM rsj_order_payment WHERE user_id={$key} ORDER BY id DESC");
			
			$records += $query2->num_rows;
			$records1 += $order->num_rows;
			while($orders = $order->fetch_assoc())
			{
				$orderid = $orders['order_id'];
				$userId = $orders['user_id'];
				$paymentStatus = $orders['payment_status'];
				$txnId  = $orders['txn_id'];
				$datetime  = $orders['createdtime'];
				$paidAmount  = $orders['paid_amount'];
				$delivery  = $orders['delivery'];
				$ip  = $orders['ip'];
				$by = $orders['delivered_by'];
				$proof = $orders['proof'];
				$coupon = $orders['coupen'];
				$status = $orders['status'];
				
				$query2 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$userId}");
				$user = $query2->fetch_assoc();
				
				$product = $mysqli->query("SELECT * FROM rsj_order_detail WHERE order_id={$orderid}");
				$productCase = $product->fetch_assoc();
				
				
				echo '<tr>
					<td>'.$orderid.'</td>
        			<td>'.$productCase['case_name'].'</td>
        			<td>'.number_format($productCase['case_price'],2).'</td>
        			<td style="white-space:nowrap;"><a style="border-radius:3px 0 0 3px;" href="user.php?id='.$userId.'">'.$user["usr"].'</a><a href="search.php?username='.$user['usr'].'&searchuser=Search"  style="border-radius:0 3px 3px 0; margin-left:-1px;"><img src="images/find.png" style="width:12px; height:12px;" /></a></td>
        			<td>'.$ip.'</td>
					<td>'.$txnId.'</td>
					<td>'.$paymentStatus.'</td>
        			<td>'.number_format($paidAmount,2).'</td>
					<td>'.$coupon.'</td>
        			<td>'.$productCase['case_items'].'</td>
        			<td>'.$status.'</td>
        			<td>'.$datetime.'</td>';
					
					if ($status == 'closed') {
        				echo '<td class="neutral"><input type="checkbox" name="id" disabled/>';
					}
					else {
						if ($delivery == 'tbd') {
							echo '<td class="not-delivered"><input type="checkbox" name="orderId" value="'.$orderid.'" />';
						}
						else {
							echo '<td class="delivered"><input type="checkbox" name="orderId" value="'.$orderid.'" /><br>'.$by;
						}
					}
					echo '</td>';
					if (empty($proof)) {
							echo '<td class="not-delivered" style="white-space:nowrap;">
							<a style="cursor:pointer;" onClick="proof'.$orderid.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$orderid.'() {
								var screen = prompt("Enter direct link to screenshot", "");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$orderid.'";
								}
								else {
									alert("Cannot be empty!");
								}
							}
							</script>
							</td>';
					}
					else {
						echo '<td class="delivered" style="white-space:nowrap;">
							<a href="http://'.$proof.'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							<a style="cursor:pointer;" onClick="proof'.$orderid.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$orderid.'() {
								var screen = prompt("Enter direct link to screenshot", "'.$proof.'");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$orderid.'";
								}
							}
							</script>
							</td>';
					}
					echo '</tr>';
				
			}
			while($row = $query2->fetch_assoc()) {
				$id = $row['id'];
				$productid = $row['productid'];
				$payment = $row['payment_status'];
				$transid = $row['txnid'];
				$time = $row['createdtime'];
				$amount = $row['payment_amount'];
				$item = $row['item'];
				$casestatus = $row['case_status'];
				$delivery = $row['delivery'];
				$by = $row['deliveredby'];
				$ip = $row['ip'];
				$proof = $row['proof'];
				$coupon = $row['coupon'];
				$p = $row['product'];
				
				
				if ($p == 'pack') {
					$query3 = $mysqli->query("SELECT * FROM rsj_packs WHERE id={$productid}");
				}
				else if ($p == 'coin') {
					$query3 = $mysqli->query("SELECT * FROM rsj_coins WHERE id={$productid}");
				}
				else {
					$query3 = $mysqli->query("SELECT * FROM rsj_products WHERE id={$productid}");
				}
				$product = $query3->fetch_assoc();
				echo '<tr>
					<td>'.$id.'</td>
					
        			<td>'.$product["name"].'</td>
        			<td>'.$product["price"].'</td>
        			<td><a href="user.php?id='.$userid['id'].'">'.$user["usr"].'</a></td>
        			<td>'.$ip.'</td>';
					if (filter_var($transid, FILTER_VALIDATE_URL)) {
        				echo '<td><a href="'.$transid.'" target="_blank">Paid GP</a></td>';
					}
					else {
						echo '<td>'.$transid.'</td>';
					}
					
					echo'
        			<td>'.$payment.'</td>
        			<td>'.$amount.'</td>
					<td>'.$coupon.'</td>
        			<td>'.$item.'</td>
        			<td>'.$casestatus.'</td>
        			<td>'.$time.'</td>'; 
					if ($casestatus == 'closed') {
        				echo '<td class="neutral"><input class="not-delivered" type="checkbox" name="id" disabled/>';
					}
					else {
						if ($delivery == 'tbd') {
							echo '<td class="not-delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" />';
						}
						else {
							echo '<td class="delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" /><br>'.$by;
						}
					}
					echo '</td>';
					if (empty($proof)) {
							echo '<td class="not-delivered" style="white-space:nowrap;">
							<a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$id.'() {
								var screen = prompt("Enter direct link to screenshot", "");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								}
								else {
									alert("Cannot be empty!");
								}
							}
							</script>
							</td>';
					}
					else {
						echo '<td class="delivered" style="white-space:nowrap;">
						<a href="http://'.$proof.'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							<a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$id.'() {
								var screen = prompt("Enter direct link to screenshot", "'.$proof.'");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								}
							}
							</script>
							</td>';
					}
					echo '</tr>';
			}
		}
		$totalRecords = $records+$records1;
		echo '</table></form><div class="content" id="records">Found records : <strong>'.$totalRecords.'</strong></div>';
		}
	}
	else if (isset($_GET['searchtxnid']) && !empty($_GET['txnid'])) {
		$keyx = $mysqli->real_escape_string($_GET['txnid']);
		
		$payments = $mysqli->query("SELECT * FROM rsj_payments WHERE txnid = '".$keyx."' ORDER BY rsj_payments.id DESC");
		
		$orderPayments = $mysqli->query("SELECT * FROM rsj_order_payment WHERE txn_id = '".$keyx."' ORDER BY id DESC");
		
		if($payments->num_rows === 0 && $orderPayments->num_rows === 0) {
			echo '<div class="sub-title">
        			Your search returned no results.
        			</div>';
		}
		else {
			echo '<div class="sub-title">
        			Results
        			</div>';
			echo '<form action="delivery.php" method="post">
    	<div class="top-controls group">Found records : <strong>'.$payments->num_rows.'</strong>
    	<input type="submit" name="deliver" value="Mark Delivered" class="button green" />
    	<input type="submit" name="undeliver" value="Mark Undelivered" class="button red" />
        </div>';
			echo '<table class="orders">
      				<tr>
      				<th>ID</th>
     				<th>Product</th>
      				<th>Price</th>
      				<th>Username</th>
      				<th>IP</th>
     				<th>Trans ID</th>
    				<th>Order Status</th>
        			<th>Paid Amount</th>
        			<th>Coupon</th>
        			<th>Item</th>
        			<th>Case Status</th>
        			<th>Timestamp GMT</th>
        			<th>Delivery</th>
        			<th>Proof</th>
      				</tr>';
			while($orders = $orderPayments->fetch_assoc())
			{
				$orderid = $orders['order_id'];
				$userId = $orders['user_id'];
				$paymentStatus = $orders['payment_status'];
				$txnId  = $orders['txn_id'];
				$datetime  = $orders['createdtime'];
				$paidAmount  = $orders['paid_amount'];
				$delivery  = $orders['delivery'];
				$ip  = $orders['ip'];
				$by = $orders['delivered_by'];
				$proof = $orders['proof'];
				$coupon = $orders['coupen'];
				$status = $orders['status'];
				
				$query2 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$userId}");
				$user = $query2->fetch_assoc();
				
				$product = $mysqli->query("SELECT * FROM rsj_order_detail WHERE order_id={$orderid}");
				$productCase = $product->fetch_assoc();
				
				
				echo '<tr>
					<td>'.$orderid.'</td>
        			<td>'.$productCase['case_name'].'</td>
        			<td>'.number_format($productCase['case_price'],2).'</td>
        			<td style="white-space:nowrap;"><a style="border-radius:3px 0 0 3px;" href="user.php?id='.$userId.'">'.$user["usr"].'</a><a href="search.php?username='.$user['usr'].'&searchuser=Search"  style="border-radius:0 3px 3px 0; margin-left:-1px;"><img src="images/find.png" style="width:12px; height:12px;" /></a></td>
        			<td>'.$ip.'</td>
					<td>'.$txnId.'</td>
					<td>'.$paymentStatus.'</td>
        			<td>'.number_format($paidAmount,2).'</td>
					<td>'.$coupon.'</td>
        			<td>'.$productCase['case_items'].'</td>
        			<td>'.$status.'</td>
        			<td>'.$datetime.'</td>';
					
					if ($status == 'closed') {
        				echo '<td class="neutral"><input type="checkbox" name="id" disabled/>';
					}
					else {
						if ($delivery == 'tbd') {
							echo '<td class="not-delivered"><input type="checkbox" name="orderId" value="'.$orderid.'" />';
						}
						else {
							echo '<td class="delivered"><input type="checkbox" name="orderId" value="'.$orderid.'" /><br>'.$by;
						}
					}
					echo '</td>';
					if (empty($proof)) {
							echo '<td class="not-delivered" style="white-space:nowrap;">
							<a style="cursor:pointer;" onClick="proof'.$orderid.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$orderid.'() {
								var screen = prompt("Enter direct link to screenshot", "");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$orderid.'";
								}
								else {
									alert("Cannot be empty!");
								}
							}
							</script>
							</td>';
					}
					else {
						echo '<td class="delivered" style="white-space:nowrap;">
							<a href="http://'.$proof.'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							<a style="cursor:pointer;" onClick="proof'.$orderid.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$orderid.'() {
								var screen = prompt("Enter direct link to screenshot", "'.$proof.'");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "payment-proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$orderid.'";
								}
							}
							</script>
							</td>';
					}
					echo '</tr>';
				
			}
		while($row = $payments->fetch_assoc()) {
				$id = $row['id'];
				$productid = $row['productid'];
				$userid = $row['userid'];
				$payment = $row['payment_status'];
				$transid = $row['txnid'];
				$time = $row['createdtime'];
				$amount = $row['payment_amount'];
				$item = $row['item'];
				$casestatus = $row['case_status'];
				$delivery = $row['delivery'];
				$by = $row['deliveredby'];
				$ip = $row['ip'];
				$proof = $row['proof'];
				$coupon = $row['coupon'];
				$p = $row['product'];
				
				
				if ($p == 'pack') {
					$query3 = $mysqli->query("SELECT * FROM rsj_packs WHERE id={$productid}");
				}
				else if ($p == 'coin') {
					$query3 = $mysqli->query("SELECT * FROM rsj_coins WHERE id={$productid}");
				}
				else {
					$query3 = $mysqli->query("SELECT * FROM rsj_products WHERE id={$productid}");
				}
				
			$query2 = $mysqli->query("SELECT * FROM rsj_members WHERE id={$userid}");
			
			$user = $query2->fetch_assoc();
			$product = $query3->fetch_assoc();
			
			$username = $user["usr"];
			$productname = $product["name"];
			$price = $product['price'];
				echo '<tr>
					<td>'.$id.'</td>
        			<td>'.$productname.'</td>
        			<td>'.$price.'</td>
        			<td><a href="user.php?id='.$userid.'">'.$username.'</a></td>
        			<td>'.$ip.'</td>';
					if (filter_var($transid, FILTER_VALIDATE_URL)) {
        				echo '<td><a href="'.$transid.'" target="_blank">Paid GP</a></td>';
					}
					else {
						echo '<td>'.$transid.'</td>';
					}
					
					echo'
        			<td>'.$payment.'</td>
        			<td>'.$amount.'</td>
					<td>'.$coupon.'</td>
        			<td>'.$item.'</td>
        			<td>'.$casestatus.'</td>
        			<td>'.$time.'</td>';
					
					if ($casestatus == 'closed') {
        				echo '<td class="neutral"><input class="not-delivered" type="checkbox" name="id" disabled/>';
					}
					else {
						if ($delivery == 'tbd') {
							echo '<td class="not-delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" />';
						}
						else {
							echo '<td class="delivered"><input type="checkbox" name="'.$id.'" value="'.$id.'" /><br>'.$by;
						}
					}
					echo '</td>';
					if (empty($proof)) {
							echo '<td class="not-delivered" style="white-space:nowrap;">
							<a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$id.'() {
								var screen = prompt("Enter direct link to screenshot", "");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								}
								else {
									alert("Cannot be empty!");
								}
							}
							</script>
							</td>';
					}
					else {
						echo '<td class="delivered" style="white-space:nowrap;">
						<a href="http://'.$proof.'" target="_blank"><img src="images/image.png" style="width:16px;" /></a>
							<a style="cursor:pointer;" onClick="proof'.$id.'()"><img src="images/edit.png" /></a>
							<script type="text/javascript">
							function proof'.$id.'() {
								var screen = prompt("Enter direct link to screenshot", "'.$proof.'");
								if (screen) {
								var fscreen = screen.replace(/.*?:\/\//g, "")
									document.location.href = "proof.php?proof="+encodeURIComponent(fscreen)+"&id='.$id.'";
								}
							}
							</script>
							</td>';
					}
					echo '</tr>';
	  }	
			echo '</table></form>';
		}
	}
	if ((isset($_GET['searchtxnid']) && empty($_GET['txnid'])) or (isset($_GET['searchuser']) && empty($_GET['username'])) or (isset($_GET['username']) && empty($_GET['username'])) or (isset($_GET['txnid']) && empty($_GET['txnid']))) {
		header("Location: search.php");
		exit;
	}
	?>
  </div>
</div>
<script type="text/javascript">
$(function() {
	$('#records').contents().appendTo('#top-controls');
});
</script>

</body>
</html><? } else {
header("Location: index.php");
exit;
}
?>