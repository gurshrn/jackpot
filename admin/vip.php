<?php
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require('config.php');
include_once "pagination.php";
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
				
	  $rec_limit = 50;
	  if(isset($_GET{'page'}) && $_GET{'page'} >= 1) {
            $page = $_GET{'page'} - 1;
            $offset = $rec_limit * $page ;
       }
	   else {
            $page = 0;
            $offset = 0;
       }
			$query = $mysqli->query("SELECT * FROM rsj_vip ORDER BY rsj_vip.id DESC LIMIT $offset, $rec_limit"); 
		$results = $mysqli->query("SELECT COUNT(*) as totalCount FROM rsj_vip");
		$rec = $results->fetch_array();
    	$total = $rec['totalCount'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald' rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">VIP Payments</div>
		  <div class="top-controls group"><? echo 'Total Records: <strong>'.$total.'</strong>'; ?></div>
		  <table class="orders">
		  	<tr>
        		<th>ID</th>
        		<th>Username</th>
        		<th>IP</th>
        		<th>Trans ID</th>
        		<th>Paid Amount</th>
        		<th>Order Status</th>
        		<th>Timestamp GMT</th>
        		<th>Expiration GMT</th>
        		<th>Free Case type</th>
      		</tr>
      		<? while($row = $query->fetch_assoc()) {
				$id = $row['id'];
				$transid = $row['txnid'];
				$amount = $row['payment_amount'];
				$payment = $row['payment_status'];
				$userid = $row['userid'];
				$user = $row['username'];
				$createdtime = $row['createdtime'];
				$expiration = $row['expiration'];
				$ip = $row['ip'];
				$fct = $row['freecasetype'];
				echo '<tr>
				<td>'.$id.'</td>
				<td>'.$user.'</td>
				<td>'.$ip.'</td>
				<td>'.$transid.'</td>
				<td>'.$amount.'</td>
				<td>'.$payment.'</td>
				<td>'.$createdtime.'</td>
				<td>'.$expiration.'</td>
				<td>'.$fct.'</td>
				</tr>';
			}
			?>
		  </table>
		  <? echo displayPaginationBelow($rec_limit,$page+1,$total); ?>
		</div>
</body>
</html>
    <?
}
else {
	header("Location: index.php");
	exit;
}
?>