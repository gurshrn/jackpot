<?
ob_start();
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'config.php';
if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) {
	$incomingurl = $_SERVER["HTTP_REFERER"];
	$refData = parse_url($incomingurl);
	if($refData['host'] == 'rsjackpot.org') {
	if(isset($_POST['product'])) {
		if ($_POST['product'] == 'case') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$priority = '5,5,5,5,5,4,4,4,3,3,3,2,2,1';
			$price = $_POST['price'];
			$type = $_POST['type'];
			$vip = $_POST['viponly'];
			
			if (isset($_POST['addproduct'])) 
			{ 
				if ($mysqli->query("INSERT INTO rsj_products (`name`, `desc`, `items`, `priority`, `price`, `type`, `viponly`) VALUES ('".$name."', '".$desc."', '".$items."', '".$priority."', '".$price."', '".$type."', '".$vip."')")) {
					$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
					header("Location: products.php");
					exit;
				}
				else 
				{
					echo 'Error while inserting values in database.';
				}
			}
			
			else if (isset($_POST['editproduct'])) 
			{ 
				$id = $_POST['id'];
				if ($mysqli->query("UPDATE rsj_products SET `name` = '{$name}', `desc` = '{$desc}', `items` = '{$items}', `price` = '{$price}', `type` = '{$type}' WHERE id={$id}")) 
				{
					$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
					header("Location: products.php");
					exit;
				}
				else 
				{
					echo 'Error while Updating values in database.';
				}
			}
			
			else if (isset($_POST['deleteproduct'])) { 
			$id = $_POST['id'];
			if ($mysqli->query("DELETE FROM rsj_products WHERE id={$id}")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deleted product', 'Products', 'Name: ".$name."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while Updating values in database.';
			}
			}
		}
		else if ($_POST['product'] == 'pack') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$price = $_POST['price'];
			$type = $_POST['type'];
			
			if (isset($_POST['addproduct'])) {
			if ($mysqli->query("INSERT INTO rsj_packs (`name`, `desc`, `items`, `price`, `type`) VALUES ('".$name."', '".$desc."', '".$items."', '1.99', '".$type."')")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while inserting values in database.';
			}
			}
			
			else if (isset($_POST['editproduct'])) { 
			$id = $_POST['id'];
			if ($mysqli->query("UPDATE rsj_packs SET `name` = '{$name}', `desc` = '{$desc}', `items` = '{$items}', `price` = '{$price}', `type` = '{$type}' WHERE id={$id}")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while Updating values in database.';
			}
			}
			
			else if (isset($_POST['deleteproduct'])) { 
			$id = $_POST['id'];
			if ($mysqli->query("DELETE FROM rsj_packs WHERE id={$id}")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deleted product', 'Products', 'Name: ".$name."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while Updating values in database.';
			}
			}
		
			
		}
		else if ($_POST['product'] == 'coin') {
			$name = $_POST['name'];
			$desc = $_POST['desc'];
			$items = $_POST['items'];
			$price = $_POST['price'];
			$type = $_POST['type'];
			if (isset($_POST['addproduct'])) {
			if ($mysqli->query("INSERT INTO rsj_coins (`name`, `desc`, `items`, `price`, `type`) VALUES ('".$name."', '".$desc."', '".$items."', '".$price."', '".$type."')")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Added product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while inserting values in database.';
			}
			}
			
			
			else if (isset($_POST['editproduct'])) { 
			$id = $_POST['id'];
			if ($mysqli->query("UPDATE rsj_coins SET `name` = '{$name}', `desc` = '{$desc}', `items` = '{$items}', `price` = '{$price}', `type` = '{$type}' WHERE id={$id}")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Edited product', 'Products', 'Name: ".$name." Items: ".$items."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while Updating values in database.';
			}
			}
			
			else if (isset($_POST['deleteproduct'])) { 
			$id = $_POST['id'];
			if ($mysqli->query("DELETE FROM rsj_coins WHERE id={$id}")) {
				$mysqli->query("INSERT INTO rsj_adminlog (`user`, `action`, `section`, `details`, `ip`, `dt`) VALUES ('".$_SESSION['username']."', 'Deleted product', 'Products', 'Name: ".$name."', '".$_SERVER['REMOTE_ADDR']."', '".gmdate("Y-m-d H:i:s")."')");
				header("Location: products.php");
    			exit;
			}
			else {
				echo 'Error while Updating values in database.';
			}
			}
			
		}
		else {
			echo 'Error: Data tampered.';
		}
	}
	else {
			header("Location: products.php");
    		exit;
	}
}
else {
	echo 'Run as fast as you can.';
	exit;
}
}
else {
	header("Location: index.php");
    exit;
}
?>