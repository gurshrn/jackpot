<?php
	ob_start();
	include_once('session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	require('config.php');
	if (isset($_SESSION['username']) && isset($_SESSION['perm']) && ($_SESSION['perm'] == 'admin')) 
	{
		$goldPrice = $mysqli->query("SELECT content FROM rsj_editables where name='goldprice'");
		$getGoldPrice = $goldPrice->fetch_all(MYSQLI_ASSOC);
		
		$productCat = $mysqli->query("SELECT * FROM rsj_category");
		$category = $productCat->fetch_all(MYSQLI_ASSOC);
		
		$productSubCat = $mysqli->query("SELECT * FROM rsj_sub_category");
		$subCategory = $productSubCat->fetch_all(MYSQLI_ASSOC);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>fuserPanel - RSJackpot</title>
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Oswald" rel="stylesheet" type="text/css">
<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
	
    <div id="top-header-wrap">
    	<div class="top-header group"><a href="dashboard.php" id="logo"></a>
    		<div class="nav-wrap">
        	  	<div class="nav">
                	<a href="/" target="_blank">Website</a>
                	<a href="dashboard.php">Dashboard</a>
                    <a href="dashboard.php?logout">Log out</a>
                </div>
      		</div>
        </div>
    </div>
      <div class="main">
        <div class="title">Add Product: <? echo $p; ?></div>
        <div class="content" style="text-align: center;">
        <form action="processproduct1.php" method="post" enctype="multipart/form-data">
			<div class="tp_sctn">
			<input type="hidden" class="gold-price" name="gold_price" value="<?php echo $getGoldPrice[0]['content'];?>">
        <input type="hidden" name="product" value="<?php echo $p; ?>" />
			<select name="category_name" class="catName" required>
				<option value="">Select Category...</option>
				<?php 
					
					if(isset($category) && !empty($category))
					{ 
						foreach($category as $val)
						{ 
				?>
							<option value="<?php echo $val['id']; ?>"><?php echo $val['category_name'];?></option>
							
				<?php } } ?>
            </select>
        	<input type="text" size="20" name="case_name" placeholder="Name" required/>
            <input type="text" size="20" class="casePrice" name="case_price" placeholder="Price" required readonly/>
             <select  name="case_type" class="subCategory" required>
				<option value="">Select Sub Category...</option>
            	<?php 
					//if(isset($subCategory) && !empty($subCategory))
					//{ 
						//foreach($subCategory as $val1)
						//{ 
				?>
							<!--option value="<?php //echo $val1['id']; ?>"><?php //echo $val1['sub_category_name'];?></option-->
							
				<?php //} } ?>
            </select>
			<select name="case_color" required>
            	<option value="red" selected>Red</option>
            	<option value="green">Green</option>
            	<option value="blue">Blue</option>
            	<option value="gold">Gold</option>
            	<option value="white">White</option>
            	<option value="lightgray">Light Gray</option>
            	<option value="lightblue">Light Blue</option>
			</select> 
			</div>
			
			<div class="gpValue inpt-sctn">
			<input type="hidden" name="top_item[]">
				<p><input type="checkbox" class="top-item">Top Item</p>
				<input type="text" size="20" name="item_name[]" placeholder="Item Name" required/>
            <input type="text" size="20" name="item_unique_name[]" placeholder="Item unique name" required/>
            <input type="text" class="odds-value" size="20" name="odds[]" placeholder="Odds" required/>
        	<input type="text" class="gp-value" size="20" name="gp_value[]" placeholder="GP value" required/>
            <select name="color[]" required>
            	<option value="green" selected>Green</option>
            	<option value="red">Red</option>
            	<option value="pink">Pink</option>
            	<option value="purple">Purple</option>
            	<option value="blue">Blue</option>
            </select> 
            <input type="file" name="item_image[]" required>
			<a href="javascript:void(0)" class="addItem">+</a>
			</div>
            <div class="input_fields_wrap"></div>
            
            <input type="submit" class="button green" name="addproduct" value="Add" />
            <a href="products1.php" class="button red" style="font-size:13.3333px;" />Cancel</a>
        </form>
        
        </div>
		</div>
</body>
</html>
<?php
	
}
else {
	header("Location: index.php");
	exit;
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    var max_fields      = 5;
    var wrapper         = $(".input_fields_wrap");
    $(document).off('click','.addItem').on('click','.addItem',function(e){
        e.preventDefault();
        var x = 1;

        if(x < max_fields){
            x++;
            $(wrapper).append('<div class="form-hr gpValue clearfix"><input type="hidden" name="top_item[]"><p><input type="checkbox" class="top-item">Top Item</p><input type="text" size="20" name="item_name[]" placeholder="Item Name" required/><input type="text" size="20" name="item_unique_name[]" placeholder="Item unique name" required/><input type="text" size="20" class="odds-value" name="odds[]"  placeholder="Odds" required/><input type="text" size="20" name="gp_value[]" class="gp-value" placeholder="GP value" required/><select name="color[]" required><option value="green" selected>Green</option><option value="red">Red</option><option value="pink">Pink</option><option value="purple">Purple</option><option value="blue">Blue</option></select><input type="file" name="item_image[]" required><a href="javascript:void(0)" class="remove_fields">-</a></div>');
            
        }
    });
    $(wrapper).on("click",".remove_fields", function(e)
    {
        e.preventDefault();
        $(this).parent('div').remove();
		
    });
</script>
<script>
	$(document).on('change','.gpValue',function(){
		casePriceCal();
	});
	$(document).on('click','.remove_fields',function(){
		casePriceCal();
	});
	function casePriceCal()
	{
		
		var totalCal = 0;
		var goldprice = $('.gold-price').val();
		$('.gpValue').each(function(){
			gpValue =$(this).find('.odds-value').val();
			odds =$(this).find('.gp-value').val();
			if(gpValue != '' && odds != '')
			{
				calOddsPer = (parseFloat(gpValue)*parseFloat(odds))/100;
				totalCal += parseFloat(calOddsPer);
			}
			
		});
		var casePrice = parseFloat(totalCal)*parseFloat(goldprice);
		$('.casePrice').val(casePrice.toFixed(2));
	}
	$(document).on('change','.catName',function()
	{
		var catId = $(this).val();
		if(catId != '')
		{
			jQuery.ajax({
				type: "POST",
				url: 'get-cat-subcat.php',
				data:{catId:catId},
				dataType: "html",
				success: function (data)
				{
					$('.subCategory').html(data);
				}
			});
		}
		
		
	});
	$(document).on('change','.top-item',function(){
		if($(this).is(':checked'))
		{
			$(this).parent('p').prev('input').val('yes');
		}
		else
		{
			$(this).parent('p').prev('input').val('');
		}
		
	});
	$(document).on('change','.subCategory',function()
	{
		var subCatId = $(this).val();
		if(subCatId != '')
		{
			jQuery.ajax({
				type: "POST",
				url: 'get-goldprice.php',
				data:{subCatId:subCatId},
				dataType: "html",
				success: function (data)
				{
					$('.gold-price').val(data);
				}
			});
		}
		
		
	});
</script>