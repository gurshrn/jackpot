<?php
include_once( 'session.php' );
secure_session_start();
define( 'INCLUDE_CHECK', true );
require 'config.php';
if ( !isset( $_SESSION[ 'id' ] ) ) 
{
	$_SESSION[ 'returnUrl' ] = $_SERVER[ 'REQUEST_URI' ];
	header( "Location: login.php" );
	exit;
}
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RSJackpot - Inventory</title>
	<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
	<link href="assets/css/style.css?v=1.21" rel="stylesheet" type="text/css">
	<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/favicon/manifest.json">
	<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#2d89ef">
	<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="assets/js/fuser.js"></script>
	<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
	<!--Start of Zopim Live Chat Script-->
	<script type="text/javascript">
		window.$zopim || ( function ( d, s ) {
			var z = $zopim = function ( c ) {
					z._.push( c )
				},
				$ = z.s =
				d.createElement( s ),
				e = d.getElementsByTagName( s )[ 0 ];
			z.set = function ( o ) {
				z.set.
				_.push( o )
			};
			z._ = [];
			z.set._ = [];
			$.async = !0;
			$.setAttribute( "charset", "utf-8" );
			$.src = "//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";
			z.t = +new Date;
			$.
			type = "text/javascript";
			e.parentNode.insertBefore( $, e )
		} )( document, "script" );
	</script>
	<!--End of Zopim Live Chat Script-->
	<? if (isset($_SESSION['usr'])) { ?>
	<script type="text/javascript">
		$zopim( function () {
			$zopim.livechat.setName( '<? echo $_SESSION['usr'] ?>' );
		} );
	</script>
	<? } ?>
</head>

<body>
	<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>';
		if ((isset($_SESSION['vip'])) && ($_SESSION['vip'] != 0)) {
			echo '<div class="vip-label"></div>';
		}
		echo'
		<ul id="nav"><li><a href="http://customer-devreview.com/jackpot/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		
		 echo '</ul> 

		</div>
		</div>'; ?>
	<div class="content-wrap">
		<div class="page-title">Inventory</div>
		<div class="main-content group">



			<?
		if ((isset($_SESSION['vip'])) && ($_SESSION['vip'] != 0)) 
		{
			$stmt = $mysqli->prepare("SELECT lastvipcase FROM rsj_members WHERE `id` =?");
			$stmt->bind_param("s", $_SESSION['id']);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($lastvipcase);
			$stmt->fetch();
			
			if ($lastvipcase == '0000-00-00 00:00:00') 
			{
				
				echo '<div class="mini-title">Monthly VIP Free Cases</div>
					<div class="freecases">
					<form method="post" action="vipcase.php">
					Your 2 monthly free VIP cases are available. Claim them now!<br><br>
    				<input type="submit" class="button" style="vertical-align:middle;" name="claimviprs3" value="RS3 VIP Cases" />
					&nbsp;&nbsp;OR&nbsp;&nbsp;
				<input type="submit" class="button" style="vertical-align:middle;" name="claimvip07" value="07 VIP Cases" />
				&nbsp;&nbsp;OR&nbsp;&nbsp;
				<input type="submit" class="button" style="vertical-align:middle;" name="claimvipboth" value="Each of Both" />
					</form>
    				</div>';
			}
			else 
			{
				$gmt = new DateTimeZone('GMT');
				$currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
			
				$checkdt = new DateTime($lastvipcase, $gmt);
				$checkdt->modify('+30 day');
			
			
				if ($currentdt > $checkdt) 
				{
					echo '<div class="mini-title">Monthly VIP Free Cases</div>
					<div class="freecases">
					<form method="post" action="vipcase.php">
					Your 2 monthly free VIP cases are available. Claim them now!<br><br>
    				<input type="submit" class="button" style="vertical-align:middle;" name="claimviprs3" value="RS3 VIP Cases" />
					&nbsp;&nbsp;OR&nbsp;&nbsp;
					<input type="submit" class="button" style="vertical-align:middle;" name="claimvip07" value="07 VIP Cases" />
					&nbsp;&nbsp;OR&nbsp;&nbsp;
					<input type="submit" class="button" style="vertical-align:middle;" name="claimvipboth" value="Each of Both" />
					</form>
    				</div>';
				}
				else {
					$timeleft = $checkdt->diff($currentdt);
					echo '<div class="mini-title">Monthly Free VIP Cases</div>
					<div class="freecases">
    				You can claim your next free VIP cases in <br><br>
					<div class="timeleft">'.$timeleft->format('%a <span>Days</span> %H <span>Hours</span> %I <span>Minutes</span> %S <span>Seconds</span>').'</div>
    				</div>';
				}
			}
    		$stmt->close();
		}
		
		
		$fccheck = $mysqli->query("SELECT content FROM rsj_editables WHERE name = 'freecases'")->fetch_object()->content;
	
		if ($fccheck == 'enabled') 
		{
			$getOrders = $mysqli->query("SELECT rsj_order_payment.*,rsj_order_detail.* FROM rsj_order_payment LEFT JOIN rsj_order_detail ON rsj_order_payment.order_id = rsj_order_detail.order_id  WHERE rsj_order_payment.user_id='{$_SESSION['id']}' AND rsj_order_detail.status='closed' AND  rsj_order_payment.payment_status='Completed' AND rsj_order_payment.txn_id != 'Free Case'");

			$getClosedCase = mysqli_fetch_all($getOrders,MYSQLI_ASSOC);
			
			$stmt = $mysqli->prepare("SELECT lastfreecase FROM rsj_members WHERE `id` =?");
			$stmt->bind_param("s", $_SESSION['id']);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($lastfc);
			$stmt->fetch();
			$stmt->close();
			
			if ($lastfc == '0000-00-00 00:00:00') 
			{ 
				
				$results = $mysqli->query("SELECT * FROM rsj_case WHERE case_price = 0 GROUP BY cat_id");
				$cases = mysqli_fetch_all($results,MYSQLI_ASSOC);
				foreach($cases as $val)
				{
					$case[] = $val['cat_id'];
				}
				$catIds = implode(",",$case);
				
				$result1 = $mysqli->query("SELECT * FROM rsj_category WHERE id IN ($catIds)");
				$category = mysqli_fetch_all($result1,MYSQLI_ASSOC);
				
				
				
				echo '<div class="mini-title">Daily Free Case</div><div class="freecases">Your FIRST free case is available. Claim it now!<br><br>';
						if(!empty($category))
						{
							foreach($category as $val)
							{
								
								echo '<input type="button" class="button getSubCat" style="vertical-align:middle;" data-attr="'.$val['id'].'" value="'.$val['category_name'].'" />';
							}
						}
				
    			echo '<div class="subCat"></div></div>';
			}
			else 
			{ 
		
				$purchasecheck = $mysqli->query("SELECT * FROM rsj_order_payment WHERE user_id = '{$_SESSION['id']}' AND payment_status='Completed' AND txn_id = 'Free Case' ")->num_rows;
				
				if ($purchasecheck == 0)
				{ //if there is no purchase on record
					echo '<div class="mini-title">Daily Free Case</div>
						<div class="freecases">
						Daily Free cases are available within 1 month of a purchase!<br>
						</div>';
				}
			
			else 
			{ //Check last purchase date
				$lastpurchasedt = $mysqli->query("SELECT createdtime FROM rsj_order_payment WHERE user_id = '{$_SESSION['id']}' AND payment_status='Completed' AND txn_id = 'Free Case' ORDER BY id DESC LIMIT 1")->fetch_object()->createdtime;
				
				$gmt = new DateTimeZone('GMT');
				$currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
			
				$checkpurchasedt = new DateTime($lastpurchasedt, $gmt);
				$checkpurchasedt->modify('+30 day');
				
				if ($currentdt < $checkpurchasedt) 
				{ //if last purchase is within a month
					//Daily Last FC check
					$stmt = $mysqli->prepare("SELECT lastfreecase FROM rsj_members WHERE `id` =?");
					$stmt->bind_param("s", $_SESSION['id']);
					$stmt->execute();
					$stmt->store_result();
					$stmt->bind_result($lastfc);
					$stmt->fetch();
					$stmt->close();
					
					$gmt = new DateTimeZone('GMT');
					$currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
			
					$lastfcdt = new DateTime($lastfc, $gmt);
			
					$checkdt = new DateTime($lastfc, $gmt);
					$checkdt->modify('+1 day');
			
			
					if ($currentdt > $checkdt) 
					{ 
						$results = $mysqli->query("SELECT * FROM rsj_case WHERE case_price = 0 GROUP BY cat_id");
						$cases = mysqli_fetch_all($results,MYSQLI_ASSOC);
						foreach($cases as $val)
						{
							$case[] = $val['cat_id'];
						}
						$catIds = implode(",",$case);
						
						$result1 = $mysqli->query("SELECT * FROM rsj_category WHERE id IN ($catIds)");
						$category = mysqli_fetch_all($result1,MYSQLI_ASSOC);
						
						
						
						echo '<div class="mini-title">Daily Free Case</div><div class="freecases">Your FIRST free case is available. Claim it now!<br><br>';
						if(!empty($category))
						{
							foreach($category as $val)
							{
								
								echo '<input type="button" class="button getSubCat" style="vertical-align:middle;" data-attr="'.$val['id'].'" value="'.$val['category_name'].'" />';
							}
						}
						
						echo '<div class="subCat"></div></div>';
					}
					else 
					{
						$timeleft = $checkdt->diff($currentdt);
						echo '<div class="mini-title">Daily Free Case</div>
						<div class="freecases">
						You can claim your next free case in <br><br>
						<div class="timeleft">'.$timeleft->format('%a <span>Days</span> %H <span>Hours</span> %I <span>Minutes</span> %S <span>Seconds</span>').'</div>
						</div>';
					}
				}
				else 
				{
					echo '<div class="mini-title">Daily Free Case</div>
					<div class="freecases">
    				Daily Free cases are available within 1 month of a purchase!<br>
    				</div>';
				}
			}
			}
    	} // fccheck
	?>
	
	<?php 
		echo '<div id="freeCases"></div>'; 
		
		if(!empty($getClosedCase))
		{
			foreach($getClosedCase as $closedcase)
			{
				$totalCaseQty = $closedcase['purchase_qty'];
				for($i=1;$i<=$totalCaseQty;$i++)
				{
					$countOrderItems = $mysqli->query("SELECT * FROM rsj_order_item WHERE order_id='{$closedcase[ 'order_id' ]}'");
					$totalOrderItems = mysqli_fetch_all($countOrderItems,MYSQLI_ASSOC);
					
					$data1 .= '<div class="closed-case-wrap"><div class="closed-case group '.$closedcase['case_color'].'">
					 
					<form action="case.php" method="post"><input type="hidden" name="case_name" value="'.$closedcase[ 'case_name' ].'">
					 <input type="hidden" name="case_type" value="purchase">
					 <input type="hidden" name="order_id" value="'.$closedcase[ 'order_id' ].'"><input type="submit" class="open-case-button" value="Open Case" />
					</form><div>' . ucfirst($closedcase[ 'case_name' ]) .' '.'('.$closedcase['case_type'].')</div></div></div>';
				}
			}
			echo '<div id="purchaseCases">'.$data1.'</div>';
		}
	
		
		
	?>
	
	<br>
	
	<div class="mini-title group">Opened Cases
	
	<?php 
// CLAIM ALL
	//$claimables = $mysqli->query("SELECT * FROM rsj_payments WHERE userid ='{$_SESSION['id']}' && case_status = 'opened' && delivery ='tbd'");
	
	$row = array();
	$stmt = $mysqli->prepare("SELECT id,txnid,item FROM rsj_payments WHERE userid =? && case_status = 'opened' && delivery ='tbd' && payment_status = 'Completed'");
	$stmt->bind_param("s", $_SESSION['id']);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($row['id'], $row['txnid'], $row['item']);
	
		
	if ($stmt->num_rows > 1) {
			$items = array();
			$txnid = array();
			$orderid = array();
		while ($stmt->fetch()) {
    		$items[] = $row['item'];
			$txnid[] = $row['txnid'];
			$orderid[] = $row['id'];
		}
			echo '<a class="claim-item" style="float:right;margin:0px 3px;" href="javascript:void($zopim.livechat.say(\'I would like to claim my items: \n \n';
		for ($i=0; $i<count($items); $i++) {
			if (filter_var($txnid[$i], FILTER_VALIDATE_URL)) {
        		echo 'Item: '.$items[$i].' \nOrder ID: '.$orderid[$i].'\n \n';
			}
			else {
				echo 'Item: '.$items[$i].' \nTransaction ID: '.$txnid[$i].'\nOrder ID: '.$orderid[$i].'\n \n';
			}
		}
		echo '\')) ">Claim All</a>';
	} 
	$stmt->close();
	// CLAIM ALL ENDS ?>
	</div>
			
			<?php 
//$openedcases = $mysqli->query("SELECT * FROM rsj_payments WHERE userid ='{$_SESSION['id']}' && case_status = 'opened' ORDER BY rsj_payments.id DESC");


$payment = $mysqli->query("SELECT * FROM rsj_order_payment WHERE user_id = {$_SESSION['id']} && status = 'opened' && payment_status = 'Completed' ORDER BY rsj_order_payment.id DESC");
$paymentData = mysqli_fetch_all($payment,MYSQLI_ASSOC);

$openedcase = array();
$stmt = $mysqli->prepare("SELECT id,txnid,productname,item,delivery FROM rsj_payments WHERE userid =? && case_status = 'opened' && payment_status = 'Completed' ORDER BY rsj_payments.id DESC");
$stmt->bind_param("s", $_SESSION['id']);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($openedcase['id'], $openedcase['txnid'], $openedcase['productname'], $openedcase['item'], $openedcase['delivery']);


$query = $mysqli->query("SELECT rsj_order_detail.case_name,rsj_order_detail.order_id,rsj_order_item.order_item as case_items,rsj_order_payment.txn_id,rsj_order_payment.delivery FROM rsj_order_detail INNER JOIN rsj_order_payment ON rsj_order_detail.order_id = rsj_order_payment.order_id INNER JOIN rsj_order_item ON rsj_order_detail.order_id = rsj_order_item.order_id WHERE rsj_order_payment.user_id = {$_SESSION['id']} AND rsj_order_payment.payment_status='Completed'  ORDER BY rsj_order_item.id DESC");
$items = $query->fetch_all(MYSQLI_ASSOC);



$orderItems = array();
 foreach($items as $val)
 {
	 $items = explode(",",$val['case_items']);
	 $orderId = $val['order_id'];
	 $orderItems[] = array(
		'items'=>$items,
		'orderId'=>$orderId,
		'txnId'=>$val['txn_id'],
		'caseName'=>$val['case_name'],
		'delivery'=>$val['delivery'],
	 );
	
 }


 foreach($orderItems as $val)
 {
	
	foreach($val['items'] as $val1)
	{
		$singleArray[] = array(
			'items' => $val1,
			'order_id' => $val['orderId'],
			'txnId'=>$val['txnId'],
			'caseName'=>$val['caseName'],
			'delivery'=>$val['delivery'],
		); 
	}
 }
 //echo "<pre>";print_r($singleArray);
 //die;


if ($stmt->num_rows === 0 && count($paymentData) == 0) {
	echo '<div class="no-opened-cases"> No case opened yet :( </div>';
}
else {
	echo '<table class="opened-cases" border="0">
		<tr>
		<th>Case Name</th>
		<th>Item Won</th>
		<th>Transaction ID</th>
		<th>Order ID</th>
		<th>Item Delivery</th>
		</tr>';
	if(isset($singleArray) && !empty($singleArray))
	{
		foreach($singleArray as $val)
		{
			echo '
			<tr>
			<td>'.$val['caseName'].'</td>			
			<td>'.$val['items'].'</td>			
			<td>'.$val['txnId'].'</td>			
			<td>'.$val['order_id'].'</td>	
			<td>';
			if ($val['delivery'] == 'tbd') {
			
        		echo '<a href=" javascript:void($zopim.livechat.say(\'I would like to claim my item: '.$val['items'].' for '.$val['caseName'].'.\n\nOrder ID: '.$val['order_id'].'\')) " class="claim-item">Claim</a>';
			}
		
			else 
			{
				echo 'Delivered';
			}
			'</td>';
			
		}
		
	}
	while ($stmt->fetch()) {
		echo '
		<tr>
		<td>'.$openedcase['productname'].'</td>
		<td>'.$openedcase['item'].'</td>';
		if (filter_var($openedcase['txnid'], FILTER_VALIDATE_URL)) {
				echo '<td>Paid via GP</td>';
			}
			else {
				echo '<td>'.$openedcase['txnid'].'</td>';
			}
		
		
		echo '
		<td>'.$openedcase['id'].'</td>
		<td>';
		if ($openedcase['delivery'] == 'tbd') {
			if (filter_var($openedcase['txnid'], FILTER_VALIDATE_URL)) {
        		echo '<a href=" javascript:void($zopim.livechat.say(\'I would like to claim my item: '.$openedcase['item'].' for '.$openedcase['productname'].'.\n\nOrder ID: '.$openedcase['id'].'\')) " class="claim-item">Claim</a>';
			}
			else {
				echo '<a href=" javascript:void($zopim.livechat.say(\'I would like to claim my item: '.$openedcase['item'].' for '.$openedcase['productname'].'.\n\nTransaction ID: '.$openedcase['txnid'].' \nOrder ID: '.$openedcase['id'].'\')) " class="claim-item">Claim</a>';
			}
			
		}
		else {
			echo 'Delivered';
		}
		echo '</td>
		</tr>';
	}
	echo '</table>';
}

?>
		</div>
	</div>
            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
<script>
	$(document).on('click','.getSubCat',function(){
		var catId = $(this).attr('data-attr');
		$('#freeCases').html('');
		jQuery.ajax({
            type: "POST",
            url: 'get-sub-cat-name.php',
            data:{catId:catId},
            dataType: "html",
            success: function (data)
            {
                $('.subCat').html(data);
            }
        });
	});
	$(document).on('click','.getFreeCase',function(){
		var catId = $(this).attr('data-cat');
		var subCatId = $(this).attr('data-subcat');
		jQuery.ajax({
            type: "POST",
            url: 'get-free-cases.php',
            data:{catId:catId,subCatId:subCatId},
            dataType: "html",
            success: function (data)
            {
                $('#freeCases').html(data);;
            }
        });
	});
</script>