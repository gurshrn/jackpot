	<!doctype html>
		<html>
			<head>
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121616401-1"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());

			  gtag('config', 'UA-121616401-1');
			</script>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>RSJackpot - Opening Case...</title>
		<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
		<style>
			body {
				font-family: Lato;
				background: #333;
				color: #999;
				text-align: center;
			}
			* {
				-webkit-box-sizing: border-box;
				box-sizing: border-box;
			}
			.button {
				font-family: "Oswald";
				display: inline-block;
				padding: 10px 15px;
				background: #2E9AFE;
				border: 0;
				outline: 0;
				color: #FFF;
				text-transform: uppercase;
				text-decoration: none;
				font-size: 22px;
				cursor: pointer;
				transition: all ease 0.3s;
			}
			.button:hover {
				background: #222;
			}
			h1 {
				font-family: Lato;
				font-size: 32px;
				color: #777;
				font-weight: 300;
				text-transform: uppercase;
				padding: 10px;
				border-bottom: solid 2px #FFF;
				width: 200px;
				text-align: center;
				margin: 0 auto 30px auto;
			}
			#arrow {
				text-shadow: #000 0 1px 3px;
				font-size: 48px;
				display: inline-block;
				position: absolute;
				top: 0;
				right: 0;
				left: 0;
				margin: 0 auto;
				background: rgba(51, 103, 153, 0.4);
				width: 450px;
				border-radius: 130px;
				height: 203px;
			}
				.super-wrap {
					width: 450px;
					margin: 0 auto;
					position:  relative;
				}
			.open-case-wrap {
				overflow: hidden;
				display: inline-block;
				vertical-align: middle;
				background: #252525;
				width: 450px;
				border-radius: 130px;
				box-shadow: inset rgba(0, 0, 0, 0.3) 2px 2px 25px, #5d5d5d 0 0 3px;
				white-space: nowrap;
				padding: 50px 0;
			}
			.product-item {
				text-align: center;
				background: #222;
				color: #FFF;
				font-size: 12px;
				margin: 0 5px 0 5px;
				border-radius: 50px;
				overflow: hidden;
				box-shadow: rgba(0,0,0,0.5) 0 1px 5px;
				text-shadow: rgba(0,0,0,0.7) 0 1px 1px;
				display: inline-block;
				width: 100px;
				height: 100px;
				white-space: initial;
			}
			.open-case-wrap .product-item:first-child {
				margin-left: 450px;
			}
			.product-item .img {
				background: rgba(0,0,0,0.3);
				height: 48px;
				padding: 10px;
				margin-bottom: 15px;
				text-align: center;
			}
			.blue-c {
				border: solid 2px rgba(0, 76, 255, 0.5);
			}
			.purple-c {
				border: solid 2px rgba(105, 0, 255, 0.5);
			}
			.pink-c {
				border: solid 2px rgba(255, 0, 190, 0.5);
			}
			.red-c {
				border: solid 2px rgba(255, 0, 4, 0.5);
			}
			.green-c {
				border: solid 2px rgba(9, 231, 0, 0.5);
			}
			.gold-c {
				border: solid 2px rgba(255, 215, 0, 0.5);
			}
			.rotate {
			  -webkit-transform: rotate(3240deg);  /* Chrome, Safari 3.1+ */
				 -moz-transform: rotate(3240deg);  /* Firefox 3.5-15 */
				  -ms-transform: rotate(3240deg);  /* IE 9 */
				   -o-transform: rotate(3240deg);  /* Opera 10.50-12.00 */
					  transform: rotate(3240deg);  /* Firefox 16+, IE 10+, Opera 12.50+ */
			}
			.rotate-transition {
			  -webkit-transition: all 2s ease-out;  /* Chrome 1-25, Safari 3.2+ */
				 -moz-transition: all 2s ease-out;  /* Firefox 4-15 */
				   -o-transition: all 2s ease-out;  /* Opera 10.50–12.00 */
					  transition: all 2s ease-out;  /* Chrome 26, Firefox 16+, IE 10+, Opera 12.50+ */
			}
		</style>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
	</head>
	<body>
		<h1>Opening Case...</h1>
		<div class="super-wrap">
		<div class="open-case-wrap">