<?php
session_name('rsjLogin');
session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - News & Updates, find out what is changing on RSJackpot!</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.1" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>
		<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		
		 echo '</ul> 

		</div>
		</div>'; ?>
<div class="content-wrap">
	   <div class="page-title"><b>RSJACKPOT NEWS & UPDATES</b></div>
       <div class="main-content">
<center><h3>RSJackpot is always making changes to improve the expereience for you! Check here frequently to see everything that is going on behind the scenes.</h3></centeR>

<h3 class="font-color-blue">New Updates on 7/13/2017</h3>
         
         <h4>G2A Pay Integration: G2A pay is a safe and secure way to process transactions with over 200 local and worldwide payment methods being offered. Check out all of the payment methods they offer <a href="https://pay.g2a.com/online-payment-methods/north-america">here</a> :)</h4>
         	
<h3 class="font-color-blue">New Updates on 7/28/2017</h3>
         <ul>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=8">Red Case (RS3) price raise from $3.99 to $4.99 for the incredible rise in price of rares.</a></li>

         </ul>         
<h3 class="font-color-blue">New Updates on 7/9/2017</h3>
         <ul>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=8">Zamorak Case (07) price raise from $2.99 to $3.49 for balancing.</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=5">Gold Case (07) price raise from $2.99 to $3.49 for balancing.</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=6">Cheapskate Case (07) price drop from $1.99 to $1.79 for balancing.</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=7">Zulrah Case (07) price raise from $1.99 to $2.49 for balancing.</a></li>

         </ul>
<h3 class="font-color-blue">New Updates on 5/17/2017</h3>
         <ul>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=19">Red Case (RS3) price drop from 4.49 to 3.99.</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=17">Cheapskate Case (RS3) price drop from 1.59 to 1.49.</a></li>

         </ul>
<h3 class="font-color-blue">New Updates on 5/16/2017</h3>
         <ul>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=19">Brand new Sliske Case added for RS3!</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=20">Brand new Zaros Case added for RS3!</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=21">Brand new Seren Case added for RS3!</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=11">Guaranteed Millionaire (07) is now only $7.99! (was $13.99) due to a reduction in the red prize to 50M instead of 100M.</a></li>
         </ul>
<h3 class="font-color-blue">New Updates on 5/7/2017</h3>
         <h4>New Feature - Buy with RSGP is now an option! Click the button and a Livechat representitive will tell you how much the case is in RSGP :)</h4>
         <ul>	
         	<li><a href="https://rsjackpot.org/index.php?show=rs3">All RS3 Blue Prizes increased to 2M-2.8M (Except Cheapskate)</a></li>
         	<li>Odds of landing on a pink increased</li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=18">Brand New Dragon Case added for 07!</a></li>
         	<li>Changes to packs, all old packs removed!</li>
         	<li><a href="https://rsjackpot.org/product.php?p=pack&id=11">First new pack added is the Boss Pack (07)</a></li>

         </ul>         
<h3 class="font-color-blue">New Updates on 3/29/2017</h3>
         <ul>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=8">Zamorak Case Price lowered 25% and Top Prize Increased to 300M</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=2">Bandos Case Price lowered 25% and Top Prize Increased to 300M</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=5">Gold Case Price lowered 25% and Top Prize Increased to 300M</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=7">Zulrah Case Price lowered 25% and Top Prize Increased to 300M</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=3">Sardomin Case Price lowered 15%</a></li>
         	<li><a href="https://rsjackpot.org/product.php?p=case&id=4">Armadyl Case Price lowered 30%</a></li>
         </ul>
    </div>
</div>
            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
