<?php
include 'framework/WebPage.php';

class Ryan extends WebPage {
	
	private $productType = 'all';

	function preProcessing() {
		if (isset($_GET['show'])) {
			$this->productType = $_GET['show'];
		}
		return true;
	}

	function output() {
		$this->outputHeader();
		
		echo '<body>
		<div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>
		<div id="nav"><a href="/">Home</a><a href="how-to-win.php">How to Win</a><a href="inventory.php">Inventory</a><a href="/forum/">Forum</a></div>
		<div class="social">
		<a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt=""/></a>
		<a href="https://twitter.com/rsjackpot"><img src="assets/images/twitter.png" width="48" height="48" alt=""/></a>
		
		</div>
		</div>
		</div>
		<div class="user-details-wrap">
		<div class="user-details">';
		if(isset($_SESSION['id'])) {
			echo 'Welcome '.$_SESSION['usr'].'! (<a href="account.php?logout">Logout</a>)';
		}
		else {
			echo '<a href="login.php">Login</a>&nbsp; | &nbsp;<a href="register.php">Register</a>';
		}
		  
		echo '</div>
		</div>
		<div class="content-wrap">';
		
		echo '<div class="product-wrap group">';
		echo '<div class="announcement"><center><h2>We now have 24/7 live chat!</center></div>
		<div class="type-nav">
		<a href="ryan.php">All</a>
		<a href="ryan.php?show=rs3">RS3</a>
		<a href="ryan.php?show=07">07</a>
		</div>';
		
		if ($this->productType == 'all') {
			$products = $this->getController()->getDatabase()->getProducts();
		} else {
			$products = $this->getController()->getDatabase()->getProductsByType($this->productType);
		}
		foreach ($products as $product) {
			$this->outputProduct($product);
		}
		echo '</div>';
		echo '</div>';
		$this->outputFooter();
		echo '</body>';
		echo '</html>';
	}
	
	function outputHeader() {
		echo '
		<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>RSJackpot - The RuneScape Gambling and Case Opening Site</title>
		<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato" rel="stylesheet" type="text/css">
		<link href="assets/css/style.css" rel="stylesheet" type="text/css">
		<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
		<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
		<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/favicon/manifest.json">
		<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
		<link rel="shortcut icon" href="/favicon/favicon.ico">
		<meta name="msapplication-TileColor" content="#2d89ef">
		<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
		<meta name="msapplication-config" content="/favicon/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">
		<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="assets/js/fuser.js"></script>
		<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
		<!--Start of Zopim Live Chat Script-->
		<script type="text/javascript">
		window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
		_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
		$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
		type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zopim Live Chat Script-->
		</head>';
	}
	
	function outputFooter() {
		echo '<div class="footer-wrap">
		<div class="footer-apparatus"></div>
		<div class="footer-content">
		<div class="links">
		<ul>
		<li><a href="terms.php">Terms & Conditions</a></li>
		<li><a href="privacy.php">Privacy Policy</a></li>
		</ul>
		<ul>
		<li><a href="login.php">Login</a></li>
		<li><a href="register.php">Register</a></li>
		</ul>
		</div>
		<div class="social">
		<a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt=""/></a>
		<a href="https://twitter.com/rsjackpot"><img src="assets/images/twitter.png" width="48" height="48" alt=""/></a>
		
		</div>
		<div class="copyright">&copy; RSJackpot 2015</div>
		</div>
		</div>';
	}
	
	function outputProduct($product) {
		echo '<div class="product">';
		
		
		echo '<div class="content group bandos">
		<div class="title">' . $product->getName() .'</div>
		<a href="product.php?id=' . $product->getId() . '" class="price"> $' . $product->getPrice() . '</a>
		<div class="desc"> You can win';
		
		echo '<div id="items-slider-' . $product->getId() . '" class="items-slider">';
		
		$itemsQueue = $product->getItemsQueue();
		for ($level = $itemsQueue->getMin(); $level <= $itemsQueue->getMax(); $level ++) {
			$levelQueue = $itemsQueue->getQueueAtLevel($level);
			foreach ($levelQueue as $item) {
				switch ($level) {
					case 1: //Green
						echo '<div class="item green-c">';
						break;
					case 2: //Red
						echo '<div class="item red-c">';
						break;
					case 3: //Pink
						echo '<div class="item pink-c">';
						break;
					case 4: //Purple
						echo '<div class="item purple-c">';
						break;
					case 5: //Blue
						echo '<div class="item blue-c">';
						break;
				}
				echo '<img src="assets/images/' . $product->getType() . '/' . $item->getName() . '.png" alt="' . $item->getName() . '"/>
				<span>' . $item->getName() . '</span>
				</div>';
		
			}
		}
		echo '</div>';
		
		echo '<script type="text/javascript">
					$("#items-slider-' . $product->getId() . '").owlCarousel({
						itemsCustom : [
        				[0, 2],
       				 	[450, 3],
        				[640, 4],
        				[800, 3],
        				[955, 3],
        				[1200, 3],
        				[1300, 4],
        				[1600, 5]
      					]
					});
			</script>';
		echo '</div>';
		echo '<span class="stamp-' . $product->getType() . '">' . $product->getType() . '</span>';
		
		echo '</div>';
		echo '</div>';
	}

}

$page = new Ryan();
$page->execute();
$page->getController()->destroy();
?>