<!doctype html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121616401-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121616401-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Payment Cancelled</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
</style>
</head>
<body>
<h1>Payment Cancelled</h1>
Payment Aborted by user.<br><br>

<a class="button" onClick="window.close();">
Close
</a>
</body>
</html>