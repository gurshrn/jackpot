<?php
error_reporting(0);
include 'framework/WebPage.php';

class Index extends WebPage
{
    
    private $productType = '07';
    private $product = 'cases';
    
    function preProcessing()
    {
        if (isset($_GET['show'])) 
		{
            $this->productType = $_GET['show'];
        }
        if (isset($_GET['product'])) 
		{
            $this->product = $_GET['product'];
        }
        return true;
    }
    
    function output()
    {
        $this->outputHeader();
        echo '<body>
        <div class="top-wrap">
        <div class="top group">
        <div id="nav-icon"></div>
        <a id="logo" title="RSJackpot" href="/"></a>';
        if ((isset($_SESSION['vip'])) && ($_SESSION['vip'] != 0)) 
		{
            echo '<div class="vip-label"></div>';
        }
        echo '
        <ul id="nav"><li><a href="http://customer-devreview.com/jackpot/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
        if (isset($_SESSION['id'])) 
		{
            echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
            <ul>
            <li><span>Welcome, ' . $_SESSION['usr'] . '!</span></li>
            <li><a href="inventory.php">Inventory</a></li>
            <li><a href="feedback.php">Feedback</a></li>
            <li><a href="upload.php">Upload</a></li>
            <li><a href="password.php">Change Password</a></li>
            <li><a href="account.php?logout">Logout</a></li>
            </ul></li>';
        } 
		else 
		{
            echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
        }
		
        echo '<li class="cart"><a href="cart.php">
        <svg  id="cart" x="0px" y="0px"
            viewBox="0 0 486.569 486.569" style="enable-background:new 0 0 486.569 486.569;" xml:space="preserve">
            <g>
                <path d="M146.069,320.369h268.1c30.4,0,55.2-24.8,55.2-55.2v-112.8c0-0.1,0-0.3,0-0.4c0-0.3,0-0.5,0-0.8c0-0.2,0-0.4-0.1-0.6
                    c0-0.2-0.1-0.5-0.1-0.7s-0.1-0.4-0.1-0.6c-0.1-0.2-0.1-0.4-0.2-0.7c-0.1-0.2-0.1-0.4-0.2-0.6c-0.1-0.2-0.1-0.4-0.2-0.6
                    c-0.1-0.2-0.2-0.4-0.3-0.7c-0.1-0.2-0.2-0.4-0.3-0.5c-0.1-0.2-0.2-0.4-0.3-0.6c-0.1-0.2-0.2-0.3-0.3-0.5c-0.1-0.2-0.3-0.4-0.4-0.6
                    c-0.1-0.2-0.2-0.3-0.4-0.5c-0.1-0.2-0.3-0.3-0.4-0.5s-0.3-0.3-0.4-0.5s-0.3-0.3-0.4-0.4c-0.2-0.2-0.3-0.3-0.5-0.5
                    c-0.2-0.1-0.3-0.3-0.5-0.4c-0.2-0.1-0.4-0.3-0.6-0.4c-0.2-0.1-0.3-0.2-0.5-0.3s-0.4-0.2-0.6-0.4c-0.2-0.1-0.4-0.2-0.6-0.3
                    s-0.4-0.2-0.6-0.3s-0.4-0.2-0.6-0.3s-0.4-0.1-0.6-0.2c-0.2-0.1-0.5-0.2-0.7-0.2s-0.4-0.1-0.5-0.1c-0.3-0.1-0.5-0.1-0.8-0.1
                    c-0.1,0-0.2-0.1-0.4-0.1l-339.8-46.9v-47.4c0-0.5,0-1-0.1-1.4c0-0.1,0-0.2-0.1-0.4c0-0.3-0.1-0.6-0.1-0.9c-0.1-0.3-0.1-0.5-0.2-0.8
                    c0-0.2-0.1-0.3-0.1-0.5c-0.1-0.3-0.2-0.6-0.3-0.9c0-0.1-0.1-0.3-0.1-0.4c-0.1-0.3-0.2-0.5-0.4-0.8c-0.1-0.1-0.1-0.3-0.2-0.4
                    c-0.1-0.2-0.2-0.4-0.4-0.6c-0.1-0.2-0.2-0.3-0.3-0.5s-0.2-0.3-0.3-0.5s-0.3-0.4-0.4-0.6c-0.1-0.1-0.2-0.2-0.3-0.3
                    c-0.2-0.2-0.4-0.4-0.6-0.6c-0.1-0.1-0.2-0.2-0.3-0.3c-0.2-0.2-0.4-0.4-0.7-0.6c-0.1-0.1-0.3-0.2-0.4-0.3c-0.2-0.2-0.4-0.3-0.6-0.5
                    c-0.3-0.2-0.6-0.4-0.8-0.5c-0.1-0.1-0.2-0.1-0.3-0.2c-0.4-0.2-0.9-0.4-1.3-0.6l-73.7-31c-6.9-2.9-14.8,0.3-17.7,7.2
                    s0.3,14.8,7.2,17.7l65.4,27.6v61.2v9.7v74.4v66.5v84c0,28,21,51.2,48.1,54.7c-4.9,8.2-7.8,17.8-7.8,28c0,30.1,24.5,54.5,54.5,54.5
                    s54.5-24.5,54.5-54.5c0-10-2.7-19.5-7.5-27.5h121.4c-4.8,8.1-7.5,17.5-7.5,27.5c0,30.1,24.5,54.5,54.5,54.5s54.5-24.5,54.5-54.5
                    s-24.5-54.5-54.5-54.5h-255c-15.6,0-28.2-12.7-28.2-28.2v-36.6C126.069,317.569,135.769,320.369,146.069,320.369z M213.269,431.969
                    c0,15.2-12.4,27.5-27.5,27.5s-27.5-12.4-27.5-27.5s12.4-27.5,27.5-27.5S213.269,416.769,213.269,431.969z M428.669,431.969
                    c0,15.2-12.4,27.5-27.5,27.5s-27.5-12.4-27.5-27.5s12.4-27.5,27.5-27.5S428.669,416.769,428.669,431.969z M414.169,293.369h-268.1
                    c-15.6,0-28.2-12.7-28.2-28.2v-66.5v-74.4v-5l324.5,44.7v101.1C442.369,280.769,429.669,293.369,414.169,293.369z"/>
            </g>
        </svg>
        </a></li>';
        
        echo '</ul>

        </div>
        </div>
        
        <div class="content-wrap">';
        
        echo '<div class="product-wrap group">';
        $announcement = $this->getController()->getDatabase()->getAnnouncement();
        if (!empty($announcement)) 
		{
            echo '<div class="announcement">' . $announcement . '</div>';
        }
        
        echo '<div class="livefeed-super-wrap" id="livefeed-super-wrap" style="display:none;">
        <div class="stats">
            <span id="stats"></span><br>cases opened
        </div><div class="livefeed-wrap">
            <div class="title">
                Item Live Feed <a class="itemtype" id="lfsa">All</a><a class="itemtype selected" id="lfst">Top Items</a>
            </div>
            <div class="livefeed" id="livefeed" style="display:none;">
                
            </div>
            <div class="livefeed" id="livefeed-top">
                
            </div>
        </div>
        </div>';
        echo '<script>
        
        </script>';
        
        $flashdata = $this->getController()->getDatabase()->getFlashsale();
		if (($flashdata['visibility'] == 'enable') && (!empty($_SESSION['time']))) 
		{
            $flashproduct  = $this->getController()->getDatabase()->getFlashproduct();
            $discount      = $flashproduct["price"] - ($flashproduct["price"] * $flashdata["discount"] / 100);
            $discountprice = number_format($discount, 2, '.', '');
            $usertz        = new DateTimeZone($_SESSION['time']);
            $gmt           = new DateTimeZone('GMT');
            
            $startdt = new DateTime($flashdata['startdt'], $gmt);
            $startdt->setTimezone($usertz);
            $startdate = $startdt->format('Y/m/d H:i:s');
            
            $enddt = new DateTime($flashdata['enddt'], $gmt);
            $enddt->setTimezone($usertz);
            $enddate = $enddt->format('Y/m/d H:i:s');
            
            $currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
            $currentdt->setTimezone($usertz);
            
            
            echo '<div class="flash-sale-wrap" id="flash-sale-wrap" style="display:none;">
        <div class="title">Flash Sale!</div>
        <div class="content group" style="border-radius: 10px;">
            <div class="discount">
                ' . $flashdata['discount'] . '%
                <span>O F F</span>
            </div>
            <div class="case ' . $flashproduct['desc'] . '">
                ' . $flashproduct['name'] . '
                <div class="price-wrap-old"><span>$' . $flashproduct["price"] . '</span><div class="strike"></div></div>
                <div class="price-wrap"><span>$' . number_format($discountprice - $discountprice * $_SESSION['vip'], 2) . '</span></div>
            </div>';
            if ($currentdt > $enddt) { // sale ended
                echo '<div class="countdown-wrap"><div class="countdown">Sale Ended!</div></div>';
            } else if ($startdt > $currentdt) { // sale is starting
                echo '<div class="countdown-wrap">
            <span style="font-style:italic; color:#999;text-transform:uppercase;">Sale starts in:</span><br>
            <div class="countdown" id="countdown">
            </div>
            </div>
            <script type="text/javascript">
            $(function() {
                $("#countdown").countdown("' . $startdate . '", function(event) {
                    var totalHours = event.offset.totalDays * 24 + event.offset.hours;
                    $(this).html(event.strftime("<div>" + totalHours + " <span>hour</span></div> <div>: %M :<span>min</span></div> <div>%S <span>sec</span></div>"));
                }).on("finish.countdown", function(event) {
                    location.reload();
                });                
            });
            </script>';
            } else if (($startdt < $currentdt) && ($currentdt < $enddt)) { // sale is ON!
                echo '<div class="countdown-wrap">
            <span style="font-style:italic; color:#999;text-transform:uppercase;">Hurry up! Sale ends in:</span><br>
            <div class="countdown" id="countdown">
            </div>
            </div>
            <script type="text/javascript">
            $(function() {
                $("#countdown").countdown("' . $enddate . '", function(event) {
                    var totalHours = event.offset.totalDays * 24 + event.offset.hours;
                    $(this).html(event.strftime("<div>" + totalHours + " <span>hour</span></div> <div>: %M :<span>min</span></div> <div>%S <span>sec</span></div>"));
                }).on("finish.countdown", function(event) {
                    $(this).html("Sale Ended!")
                });
            });
            </script>';
                echo '<a href="product.php?p=' . $flashdata['p'] . '&id=' . $flashdata['productid'] . '" class="button">Buy now</a>';
            }
            echo '</div></div>'; // sale wrap and content
        }

       
        echo '<div class="tabs" id="tabs">';
		
		$category = $this->getController()->getDatabase()->getCategory();
		
		if(isset($category) && !empty($category))
		{
			$count = 1;
			foreach($category as $cat)
			{
				echo '<a href="javascript:void(0)" data-attr="'.$cat['id'].'"';
				if ($count == 1) 
				{
					echo 'class="active categories"';
				}
				else
				{
					echo 'class="categories"';
				}
				echo '>'.$cat['category_name'].'</a>';
				
				if ($count == 1) 
				{
					$subCat[] = $this->getController()->getDatabase()->getSubCategory($cat['id']);
				}
				$count++;
			}
		}
		
		
		echo '</div>
        <div class="tabilicious group catItems"> <!-- Nigga this is a marker -->
        ';
		
		// ********** Products Start here **********
		
		echo '<div class="type-nav">';
		

			if(isset($subCat) && !empty($subCat))
			{
				foreach($subCat as $subCategory)
				{
					foreach($subCategory as $val6)
					{
						echo '<a href="javascript:void(0)" class="subCategories" data-target="'.$val6['id'].'" data-attr="'.$val6['cat_id'].'">'.$val6['sub_category_name'].'</a>';
					}
				}
			}
	
      
		echo '</div>';
              
		$counts = 1;
			
		foreach($category as $key=>$cats)
		{
			if($counts == 1)
			{
				$products[] = $this->getController()->getDatabase()->getProducts($cats['id']);
			}
			$counts++;
		}
		echo '<div class="allResult">';
		foreach ($products as $product) 
		{
			foreach($product as $value)
			{
				$this->outputProduct($value);
			}
			
		}
		echo '</div>';
         
		// ********** Cases Ends here **********
       
	   
        echo '</div><!-- Tabilicious ends here -->';
	
        
        echo '<div class="the-last-ride">';
       
        $feedbacks = $this->getController()->getDatabase()->getFeedbacks();
        if ($feedbacks->num_rows !== 0) {
            echo '<div class="feedbacks-wrap">
        <div class="feedbacks">
            <div class="title">Feedbacks from our customers</div>
            <div id="feedbacks-slider" class="bigwins-slider">';
            while ($feedback = $feedbacks->fetch_assoc()) {
                echo '
            <div class="item">
            <div class="feedback-text">"' . $feedback['feedback'] . '"</div>
            <span>- ' . $feedback['username'] . '</span>
            </div>';
            }
            echo '
            </div>
            <script type="text/javascript">
                    $("#feedbacks-slider").owlCarousel({
                           items: 2,
                        autoPlayTimeout: 4000,
                        autoPlay: true,
                        stopOnHover : true
                    });
            </script>
            </div>
        </div>';
        }
        // feedbacks wrap ends here
        
        echo '</div>'; // last ride
        echo '</div>';
        echo '</div>';
        $this->outputFooter();
        echo '</body>';
        echo '</html>';
    }
    
    
    function outputHeader()
    {
        echo '<!doctype html>
        <html>
        <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121616401-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag("js", new Date());

  gtag("config", "UA-121616401-1");
</script>

        <meta charset="utf-8">
        <meta name="description" content="RSJackpot is the best Case Opening site for RuneScape. Get your favorite RuneScape items today! Bandos, Godswords, and Mills!">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>RSJackpot - The best RuneScape Case Opening Site! RS3 and 07!</title>

        <link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
        <link href="assets/css/style.css?v=1.44" rel="stylesheet" type="text/css">
        <link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/favicon/manifest.json">
        <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/favicon/favicon.ico">
        <meta name="msapplication-TileColor" content="#2d89ef">
        <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
        <meta name="msapplication-config" content="/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="assets/js/fuser.js"></script>
        <script type="text/javascript" src="assets/carousel/owl.carousel.js"></script>
        <!--<script type="text/javascript" src="assets/js/jquery.classywiggle.js"></script>-->
        <script type="text/javascript" src="assets/js/jquery.countdown.js"></script>
        <script type="text/javascript" src="assets/js/jstz.js"></script>
        <script type="text/javascript">
                
                    $(function() {
                        function loadlivefeed() {
							
                            $.getJSON(\'livefeed.php\', function (data) {
								
                            var itemhtmlstring = \'\';
                            var itemhtmlstringtop = \'\';
                                data[0].forEach(function(item) {
                                        itemhtmlstring += \'<div class="item"><div class="image"><img height="30px"  width="25px" src="admin/upload/\'+\'/\'+ item.order_item_image +\'"/></div><div class="details"><div class="itemname">\'+ item.order_item +\'</div><div class="userdetails">won by<br> <span class="\' + item.vip +\'">\'+ item.usr +\'</span></div></div></div>\';
                                    
                                });
                                
                                data[1].forEach(function(item) {
                                        itemhtmlstringtop += \'<div class="item"><div class="image"><img height="30px"  width="25px" src="admin/upload/\'+\'/\'+ item.item_image +\'"/></div><div class="details"><div class="itemname">\'+ item.item_name +\'</div><div class="casename">\'+ item.case_name +\'</div></div></div>\';
                                    
                                });
                                $(\'#stats\').html(data[2]);
                                $(\'#livefeed\').html(itemhtmlstring);
                                $(\'#livefeed-top\').html(itemhtmlstringtop);
                                
                            });
                            return true;
                        }
                        
                        if (loadlivefeed()) {
                            $(\'#livefeed-super-wrap\')
                              .css(\'opacity\', 0)
                              .slideDown(\'slow\')
                              .animate(
                                { opacity: 1 },
                                { queue: false, duration: 1800 }
                              );
                        }
                        
                        
                        
                          setInterval(loadlivefeed, 8000);
                        
                        $(\'#flash-sale-wrap\')
                              .css(\'opacity\', 0)
                            .delay(600)
                              .slideDown(\'slow\')
                              .animate(
                                { opacity: 1 },
                                { queue: false, duration: 1800 }
                          );
                        
                        $(\'#lfsa\').on(\'click\', function() {
                            $(\'#livefeed-top:visible\').fadeOut(\'fast\', function() {
                                $(\'#livefeed:hidden\').fadeIn(\'fast\');
                            });
                            $(this).addClass(\'selected\');
                            $(\'#lfst\').removeClass(\'selected\');
                        });
                        $(\'#lfst\').on(\'click\', function() {
                            $(\'#livefeed:visible\').fadeOut(\'fast\', function() {
                                $(\'#livefeed-top:hidden\').fadeIn(\'fast\');
                            });
                            
                            $(this).addClass(\'selected\');
                            $(\'#lfsa\').removeClass(\'selected\');
                        });
                        
                    });
                
        </script>
        
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version="1.1",s.queue=[],u=t.createElement(n),u.async=!0,u.src="//static.ads-twitter.com/uwt.js",
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,"script");
// Insert Twitter Pixel ID and Standard Event data below
twq("init","ny6gp");
twq("track","PageView");
</script>
<!-- End Twitter universal website tag code -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version="2.0";
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,"script",
  "https://connect.facebook.net/en_US/fbevents.js");
  fbq("init", "1226776230802365");
  fbq("track", "PageView");
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1226776230802365&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

        </head>';
    }
    
    function outputFooter()
    {
        
        echo '                       <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                   <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div><script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>
                         <ul>
                            <li>  <iframe src="https://discordapp.com/widget?id=481200804686987275&theme=dark" width="300" height="300" allowtransparency="true" frameborder="0"></iframe>
                            </li>
                            </ul>

                    </div>
                    <div class="social">
                      <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>';
    }
    
    
    function outputCoin($coin)
    {
        
        echo '<div class="product">';
        
        echo '<div class="content group ' . $coin->getDesc() . '">
        <div class="title">' . $coin->getName() . '</div>
        <a href="product.php?p=coin&id=' . $coin->getId() . '" class="price">' . $coin->getPrice() . 'M</a>
        <div class="desc coin-fix"> You could land on';
        
        echo '<div id="items-slider-' . $coin->getId() . '" class="items-slider">';
        
        $items = $coin->getItemsArray();
        foreach ($items as $item) {
            echo '<div class="item gold-c">';
            echo '<img src="assets/images/' . $coin->getType() . '/' . $item . '.png" alt="' . $item . '"/>
                <span>' . $item . '</span>
                </div>';
            
        }
        echo '</div>';
        
        echo '<script type="text/javascript">
                    $("#items-slider-' . $coin->getId() . '").owlCarousel({
                        itemsCustom : [
                        [0, 2],
                            [450, 3],
                        [640, 4],
                        [800, 3],
                        [955, 3],
                        [1200, 3],
                        [1300, 4],
                        [1600, 5]
                          ]
                    });
            </script>';
        echo '</div>';
        echo '<span class="stamp-' . $coin->getType() . '">' . $coin->getType() . '</span>';
        
        echo '</div>';
        echo '</div>';
        
        
    } // Coins end
    
    
    function outputPack($pack)
    {
        
        echo '<div class="product">';
        
        echo '<div class="content group ' . $pack->getDesc() . '">
        <div class="title">' . $pack->getName() . '</div>
        <a href="product.php?p=pack&id=' . $pack->getId() . '" class="price"> $' . $pack->getPrice() . '</a>
        <div class="desc"> Pack could contain:';
        
        echo '<div id="items-slider-' . $pack->getId() . '" class="items-slider">';
        
        $items = $pack->getItemsArray();
        foreach ($items as $item) {
            echo '<div class="item gold-c">';
            echo '<img src="assets/images/' . $pack->getType() . '/' . $item . '.png" alt="' . $item . '"/>
                <span>' . $item . '</span>
                </div>';
            
        }
        echo '</div>';
        
        echo '<script type="text/javascript">
                    $("#items-slider-' . $pack->getId() . '").owlCarousel({
                        itemsCustom : [
                        [0, 2],
                            [450, 3],
                        [640, 4],
                        [800, 3],
                        [955, 3],
                        [1200, 3],
                        [1300, 4],
                        [1600, 5]
                          ]
                    });
            </script>';
        echo '</div>';
        echo '<span class="stamp-' . $pack->getType() . '">' . $pack->getType() . '</span>';
        
        echo '</div>';
        echo '</div>';
        
        
    } // packs end
    
    function outputProduct($product)
    {
	
        
        echo '<div class="product">';
        
        echo '<div class="content group '.' '.$product->getPriority().'">
        <div class="title">' . ucfirst($product->getName()) .' '.'('.$product->getItems().')</div>
        <a href="javascript:void(0)" data-attr="' . $product->getName() . '" data-qty="1" data-type="case" data-target="' . $product->getId() . '" class="addToCart">Add to cart</a>
        <a href="product.php?p=case&id=' . $product->getId() . '&subcat='.$product->getItems().'" class="price"> $' . number_format($product->getDesc(), 2) . '</a>
        <div class="desc"> Case could contain:';
        
        echo '<div id="items-slider-' . $product->getId() . '" class="items-slider">';
        
        $itemsQueue = $product->getCases();
		
        foreach ($itemsQueue as $item) {
            echo '<div class="item ' . $item['color'] . '-c"><img height="30" width="40" src="admin/upload/' . $item['item_image'] . '" alt="' . $item['item_image'] . '"/><span>' . $item['item_unique_name'] . '</span></div>';
            
        }
        
        echo '</div>';
        
        echo '<script type="text/javascript">
                    $("#items-slider-' . $product->getId() . '").owlCarousel({
                        itemsCustom : [
                        [0, 2],
                            [450, 3],
                        [640, 4],
                        [800, 3],
                        [955, 3],
                        [1200, 3],
                        [1300, 4],
                        [1600, 5]
                          ]
                    });
            </script>';
        echo '</div>';
        echo '<span class="stamp-' . $product->getItems() . '">' . $product->getItems() . '</span>';
        
        echo '</div>';
        echo '</div>';
        
    }
    
}

$page = new Index();
$page->execute();
$page->getController()->destroy();
?>
<script>
    jQuery(document).on('click','.addToCart',function(){
        var caseName = $(this).attr('data-attr');
        var caseId = $(this).attr('data-target');
        var type = $(this).attr('data-type');
        var qty = $(this).attr('data-qty');
        jQuery.ajax({
            type: "POST",
            url: 'cart-database.php',
            data:{qty:qty,type:type,caseName:caseName,caseId:caseId},
            dataType: "json",
            success: function (data)
            {
                window.location = data.url;
            }
        });
    });
	jQuery(document).on('click','.subCategories',function(){
		var caseType = $(this).attr('data-target');
		var catId = $(this).attr('data-attr');
		jQuery.ajax({
            type: "POST",
            url: 'get-items.php',
            data:{caseType:caseType,catId:catId},
            dataType: "html",
            success: function (data)
            {
                $('.allResult').html(data);
            }
        });
	});
	jQuery(document).on('click','.categories',function(){
		var catId = $(this).attr('data-attr');
		$('.tabs a.active').removeClass('active');
		$(this).addClass('active');
		jQuery.ajax({
            type: "POST",
            url: 'get-cat-items.php',
            data:{catId:catId},
            dataType: "html",
            success: function (data)
            {
                $('.catItems').html(data);
            }
        });
	});
</script>