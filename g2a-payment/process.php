<?
	include_once('../session.php');
	secure_session_start();
	define('INCLUDE_CHECK',true);
	include_once("../config.php");
	require('../vendor/autoload.php');

	use G2APay\G2APay;
	
	if(isset($_SESSION['id'])) {
		if (isset($_POST['g2a-submit'])) 
		{


			// Set required variables
			//$hash = '06095303-81af-4cdb-82a7-b5d93169a9b0'; // Get it from G2APay
			//$secret = 'clILudixYceMtCP*^mskE2^Ha!8B=ULS4dnx4JicwAydMBcJyI+*K-GutO!jtE&w'; // Get it from G2APay
			
			$hash = 'd176ae98-f332-44e7-92c0-241b3b371b39';
			$secret = 'http://customer-devreview.com/jackpot/';
			
			$success = 'http://customer-devreview.com/jackpot/successg2a.php'; // URL for successful callback;
			$fail = 'http://customer-devreview.com/jackpot/failg2a.php'; // URL for failed callbackcallback;
	
			//G2A TEST data
			//$hash = '06095303-81af-4cdb-82a7-b5d93169a9b0';
			///$secret = 'clILudixYceMtCP*^mskE2^Ha!8B=ULS4dnx4JicwAydMBcJyI+*K-GutO!jtE&w';
	
			$order = $_POST['orderId']; // Choose your order id or invoice number, can be anything
			$_SESSION['orderid'] = $order;
			$currency = 'USD'; // Pass currency, if no given will use "USD"
			$payment = new G2APay($hash, $secret, $success, $fail, $order, $currency);
			
			$discount = 0;
			
			if (isset($_POST['coupon']) && !empty($_POST['coupon'])) {
				$coupon = $_POST['coupon'];
				$stmt = $mysqli->prepare("SELECT `id`, `coupon`, `discount`, `status` FROM `rsj_coupons` WHERE `coupon` =?");
				$stmt->bind_param("s", $coupon);
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($couponid, $couponcontent, $coupondiscount, $couponstatus);
				$stmt->fetch();
				if ($stmt->num_rows > 0) 
				{
					$discount = $coupondiscount;
				}
				$stmt->close();
				
			}
			else 
			{
				$couponcontent = 'Not used';
			}
	
	
						//Process product data
						$p = 'case';
						$postproductid = 8;
						$postquantity = $_POST['quantity'];
					
						$sku = $_POST['orderId']; // Item number (In most cases $sku can be same as $id)
						$name = 'tEST';

						$quantity = 1; // Must be integer
						$id = $_POST['orderId']; // Your items' identifier
						if ($discount !== 0) {
							$absolutediscount = $_POST['price']*($discount/100);
							$absolutediscountx =  number_format($absolutediscount, 2);
							$finalprice = number_format($_POST['price']-$absolutediscountx,2);
						}
						else
						{
							$finalprice = number_format($_POST['price'],2);
						}
						$url = 'http://customer-devreview.com/jackpot/g2a-ipn.php';

						// Optional
						$extra = '';
						$type = '';

						// Add item to payment
						$payment->addItem($sku, $name, $quantity, $id, $finalprice, $url, $extra, $type);

						// Create payment against G2APay
						//$response = $payment->create();
						

						// Or if you want to create sandbox payment (for testing only)
						$response = $payment->test()->create();
						
	
						// Check if successful
						if ($response['success']) 
						{
							$delivery = 'tbd';
							$transactionId = 'pending';
							$paymentstatus = 'pending';
							mysqli_query($mysqli,"UPDATE rsj_order_payment SET txn_id='{$transactionId}', payment_status='{$paymentstatus}', delivery='{$delivery}', coupen='{$couponcontent}' WHERE order_id='{$id}'");
							header('Location: '.$response['url']); // redirect
							exit;
						} 
						else 
						{
							echo $response['message']; // print out error message
						}
		}
		else 
		{
			header('Location: http://customer-devreview.com/jackpot/index.php'); // redirect
			exit;
		}
	}
else 
{
	echo '<!doctype html>
	<html>
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RSJackpot - Login Required</title>
	<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
	<style>
	body {
		font-family: Lato;
		background: #333;
		color: #999;
		text-align: center;
	}
	.button {
		font-family: "Oswald";
		display: inline-block;
		padding: 10px 15px;
		background: #2E9AFE;
		border: 0;
		outline: 0;
		color: #FFF;
		text-transform: uppercase;
		text-decoration: none;
		font-size: 22px;
		cursor: pointer;
		transition: all ease 0.3s;
	}
	.button:hover {
		background: #222;
	}
	</style>
	</head>
	<body>
	You must be Logged in to continue.<br>
	<br>

	<a class="button" href="http://customer-devreview.com/jackpot/login.php?return">
	Login
	</a><br>
	<br>
	OR<br>
	<br>

	<a class="button" href="http://customer-devreview.com/jackpot/register.php?return">
	Register
	</a>
	</body>
	</html>
	';
	}
?>