<?
include_once('../session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
include_once("../config.php");
require('../../../vendor/autoload.php');

use Tuxxx128\G2aPay\G2aPayApi;
use Tuxxx128\G2aPay\G2aPayItem;
if(isset($_SESSION['id'])) {


// Set required variables
$hash = '06095303-81af-4cdb-82a7-b5d93169a9b0'; // Get it from G2APay
$secret = 'clILudixYceMtCP*^mskE2^Ha!8B=ULS4dnx4JicwAydMBcJyI+*K-GutO!jtE&w'; // Get it from G2APay
$success = 'http://rsjackpot.org/development/successg2a.php'; // URL for successful callback;
$fail = 'http://rsjackpot.org/failg2a.php'; // URL for failed callback;
	
//G2A TEST data
//$hash = 'd57d978b-3e03-452b-8e7f-f00a79ede563';
//$secret = 'rjZr@*2=2@9Et?TdL86OLbI3o59s6MJx@sc-reqtYy$FyOs7WyVrOnu*t+rzD&k7';
	
	//get next orderID from database
	$result = $mysqli->query("
    SHOW TABLE STATUS LIKE 'rsj_payments'
	");
	$data = $result->fetch_assoc();
	$next_increment = $data['Auto_increment'];
	
$order = $next_increment; // Choose your order id or invoice number, can be anything
$_SESSION['orderid'] = $order;
// Optional
$currency = 'USD'; // Pass currency, if no given will use "USD"

// Create payment instance
$g2aPayApi = new G2aPayApi($hash, $secret, true, 'dansontechnologies@gmail.com');
	
// Process Coupon if submitted
	$discount = 0;
	if (isset($_POST['coupon']) && !empty($_POST['coupon'])) {
		
		$coupon = $mysqli->real_escape_string($_POST['coupon']);
		//$couponquery = $mysqli->query("SELECT * FROM `rsj_coupons` WHERE `coupon` = '{$coupon}'");
		//$couponresults = $couponquery->fetch_assoc();
		
		
		$stmt = $mysqli->prepare("SELECT `id`, `coupon`, `discount`, `status` FROM `rsj_coupons` WHERE `coupon` =?");
		$stmt->bind_param("s", $coupon);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($couponid, $couponcontent, $coupondiscount, $couponstatus);
		$stmt->fetch();		
		
		if (($stmt->num_rows == 0) or ($couponstatus == 'inactive')) {
			echo '<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Login Required</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
</style>
</head>
<body>
Invalid/Expired Coupon code.<br>
<br>
<a class="button" onClick="window.history.back();">
Back
</a><br>
</body>
</html>';
exit;
		}
		else {
			$discount = $coupondiscount;
		}
		
	$stmt->close();
	}
	else {
		$couponcontent = 'Not used';
	}
	
	
//Process product data
	$p = $_POST['p'];
	$postproductid = $_POST['productid'];
	$postquantity = $_POST['quantity'];
//	$_SESSION['p'] = $p;
//	$_SESSION['productid'] = $postproductid;
//	$_SESSION['quantity'] = $quantity;
	
	if ($p == 'case') {
		//$result = $mysqli->query("SELECT * FROM rsj_products WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_products WHERE id=?");
	}
	else if ($p == 'pack') {
		//$result = $mysqli->query("SELECT * FROM rsj_packs WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_packs WHERE id=?");
	}
	else if ($p == 'coin') {
		//$result = $mysqli->query("SELECT * FROM rsj_coins WHERE id='{$productid}'");
		
		$stmt = $mysqli->prepare("SELECT `id`, `name`, `desc`, `items`, `price`, `type` FROM rsj_coins WHERE id=?");
	}
	
		$stmt->bind_param("s", $postproductid);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($productid, $productname, $productdesc, $productitems, $productprice, $producttype);
		$stmt->fetch();
		$stmt->close();
	
// check if product is in flash sale (makes any tampered coupon submitted useless)
	$flashquery = $mysqli->query("SELECT * FROM rsj_flash WHERE id='1'");
	$flashdata = $flashquery->fetch_assoc();
	
	$gmt = new DateTimeZone('GMT');
		
	$startdt = new DateTime($flashdata['startdt'], $gmt);
		
	$enddt = new DateTime($flashdata['enddt'], $gmt);
		
	$currentdt = new DateTime(gmdate("Y-m-d H:i:s"), $gmt);
	
	
	if (($flashdata['p'] == $p) && ($flashdata['productid'] == $productid) && ($flashdata['visibility'] == 'enable') && ($startdt < $currentdt) && ($currentdt < $enddt)) {
		$discount = $flashdata['discount'];
	}
	
	if ($discount !== 0) {
		$absolutediscount = $productprice*($discount/100);
		$absolutediscountx =  number_format($absolutediscount, 2, '.', '');
	}
	$finalprice = $productprice-$absolutediscountx;
	$finalpricewithvip = number_format($finalprice - $finalprice*$_SESSION['vip'], 2);
	
	//$paymentAmount = $finalpricewithvip*$quantity;
	
// Set item parameters
$sku = $productid; // Item number (In most cases $sku can be same as $id)
$name = $productname;
$quantity = $postquantity; // Must be integer
$id = $productid; // Your items' identifier
$price = $finalpricewithvip; // Must be float
$url = 'https://rsjackpot.org/product.php?p='.$p.'&id='.$productid;

//Item Details
$g2aitem = (new G2aPayItem)->itemTemplate();

$g2aitem->name = $name;
$g2aitem->url = $url;
$g2aitem->price = $price;
$g2aitem->qty = $quantity;
$g2aitem->sku = $sku;
$g2aitem->id = $id;

$g2aPayApi->addItem($g2aitem);


$g2aPayApi->setUrlFail($fail);
$g2aPayApi->setUrlSuccess($success);
$g2aPayApi->setOrderId($order);
	
// $g2aPayApi->setEmail('user@server.tld');
$items = explode(',', $productitems);
	
	if ($p == 'case') {
		for ($x=0; $x<$quantity; $x++) {
		$rand1 = rand(1,2000);
	
		if ((1 <= $rand1) && ($rand1 <= 980)) {
			// Blue
			$i = rand(0,4);
		}
		else if ((981 <= $rand1) && ($rand1 <= 1820)) {
			// Purple
			$i = rand(5,7);
		}
		else if ((1821 <= $rand1) && ($rand1 <= 1979)) {
			// Pink
			$i = rand(8,10);
		}
		else if ((1980 <= $rand1) && ($rand1 <= 1999)) {
			// Red
			$i = rand(11,12);
		}
		else if ($rand1 = 2000) {
			// Red
			$i = 13;
		}
		$item[$x] = $items[$i];
		}
	}
	else if ($p == 'pack') {
		$totalItems = count($items)-1;
		for ($x=0; $x<$quantity; $x++) {
			$i = rand(0,$totalItems);
			$item[$x] = $items[$i];
		}
	}
	else if ($p == 'coin') {
		for ($x=0; $x<$quantity; $x++) {
			$rand1 = rand(1,100);
			if ((1 <= $rand1) && ($rand1 <= 60)) {
				$i = 1;
			}
			else if ((61 <= $rand1) && ($rand1 <= 100)) {
				$i = 0;
			}
			$item[$x] = $items[$i];
		}
	}
	// CREATE A PENDING ENTRIES IN DATABASE
	$ip = $_SERVER['REMOTE_ADDR'];
	
	for ($y=0; $y<$quantity; $y++) {
		
			$dt = gmdate("Y-m-d H:i:s");
			$casestatus = 'closed';
			$delivery = 'tbd';
			$transactionId = 'pending';
			$paymentamount = 'pending';
			$paymentstatus = 'pending';
			$stmt = $mysqli->prepare("INSERT INTO rsj_payments (txnid, processid, payment_amount, payment_status, productid, product, color, productname, case_status, item, userid, createdtime, ip, delivery, coupon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("sssssssssssssss", $transactionId, $order, $paymentamount, $paymentstatus, $productid, $p, $productdesc, $productname, $casestatus, $item[$y], $_SESSION['id'], $dt, $ip, $delivery, $couponcontent);
			$stmt->execute();
			$stmt->close();
	}
	
	header('Location: '.$g2aPayApi->getRedirectUrlOnGateway());
	die;
}
else {
	echo '<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Login Required</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
</style>
</head>
<body>
You must be Logged in to continue.<br>
<br>

<a class="button" href="http://rsjackpot.org/login.php?return">
Login
</a><br>
<br>
OR<br>
<br>

<a class="button" href="http://rsjackpot.org/register.php?return">
Register
</a>
</body>
</html>
';
}
?>