<?php
include_once('session.php');
secure_session_start();
define('INCLUDE_CHECK',true);
require 'connect.php';
if (!isset($_SESSION['id'])) {
	header("Location: login.php");
	exit;
}
else if (isset($_POST['caseid'])) {
	$caseid = $_POST['caseid'];
	//$results = $mysqli->query("SELECT * FROM rsj_payments WHERE userid ='{$_SESSION['id']}' && id = '{$caseid}'");
	
	$stmt = $mysqli->prepare("SELECT `case_status`, `productid`, `item` FROM rsj_payments WHERE userid=? AND id=?");
	$stmt->bind_param("ss", $_SESSION['id'], $caseid);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($casestatus, $caseproductid, $caseitem);
	$stmt->fetch();
	
	if ($stmt->num_rows === 0) {
		header("Location: inventory.php");
		exit;
	}
	else {
		if ($casestatus == 'opened') {
			header("Location: inventory.php");
			exit;
		}
		else {
			//$mysqli->query("UPDATE rsj_payments SET case_status = 'opened' WHERE id = '{$caseid}'");
			$stmtx = $mysqli->prepare("UPDATE rsj_payments SET case_status = 'opened' WHERE id =?");
			$stmtx->bind_param("s", $caseid);
			$stmtx->execute();
			$stmtx->close();
			
$products = $mysqli->query("SELECT * FROM rsj_coins WHERE id ='{$caseproductid}'");
$product = $products->fetch_assoc();
$items = explode(',', $product['items']);
$targetitem = $caseitem;
$targetkey = array_search ($targetitem, $items);
if ($targetkey == 0) {
	$degrees = 10*360;
}
else {
	$degrees = 19*180;
}
			?>

<!doctype html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121616401-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121616401-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Flipping Coin...</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Raleway:300,400,700|Lato:300,400" rel="stylesheet" type="text/css">
<style>
body {
	font-family: Lato;
	background: #333;
	color: #999;
	text-align: center;
}
* {
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
.button {
	font-family: "Oswald";
	display: inline-block;
	padding: 10px 15px;
	background: #2E9AFE;
	border: 0;
	outline: 0;
	color: #FFF;
	text-transform: uppercase;
	text-decoration: none;
	font-size: 22px;
	cursor: pointer;
	transition: all ease 0.3s;
}
.button:hover {
	background: #222;
}
h1 {
	font-family: Lato;
	font-size: 32px;
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	padding: 10px;
	border-bottom: solid 2px #FFF;
	width: 200px;
	text-align: center;
	margin: 0 auto 30px auto;
}
.flip-wrap {
	margin: 0 auto;
	perspective: 1000px;
}
.flip-wrap, .front, .back {
	width: 200px;
	height: 200px;
}
/* flip speed goes here */
.flipper {
	transform-style: preserve-3d;

	position: relative;
}

/* hide back of pane during swap */
.front, .back {
	background: #222;
	backface-visibility: hidden;

	position: absolute;
	top: 0;
	left: 0;
	border-radius: 100px;
	line-height: 210px;
}

/* front pane, placed above back */
.front {
	z-index: 2;
	/* for firefox 31 */
	transform: rotateY(0deg);
	border: #FFD700 solid 2px;
}

/* back, initially hidden pane */
.back {
	transform: rotateY(180deg);
	border: #DDD solid 2px;
}

#fwrap.flip .flipper {
		transform: rotateY(<? echo $degrees ?>deg);
}
.transition {
	transition: 5s;
}
.img {
	background: #111;
	padding: 10px;
	width: 60px;
	height: 60px;
	line-height: 60px;
	border-radius: 30px;
	display: inline-block;
}
.item-name {
	position: absolute;
	left: 0;
	right: 0;
	bottom: -50px;
}

.blue-c {
	border: solid 2px rgba(0, 76, 255, 0.5);
}
.purple-c {
	border: solid 2px rgba(105, 0, 255, 0.5);
}
.pink-c {
	border: solid 2px rgba(255, 0, 190, 0.5);
}
.red-c {
	border: solid 2px rgba(255, 0, 4, 0.5);
}
.green-c {
	border: solid 2px rgba(9, 231, 0, 0.5);
}
.gold-c {
	border: solid 2px rgba(255, 215, 0, 0.5);
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
</head>

<body>
<h1>Flipping Coin...</h1>
<div class="flip-wrap" id="fwrap">
	<div class="flipper">
		<div class="front"><?
        echo '<div class="img"><img src="assets/images/coins/'.$product['type'].'/'.$items[0].'.png" alt="'.$items[0].'"/></div>';
			 echo '<div class="item-name">'.$items[0].'</div>' ?>
		</div>
		<div class="back"><?
        echo '<div class="img"><img src="assets/images/coins/'.$product['type'].'/'.$items[1].'.png" alt="'.$items[1].'"/></div>';
			 echo '<div class="item-name">'.$items[1].'</div>' ?>
		</div>
	</div>
</div>
<?
//for($i=0; $i<2; $i++) {
//	$class = 'gold-c';
//				if ($items[$i] == 'Godsword Shard 1/2/3') {
//					$image = 'Godsword Shard';
//				}
//				//else if ($product['id'] == '9') {
////					if (strpos($items[$i],'Frost') !== false) {
////						$image = 'Frost bones';
////					}
////					else if (strpos($items[$i],'Dagonnath') !== false) {
////						$image = 'Dagonnath bones';
////					}
////					else if (strpos($items[$i],'Burnt') !== false) {
////						$image = 'Burnt bones';
////					}
////					else if (strpos($items[$i],'Baby') !== false) {
////						$image = 'Baby dragon bones';
////					}
////					else if (strpos($items[$i],'Monkey') !== false) {
////						$image = 'Monkey bones';
////					}
////					else if (strpos($items[$i],'Big') !== false) {
////						$image = 'Big bones';
////					}
////					else {
////						$image = 'Bones';
////					}
////				}
//				else {
//					$image = $items[$i];
//				}
//				if ($product['type'] == '07') {
//				echo 	'
//						<div class="product-item '.$class.'" id="selector-'.$i.'">
//						<div class="img"><img src="assets/images/packs/07/'.$image.'.png" alt="'.$items[$i].'"/></div>
//            			'.$items[$i].'
//            			</div>';
//				}
//				else {
//				echo 	'<div class="product-item '.$class.'" id="selector-'.$i.'">
//						<div class="img"><img src="assets/images/packs/rs3/'.$image.'.png" alt="'.$items[$i].'"/></div>
//            			'.$items[$i].'
//            			</div>';
//				}
//		}
?>

<script type="text/javascript">
$(function() {
	$("#fwrap .flipper").addClass('transition');
	$("#fwrap").addClass('flip');
	setTimeout( function(){
         $("#fwrap").effect( "pulsate", {times:3}, 1500 );
    }, 5100 );
		setTimeout( function(){
             window.location = "inventory.php";
    }, 8500 );
});
</script>
</body>
</html>
            <?
		}
	}
}
else {
	header("Location: inventory.php");
	exit;
}
?>
