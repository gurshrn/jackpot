<?php
define('INCLUDE_CHECK',true);
require 'config.php';

include_once('session.php');
secure_session_start();

if(is_null($_SESSION['id'])) {
   header( 'Location: https://rsjackpot.org/login.php' ) ;
   }

$productid = $_POST['productid'];
$product = $_POST['p'];
$quantity = $_POST['quantity'];

$itemquery = $mysqli->query("SELECT * FROM `rsj_products` WHERE `id` = '$productid'");
$itemresults = $itemquery->fetch_assoc();
$color = $itemresults['desc'];
$totalPrice = $itemresults['price'];
$btc_total_price = $quantity*$totalPrice;

if (isset($_POST['coupon']) && !empty($_POST['coupon']))
{
  $coupon = $_POST['coupon'];

  $couponquery = $mysqli->query("SELECT * FROM `rsj_coupons` WHERE `coupon` = '$coupon' AND `status` = 'active'");
  $couponresults = $couponquery->fetch_assoc();

  if ($couponresults)
  {
    $discountPercentage = $couponresults['discount'];
    $discountAmount = $totalPrice/100*$discountPercentage;
    $finalPrice = $totalPrice-$discountAmount;
    $couponCode = $coupon;
  }
  else
  {
    $finalPrice = $totalPrice;
    $couponCode = 'Not used';
  }
}
else
{
  $finalPrice = $totalPrice;
  $couponCode = 'Not used';
}

$coin = $_POST['cointype'];
$chs = curl_init('https://min-api.cryptocompare.com/data/pricemulti?fsyms='.$coin.'&tsyms=USD');
curl_setopt($chs, CURLOPT_FAILONERROR, TRUE);
curl_setopt($chs, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($chs, CURLOPT_SSL_VERIFYPEER, 0);

$local = curl_exec($chs);
$local = json_decode($local, true);

$price = $local[$coin]['USD']; //get price


$btc_amount = $btc_total_price/$price;
$btc_amount_insert = $btc_amount*100000000;

$public_key = '2500097abe57d11f6fa524dc53d482d5f4a1b399d6f48f4b9e8945b6c0e9cf72';
$private_key = '1ea39382f9421c03D5741d3675e8b4f5C00Cc32D69C1CF0E6029a09b3855dae6';

$cmd = 'get_callback_address';
// Set the API command and required fields
$req['version'] = 1;
$req['cmd'] = $cmd;
$req['currency'] = $coin;
$req['key'] = $public_key;
$req['format'] = 'json'; //supported values are json and xml
// Generate the query string
$post_data = http_build_query($req, '', '&');
// Calculate the HMAC signature on the POST data
$hmac = hash_hmac('sha512', $post_data, $private_key);
// Create cURL handle and initialize (if needed)
static $ch = NULL;
if ($ch === NULL)
{
    $ch = curl_init('https://www.coinpayments.net/api.php');
    curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
}
curl_setopt($ch, CURLOPT_HTTPHEADER, array('HMAC: '.$hmac));
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
// Execute the call and close cURL handle
$server_output = curl_exec($ch);

curl_close ($ch);
$dec = json_decode($server_output, TRUE, 512, JSON_BIGINT_AS_STRING);

$btc_address = $dec['result']['address'];

$items = explode(',', $itemresults['items']);


if ($product == 'case') {
  for ($x=0; $x<$quantity; $x++) {
  $rand1 = rand(1,2000);

  if ((1 <= $rand1) && ($rand1 <= 980)) {
    // Blue
    $i = rand(0,4);
  }
  else if ((981 <= $rand1) && ($rand1 <= 1820)) {
    // Purple
    $i = rand(5,7);
  }
  else if ((1821 <= $rand1) && ($rand1 <= 1979)) {
    // Pink
    $i = rand(8,10);
  }
  else if ((1980 <= $rand1) && ($rand1 <= 1999)) {
    // Red
    $i = rand(11,12);
  }
  else if ($rand1 = 2000) {
    // Red
    $i = 13;
  }
  $item[$x] = $items[$i];
  }
}
else if ($product == 'pack') {
  $totalItems = count($items)-1;
  for ($x=0; $x<$quantity; $x++) {
    $i = rand(0,$totalItems);
    $item[$x] = $items[$i];
  }
}
else if ($product == 'coin') {
  for ($x=0; $x<$quantity; $x++) {
    $rand1 = rand(1,100);
    if ((1 <= $rand1) && ($rand1 <= 60)) {
      $i = 1;
    }
    else if ((61 <= $rand1) && ($rand1 <= 100)) {
      $i = 0;
    }
    $item[$x] = $items[$i];
  }
}
$productname = $itemresults['name'];

// CREATE A PENDING ENTRIES IN DATABASE
$ip = $_SERVER['REMOTE_ADDR'];
$user_idz = $_SESSION['id'];
$dt = gmdate("Y-m-d H:i:s");
$mysqli->query("INSERT INTO rsj_bitcoin (amount, address) VALUES ('$btc_amount_insert', '$btc_address')");
$order_id = $mysqli->insert_id;
for ($y=0; $y<$quantity; $y++) {
$insert_item = $item[$y];
$stmt = $mysqli->query("INSERT INTO rsj_payments (txnid, processid, payment_amount, payment_status, productid, product, color, productname, case_status, item, userid, createdtime, ip, delivery, coupon, btc_id) VALUES ('pending', 'pending', '$finalPrice', 'pending', '$productid', '$product', '$color', '$productname', 'closed', '$insert_item', '$user_idz', '$dt', '$ip', 'tbd', '$couponCode', '$order_id')");
}


?>
<!doctype html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121616401-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121616401-1');
</script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - <? echo $productname; ?></title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.2" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
</script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>
		<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}

		 echo '</ul>

		</div>
		</div>'; ?>
<div class="content-wrap"><div class="page-title"><? echo $coin ?> Coin Payment</div>
    <div class="main-content">

      <div class="product-buy-now" style="text-align: center !important" id="paymentPart">
        <h4>Please make <b><font color="red"><?php echo $btc_amount; ?></font> <? echo $coin ?></b> payment to the following address</h4>
        <h3><b><?php echo $btc_address; ?></b></h3>
        <br /><br />
        <img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:<?php echo $btc_address; ?>?&amount=<?php echo $btc_amount; ?>" />
        <br /><br />
        <h4>Important Note: Please don't close the page until your payment is confirmed!</h4>
        </div>
    </div>
</div>



            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
<script type="text/javascript">
setInterval(ajaxCall, 6000);

function ajaxCall() {
  $.ajax({
  url: "https://rsjackpot.org/bitcoin-check.php?order_id=<?php echo $order_id; ?>",
  success: function (response) {
  if (response == 'OK') {
  $("#paymentPart").html('<h1><font color="white">Your payment is confirmed! Thank you.</font></h1>');
  setTimeout(function(){ window.location = "https://rsjackpot.org/inventory.php"; }, 3000);

  } else {
  console.log(response);
  }
  }
  });
}

function sleep(milliseconds) {
var start = new Date().getTime();
for (var i = 0; i < 1e7; i++) {
if ((new Date().getTime() - start) > milliseconds){
break;
}
}
}
</script>
