<?php
define('INCLUDE_CHECK',true);
require 'config.php';

$allresults = $mysqli->query("SELECT rsj_order_item.*,rsj_members.usr FROM rsj_order_item LEFT JOIN rsj_order_payment ON rsj_order_item.order_id = rsj_order_payment.order_id LEFT JOIN rsj_members ON rsj_order_payment.user_id = rsj_members.id ORDER BY rsj_order_item.id DESC");


$alldata = array();
while ($allrow = $allresults->fetch_assoc()) {
	$alldata[] = $allrow;
}



$topresults = $mysqli->query("SELECT rsj_case.case_name,rsj_case_items.* FROM rsj_case_items LEFT JOIN rsj_case ON rsj_case_items.case_id = rsj_case.id WHERE rsj_case_items.top_item = 'yes' ORDER BY rsj_case_items.id DESC");

$topdata = array();
while ($toprow = $topresults->fetch_assoc()) {
	$topdata[] = $toprow;
}


$statresults = $mysqli->query("SELECT * FROM rsj_order_item");
$casecount = $statresults->num_rows;

$statresults->close();

$allresults->close();
$topresults->close();
echo json_encode(array($alldata, $topdata, $casecount));
?>