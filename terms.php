<?php
include_once('session.php');
secure_session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - Terms &amp; Conditions</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.1" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>
				<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="about.php">About Us</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		
		 echo '</ul> 

		</div>
		</div>'; ?>
<div class="content-wrap">
	 <div class="page-title" id="">Terms of Service</div>
  <div class="main-content">
Please read these terms of service ("terms", "terms of service") carefully before using the rsjackpot.org website (the "service") operated by Danson Technologies LLC ("us", 'we", "our"). If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable copyright and trade mark law.
<br><br>
<h2>Your Obligations and Use of the Website</h2>
By agreeing to these Terms of Service, you represent that you are the legal age required to engage in the activities on this website from the jurisdiction from which the site is being accessed. You also agree you are not restricted by limited legal capcity or from a jurisdiction where it is not legal to do so.  You also confirm and acknowledge that we have no legal requirement to obtain any type of license in order to provide the website's services from the jurisdiction from which the website is being accessed. <br>
You confirm and acknowledge you are fully able to enter into all of these terms and conditions set forth and you agree to abide by and comply with these terms<br>
<h2>Copyright </h2>
We are not affiliated with Jagex LTD, Runescape, or any other game publishing company.
<br>
<h2>Delivery and Refund Policy</h2>
If you have not opened your case or pack AND it has been less than 24 hours since your purchase we may refund your order(s). We can not offer a refund on any case or pack that has been purchased and opened by a user. 
<br>
<h2>Fraud Prevention</h2>
We reserve the right to request info for further verification of your account for any reason. We may ask for government issued ID, name, email, and transaction details at any point in the delivery process. If a customer fails to supply the requested information, rsjackpot.org has every right to refuse service without refund.<br><br>
We reserve the right to use any of the information sent by users to incriminate any individual involved in fraudulent claims against rsjackpot.org. In the case of fraud these documents can be given to local authorities, PayPal, G2A, or any other 3rd party involved.
<br>
<h2>Disclaimer</h2>
The materials on this web site are provided “as is” without warranties, expressed or implied.<br>
<br>
It is your responsibility to perform proper research and understand the individual policies of 3rd party services. We can not be responsible for any enforcement taken against your account(s) on 3rd party services resulting in any loss. This includes without limitation, loss of account, virtual items, data, or profit.<Br>
<br>
In no event shall we our our suppliers be liable for any damages arising out of the use or inability to use the rsjackpot.org website or services. 
<br>
<h2>Third-Party Links</h2>
We have not reviewed all of the sites linked to the rsjackpot.org website and is not responsible for the information posted or actions taking place on any such website. The inclusion of any link does not imply endorsement by us of the linked site. Use of any such linked web site is at the user's own risk.
<br>
<h2>Terms of Service Modifications</h2>
We may revise these terms of use for its web site at any time without notice. By using this web site you are agreeing to be bound by the then current version of these Terms and Service.

<h2>Governing Law</h2>
Any claim relating shall be governed by the State of North Carolina in the United States of America.
<br><br>
<h2>Additional Terms and Conditions; EULAs</h2>
hen you use G2A Pay services provided by G2A.COM Limited (hereinafter referred to as the "G2A Pay services provider") to make a purchase on our website, responsibility over your purchase will first be transferred to G2A.COM Limited before it is delivered to you. G2A Pay services provider assumes primary responsibility, with our assistance, for payment and payment related customer support. The terms between G2A Pay services provider and customers who utilize services of G2A Pay are governed by separate agreements and are not subject to the Terms on this website.  
With respect to customers making purchases through G2A Pay services provider checkout, (i) the Privacy Policy of G2A Pay services provider shall apply to all payments and should be reviewed before making any purchase, and (ii) the G2A Pay services provider Refund Policy shall apply to all payments unless notice is expressly provided by the relevant supplier to buyers in advance. In addition the purchase of certain products may also require shoppers to agree to one or more End-User License Agreements (or "EULAs") that may include additional terms set by the product supplier rather than by Us or G2A Pay services provider. You will be bound by any EULA that you agree to.
We and/or entities that sell products on our website by using G2A Pay services are primarily responsible for  warranty, maintenance, technical or product support services for those Products. We and/or entities that sell products on our website are primarily responsible to users for any liabilities related to fulfillment of orders, and EULAs entered into by the End-User Customer. G2A Pay services provider is primarily responsible for facilitating your payment. 
You are responsible for any fees, taxes or other costs associated with the purchase and delivery of your items resulting from charges imposed by your relationship with payment services providers or the duties and taxes imposed by your local customs officials or other regulatory body.<br><br>
<b>For customer service inquiries or disputes, You may contact us by email at support@rsjackpot.org or by livechat!<br><br>
Questions related to payments made through G2A Pay services provider payment should be addressed to support@g2a.com. </b>
Where possible, we will work with You and/or any user selling on our website, to resolve any disputes arising from your purchase.

  </div><br>
    <div class="page-title" id="">Company Contact Info</div>
  <div class="main-content">
        		<b>Company Name:</b> Danson Technologies LLC | Registered in North Carolina, USA<br>
        		<b>Company Telephone:</b> +1 (828) 394-9706<Br>
        		<b>Company Registration Number:</b> 1593550<br>
        		<b>Company Address:</b> 1805 Wood Henge Drive Raleigh, NC 27613<br>
        		<b>Contact E-mail:</b> <a href="mailto:dansontechnologies@gmail.com">dansontechnologies@gmail.com</a><br>
  </div>
</div>
    
            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>
