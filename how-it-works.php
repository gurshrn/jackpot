<?php
include_once('session.php');
secure_session_start();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>RSJackpot - How it works</title>
<link href="https://fonts.googleapis.com/css?family=Oswald|Fira+Sans+Condensed:300,400,700|Lato:300,400|Satisfy" rel="stylesheet" type="text/css">
<link href="assets/css/style.css?v=1.1" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="assets/carousel/owl.theme.css" rel="stylesheet" type="text/css">
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicon/manifest.json">
<link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png">
<meta name="msapplication-config" content="/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/fuser.js"></script>
<script type="text/javascript" src="assets/carousel/owl.carousel.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3hPsqPP0OZlWQSYtrSjowtXhAGgCgr1s";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<? if (isset($_SESSION['usr'])) { ?>
<script type="text/javascript">
   $zopim(function(){
           $zopim.livechat.setName('<? echo $_SESSION['usr'] ?>');
    });
</script>
<? } ?>
</head>

<body>
<? echo ' <div class="top-wrap">
		<div class="top group">
		<div id="nav-icon"></div>
		<a id="logo" title="RSJackpot" href="/"></a>';
		if ((isset($_SESSION['vip'])) && ($_SESSION['vip'] != 0)) {
			echo '<div class="vip-label"></div>';
		}
		echo'
		<ul id="nav"><li><a href="/">Home</a></li><li><a href="how-it-works.php">How it works</a></li><li><a href="daily-free-case.php">Daily Free Case</a><li><a href="vip.php">VIP</a></li><li><a href="about.php">About Us</a></li><li><a href="https://rs.gold/">Buy RS Gold</a></li>';
		 if(isset($_SESSION['id'])) {
			echo '<li><a href="#" class="profile-drop-down"><img src="assets/images/profile.png" /></a>
			<ul>
			<li><span>Welcome, '; echo $_SESSION['usr'].'!</span></li>
			<li><a href="inventory.php">Inventory</a></li>
			<li><a href="feedback.php">Feedback</a></li>
			<li><a href="upload.php">Upload</a></li>
			<li><a href="password.php">Change Password</a></li>
			<li><a href="account.php?logout">Logout</a></li>
			</ul></li>';
		}
		else {
			echo '<li><a href="login.php">Login</a></li><li><a href="register.php">Register</a></li>';
		}
		echo '<li><a href="cart.php">Cart</button></li>';
		 echo '</ul> 

		</div>
		</div>'; ?>
<div class="content-wrap">
	<div class="page-title">How it works</div>
    <div class="main-content">
    	<div class="htw">
    		<div class="htw-box">
        		<div class="content"><div class="step">1</div><h1>Purchase Case</h1>
        		<h3><a href="register.php">Register</a>, <a href="login.php">log in</a>, and then purchase your favorite case or pack!</h3>
           	    <img src="assets/images/htw1.png" width="150" height="150" alt=""/> </div>
        	</div>
    		<div class="htw-box">
        		<div class="content"><div class="step">2</div><h1>Open Case</h1>
        		<h3>Go to your RSJackpot <a href="inventory.php">Inventory</a> and open your case or pack to receive your item!</h3>
           	    <img src="assets/images/htw2.png" width="150" height="150" alt=""/>
                
                </div>
        	</div>
    		<div class="htw-box">
        		<div class="content"><div class="step">3</div><h1>Redeem Prize</h1>
        		<h3>Click "claim" and talk with a Livechat member who will meet you in-game at the Grand Exchange to trade you your item!</h3>
           	    <img src="assets/images/htw3.png" width="150" height="150" alt=""/>

                
                </div>
        	</div>
        </div>
    </div>
</div>
    
            <div class="footer-wrap">
                <div class="footer-apparatus"></div>
                <div class="footer-content">
                    <div class="links">
                        <ul>
                            <li><a href="terms.php">Terms & Conditions</a></li>
                            <li><a href="privacy.php">Privacy Policy</a></li>
                            <li><a href="faq.php">FAQ</a></li>

                        </ul>
                        <ul>
                            <li><a href="login.php">Login</a></li>
                            <li><a href="register.php">Register</a></li>
                        </ul>
                        <ul>
                            <li>
                                <a id="g2a" title="G2A Payment Methods" href="https://pay.g2a.com/"></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <div class="g2a_widget_block">
                                    <div class="g2a_widget_rating" data-widget-rating="e068bf1d-0da7-47a2-a035-2cec6165250b"></div>
                                    <script async src="https://platform.g2a.com/rating.js" charset="utf-8"></script>
                                </div>
                            </li>
                        </ul>

                    </div>
                    <div class="social">
                        <a href="https://www.facebook.com/RSJackpot/"><img src="assets/images/facebook.png" width="48" height="48" alt="" /></a>
                        <a href="https://twitter.com/RSJackpot"><img src="assets/images/twitter.png" width="48" height="48" alt="" /></a>

                    </div>

                    <div class="copyright">&copy; Danson Technologies LLC 2017</div>
                </div>
            </div>
</body>
</html>